<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

include("includes/fns.php");
include("/opt/autoglosser2/config.php");

// Generate default names from the filepath given
list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();

// echo $importfile."\n";
// echo $filename."\n";
// echo $utterances."\n";
// echo $words."\n";
// echo $cgfinished."\n";
// echo $holding."\n";
echo "Outputs are in outputs/$filename/\n";

include("import.php");  // Outputs: _utterances.txt, _sentencised.txt, _utterances table
include("wordify.php");  // Outputs: _words.txt, _wordified.txt, _words table
    // Activate the following two and deactivate import.php and wordify.php if using tokenised text from CyTag.
    //include("cytag_import.php");  
    //include("cytag_words.php");
include("cohorts.php");  // Outputs: _unknowns.txt, _names.txt, _cg.txt
include("apply_cg.php");  // Outputs: _cg_applied.txt, _cg_applied_old, _cg_applied_old_old
include("trace.php");  // Outputs: _cg_traced.txt
include("cgfinished.php");  // Outputs: _cgfinished.txt, _cgfinished table
include("join_tags.php");  // Outputs: _joined.txt, _holding table, updates to _words table
include("pdfoutput.php");  // Outputs: .tex, pdf
include("txtoutput.php");  // Outputs: _txtoutput.txt
    
?>
