# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-09-12 16:34+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: includes/main-page-php-clean.php:27 includes/main-page-php-orig.php:42
#: includes/main-page-php-working.php:44 main-page-php-clean.php:27
#: main-page-php.php:44 main-page-php-clean.php:18
msgid "Page Title"
msgstr ""

#: includes/main-page-php-clean.php:32 includes/main-page-php-orig.php:47
#: includes/main-page-php-working.php:49 main-page-php-clean.php:32
#: main-page-php.php:49 main-page-php-clean.php:23
msgid "Welcome to our website"
msgstr ""

#: includes/main-page-php-clean.php:34 includes/main-page-php-orig.php:49
#: includes/main-page-php-working.php:51 main-page-php-clean.php:34
#: main-page-php.php:51 main-page-php-clean.php:25
msgid "This is a test paragraph to check if translations are working."
msgstr ""

#: includes/main-page-php-clean.php:36 main-page-php-clean.php:36
#: main-page-php-clean.php:27
msgid ""
"You can test Autoglosser2 by using the web interface below, which handles up "
"to 300 characters of text (on average, around 50 words) - anything over that "
"will not be processed.  Just type your Welsh text into the box, and press "
"Gloss it!"
msgstr ""

#: includes/footer.php:8
msgid ""
"&copy; Copyright Kevin Donnelly 2016-18. Thanks to: Joshua Gatcke for <a "
"href=\"http://www.99lime.com\">HTML KickStart"
msgstr ""

#: includes/lang_changer.php:2 includes/lang-changer-php.php:2
#: includes/rev-lang-changer.php:2 includes/lang_changer_orig.php:2
msgid "Page language:"
msgstr ""

#: includes/langinit.php:40 includes/langinit-php.php:21
#: includes/rev-langinit.php:6 includes/langinit-php.php:18
#: includes/langinit.php:9 includes/langinit_orig.php:5
msgid "English (Saesneg)"
msgstr ""

#: includes/langinit.php:41 includes/langinit-php.php:22
#: includes/rev-langinit.php:7 includes/langinit-php.php:19
#: includes/langinit.php:10 includes/langinit_orig.php:6
msgid "Welsh (Cymraeg)"
msgstr ""

#: index.php:42
msgid "<h3 class=\"center\">Welcome to Autoglosser2!</h3>"
msgstr ""

#: index.php:44
msgid ""
"<p>Autoglosser2 is a glosser/tagger for Welsh, using the <a href=\"http://"
"eurfa.org.uk\">Eurfa</a> Welsh dictionary.  The Autoglosser2 code (GPL) is "
"in a <a href=\"https://donnek@bitbucket.org/donnek/autoglosser2.git"
"\">Bitbucket repository</a>, and a detailed <a href=\"./resources/manual.pdf"
"\">manual</a> is available.  Autoglosser2 is a heavily revised version of "
"the <a href=\"https://github.com/donnekgit/autoglosser\">Bangor Autoglosser</"
"a>, which was developed to gloss the <a href=\"http://bangortalk.org.uk"
"\">Bangor ESRC corpora</a> of conversational text.  Autoglosser2, on the "
"other hand, is aimed at written rather than spoken Welsh text, and has been "
"refactored to tidy the code and make it far faster (over 22,000 glosses per "
"minute).</p>"
msgstr ""

#: index.php:46 index.php:148 index.php:144 index.php:143
msgid "<p><strong>References:</strong></p>"
msgstr ""

#: index.php:60
msgid "<h4 class=\"center\">Gloss parts of speech in Welsh</h4>"
msgstr ""

#: index.php:62 index.php:54 index.php:50 index2.php:47
#: main-page-php-clean.php:38 index.php:49 main-page-php-clean.php:29
msgid ""
"<p>You can test Autoglosser2 by using the web interface below, which handles "
"up to 300 characters of text (on average, around 50 words) - anything over "
"that will not be processed.  Just type your Welsh text into the box, and "
"press <strong>Gloss it!</strong>. If you can't think of any Welsh to use, "
"try some of the samples below the box.  The glossing works best if you enter "
"\"written\" Welsh, i.e. with correct spelling and punctuation.  Words that "
"are not in Eurfa yet (which may also affect the glossing accuracy) are "
"marked in red.  If you need to tag large amounts of text, it's best to "
"install Autoglosser2 on your local machine - see the <a href=\"./resources/"
"manual.pdf\">manual</a> - Appendix A has full installation instructions.</p>"
msgstr ""

#: index.php:74 index.php:66 index.php:62 index.php:61
msgid "Enter up to 300 characters of Welsh text here."
msgstr ""

#: index.php:76 index.php:68 index.php:64 index.php:63
msgid "In glosses, don't tag punctuation"
msgstr ""

#: index.php:80 index.php:72 testme.php:71 index.php:68 index.php:67
msgid "Glosses, horizontal layout"
msgstr ""

#: index.php:83 index.php:75 index.php:71 index.php:70
msgid "Glosses, vertical layout"
msgstr ""

#: index.php:86 index.php:78 index.php:74 index.php:73
msgid "CorCenCC tags, horizontal layout"
msgstr ""

#: index.php:89 index.php:81 index.php:77 index.php:76
msgid "CorCenCC tags, vertical layout"
msgstr ""

#: index.php:93 index.php:85 index.php:81 index.php:80
msgid "Gloss it!"
msgstr ""

#: index.php:116 index.php:107 index.php:103 index.php:102
msgid "<h6>Samples for testing</h6>"
msgstr ""

#: index.php:142 index.php:132 index.php:128 index.php:127
#: main-page-php-clean.php:37
msgid "The glossed/tagged text will appear here."
msgstr ""

#: txtoutput.php:49
msgid "<h6>Gist translation and tags</h6>"
msgstr ""

#: txtoutput.php:52
msgid "Right-click and select \"Save\" to download "
msgstr ""

#: txtoutput.php:54
msgid "the glossed/tagged text"
msgstr ""

#: index.php:52 index.php:48 index2.php:45
msgid "<h2 class=\"center\">Gloss parts of speech in Welsh</h2>"
msgstr ""

#: index.php:144 index.php:140
msgid "<h4 class=\"center\">Background</h4>"
msgstr ""

#: index.php:146 index.php:142 index.php:141
msgid ""
"<p>Autoglosser2 is a glosser/tagger for Welsh, using the <a href=\"http://"
"eurfa.org.uk\">Eurfa</a> Welsh dictionary.  The Autoglosser2 code (GPL) is "
"in a <a href=\"https://donnek@bitbucket.org/donnek/autoglosser2.git"
"\">Bitbucket repository</a>, and a detailed <a href=\"./resources/manual.pdf"
"\">manual</a> is available.  Autoglosser2 is a heavily revised version of "
"the <a href=\"https://github.com/donnekgit/autoglosser\">Bangor Autoglosser</"
"a>, which was developed to gloss the <a href=\"http://bangortalk.org.uk"
"\">Bangor corpora</a> of multilingual conversational text.  Autoglosser2, on "
"the other hand, is aimed at written rather than spoken Welsh text, and has "
"been refactored to tidy the code and make it far faster (over 22,000 glosses "
"per minute).</p>"
msgstr ""

#: includes/language-test-php.php:26
msgid "Hello, world!"
msgstr ""

#: includes/main-page-php-clean.php:38
msgid ""
"You can test Autoglosser2 by using the web interface below, which handles up "
"to 300 characters of text (on average, around 50 words) - anything over that "
"will not be processed.  Just type your Welsh text into the box, and press "
"<strong>Gloss it!</strong>. If you can't think of any Welsh to use, try some "
"of the samples below the box.  The glossing works best if you enter \"written"
"\" Welsh, i.e. with correct spelling and punctuation.  Words that are not in "
"Eurfa yet (which may also affect the glossing accuracy) are marked in red.  "
"If you need to tag large amounts of text, it's best to install Autoglosser2 "
"on your local machine - see the <a href=\"./resources/manual.pdf\">manual</"
"a> - Appendix A has full installation instructions.</p>"
msgstr ""

#: main-page-php-clean.php:40 main-page-php-clean.php:31
msgid "This is an important issue."
msgstr ""

#: main-page-php-clean.php:42 main-page-php-clean.php:33
msgid "Test whether Lokalize can see this."
msgstr ""

#: main-page-php-clean.php:44 main-page-php-clean.php:35
msgid "Test whether GTrans can see this."
msgstr ""

#: index.php:44
msgid "<h2>Gloss parts of speech in Welsh</h2>"
msgstr ""

#: index.php:139
msgid "<h4>Background</h4>"
msgstr ""
