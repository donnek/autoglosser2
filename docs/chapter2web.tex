\chapter{The Autoglosser2 web interface}
\label{ch:webint}


\section{Introduction}

The quickest way to explore the Autoglosser2 is to use the web interface.  If you have large amounts of text to convert, or you wish to integrate the glosser into a language processing workflow, you should use the command-line glosser -- see \Cref{ch:pipeline}.  However, the web interface can be useful for testing shorter stretches of text when you wish to develop or fine-tune constraint grammar rules (\Cref{ch:cg}) or want to check whether particular words are in Eurfa (\Cref{ch:eurfa}).

An online instance of the web interface is available at \url{autoglosser.org.uk}.  If you have followed the installation instructions in \Cref{s:localaccess}, this interface will also be available to you on your own machine at the address \textit{http://autoglosser2}.  To use the interface, type or paste text into the input box.\footnote{Note that cutting and pasting from pdfs can often lead to an issue where the accent is pasted in as a separate glyph, and the ``accented'' letter, although it looks OK, will not be found on lookup.  If you are find that accented words are being tagged as unknown, try deleting the ``accented'' letter from the input and typing it in manually.} Input is truncated to 300 characters, but if your text is longer than this you can tag it in chunks (see also \Cref{s:webinterface}).

Once your Welsh text is in the input box, press the button marked \textit{Gloss it!}.  This will segment the text into sentences, split each sentence into words, and begin the process of glossing (described in detail in \Cref{ch:pipeline}).  The output of the glossing process will be shown on the right-hand side of the browser window.


\section{Output options}

\subsection{Default gloss output}
\label{ss:web-siarad-hor}

Ticking the radio button \textit{Glosses, horizontal layout} gives the default output, consisting of \textit{Siarad}-type glosses (English lemma followed by POS tags) in a tiered format.  For example, \textit{droedio} would be tagged \textit{tread.V.INFIN+sm}, meaning that it is a verbal infinitive (sometimes called a verbal noun), bearing soft mutation, and corresponding to the English lemma ``tread''.  A full list of gloss components is given in \Cref{appA}.

\begin{figure}[H]
\centering
\includegraphics{images/web-siarad-hor.png}
\caption{Default web-interface output -- horizontal glosses}
\label{fig:web-siarad-hor}
\end{figure}

Sample output (meaning ``Lois is making a cake'') is given in \Cref{fig:web-siarad-hor}.  Each input sentence is numbered, and below it in square brackets there is a gist (Wenglish) translation, which consists simply of a concatenation of the English lemmas for each word in the sentence.  Below that there are two tiers giving the words in the text (with its location in the sentence marked via a superscript numeral), with the gloss below each.

Although not depicted here, in this and the other layouts any words that are undisambiguated (i.e. more than one part of speech could apply to them) or words that are unknown to Eurfa (i.e. need to be added to the dictionary) are marked in red.

A text file (ending in \textit{_txtoutput.txt}) is also generated in the background, which may be useful for further processing.  To access this, right-click the link at the top of the output area, and save the file.  Note that if you click the link the file will open in a new browser window, but depending on the cache settings in your browser, the file that opens may not be the most recently-generated version, so right-clicking and saving is the best way to access the newest file.

The default output file is in a horizontal format, with four lines for each sentence: the (numbered) sentence, the gist translation, the tagged words, and an empty delimiter line -- see \Cref{fig:web-siarad-hor-file}.  Each tagged word consists of a number in braces giving the sentence and the word's location in the sentence, the word, a hash symbol (\#), and a gloss.

\begin{figure}[H]
\verb|(1) Mae Lois yn gwneud cacen.|\\
\verb|[is Lois  make cake . ]|\\
\verb|{1,1}Mae#is.V.3S.PRES {1,2}Lois#Lois.NAME {1,3}yn#PRT {1,4}gwneud#make.V.INFIN|\\
\verb|     {1,5}cacen#cake.N.F.SG {1,6}.#PUNC.FS|\\
\verb| |
\caption{Default web-interface output file -- horizontal glosses.}
\label{fig:web-siarad-hor-file}
\end{figure}

If there is no need to tag punctuation, this can be switched off in the web interface by ticking the box \textit{In glosses, don't tag punctuation}.  Both web and file output will then filter out punctuation glosses, leaving only the punctuation mark itself.

\subsection{Vertical gloss output}
\label{ss:web-siarad-ver}

Ticking the radio button \textit{Glosses, vertical layout} gives a variation on the default where the tagged words each appear on their own line, with the word preceded by sentence and location information in brackets, and followed by a hash and a gloss, again with an empty delimiter line between each sentence -- see \Cref{fig:web-siarad-ver}.

\begin{figure}[H]
\centering
\includegraphics{images/web-siarad-ver.png}
\caption{Vertical glosses output.}
\label{fig:web-siarad-ver}
\end{figure}

The equivalent output file follows the same pattern, except that the location information is in braces -- see \Cref{fig:web-siarad-ver-file}.  As with the default layout, punctuation glossing can be switched off by ticking the box \textit{In glosses, don't tag punctuation}.

\begin{figure}[H]
\verb|(1) Mae Lois yn gwneud cacen.|\\
\verb|[is Lois  make cake . ]|\\
\verb|{1,1} Mae#is.V.3S.PRES|\\
\verb|{1,2} Lois#Lois.NAME|\\
\verb|{1,3} yn#PRT|\\
\verb|{1,4} gwneud#make.V.INFIN|\\
\verb|{1,5} cacen#cake.N.F.SG|\\
\verb|{1,6} .#PUNC.FS|\\
\verb| |
\caption{Vertical glosses output file.}
\label{fig:web-siarad-ver-file}
\end{figure}

\subsection{Horizontal tag output}
\label{ss:web-corcencc-hor}

The Eurfa dictionary also includes CorCenCC-type tags for each entry, so ticking the radio button \textit{CorCenCC tags, horizontal layout} will print CorCenCC tags instead of \textit{Siarad}-type glosses for each word in the sentence. A full list of CorCenCC tags is given in \Cref{appC}.

The web output (see \Cref{fig:web-corcencc-hor}) each input sentence is numbered, with a gist translation below it in square brackets.  Below that, each word in the sentence is preceded by its location, and followed by a hash and a tag.

\begin{figure}[H]
\centering
\includegraphics{images/web-corcencc-hor.png}
\caption{Horizontal tags output.}
\label{fig:web-corcencc-hor}
\end{figure}

The equivalent output file is in a similar horizontal format, with four lines for each sentence: the (numbered) sentence, the gist translation, the tagged words, and an empty delimiter line -- see \Cref{fig:web-corcencc-hor-file}.  Each tagged word consists of a number in braces giving the sentence and the word's location in the sentence, the word, a hash, and a tag.

\begin{figure}[H]
\verb|(1) Mae Lois yn gwneud cacen.|\\
\verb|[is Lois  make cake . ]|\\
\verb|{1,1}Mae#Bpres3u {1,2}Lois#Ep {1,3}yn#Utra {1,4}gwneud#Be {1,5}cacen#Ebll {1,6}.#Atdt|
\verb| |
\caption{Horizontal tags output file.}
\label{fig:web-corcencc-hor-file}
\end{figure}

\subsection{Vertical tag output}
\label{ss:web-corcencc-ver}

Ticking the radio button \textit{CorCenCC tags, vertical layout} produces a vertical layout similar to the vertical gloss layout, but using CorCenCC tags.  The tagged words each appear on their own line, with the word preceded by sentence and location information in brackets, and followed by a hash and a gloss, again with an empty delimiter line between each sentence -- see \Cref{fig:web-corcencc-ver}.

\begin{figure}[H]
\centering
\includegraphics{images/web-corcencc-ver.png}
\caption{Vertical tags output.}
\label{fig:web-corcencc-ver}
\end{figure}

The equivalent output file follows the same pattern, except that the location information is in braces -- see \Cref{fig:web-corcencc-ver-file}.

\begin{figure}[H]
\verb|(1) Mae Lois yn gwneud cacen.|\\
\verb|[is Lois  make cake . ]|\\
\verb|{1,1} Mae#Bpres3u|\\
\verb|{1,2} Lois#Ep|\\
\verb|{1,3} yn#Utra|\\
\verb|{1,4} gwneud#Be|\\
\verb|{1,5} cacen#Ebll|\\
\verb|{1,6} .#Atdt|\\
\verb| |
\caption{Vertical tags output file.}
\label{fig:web-corcencc-ver-file}
\end{figure}
