\chapter{Customising Autoglosser2}
\label{ch:customise}


\section{Introduction}

This chapter summarises areas where you can customise or improve the output of Autoglosser2 for your own needs.\footnote{Bear in mind that Autoglosser2 is licensed under the GPL, so if you ``distribute'' the revised code in any way you need to make that revised code available somewhere for download.}  This is apart from aspects such as improving or adding to the number of constraint grammar rules (\Cref{ch:cg}), or expanding Eurfa (\Cref{ch:eurfa}).


\section{Web interface}
\label{s:webinterface}

The default web interface has a truncation limit of 300 characters.  If you are running Autoglosser2 on a local webserver, you can change this limit by finding this line in \textit{postag_welsh.php}:

\verb|$instring=strip_tags(substr($instring, 0, 300));|

\noindent and editing it to increase the figure of 300, or commenting it out entirely (by placing two slashes -- // -- at the beginning) to remove any length limit on the text.

The default tiered output of the web interface is 4 words wide.  If you want to take advantage of a wider screen, this can be adjusted by finding this line in \textit{txtoutput.php}:

\verb|$chunks=array_chunk($values, 4, true);|

\noindent and changing ``4'' to an appropriate number.  Bear in mind that you may also need to change the CSS rules governing the layout of the page (\url{style.css}).


\section{Import}
\label{s:6import}

The segmentation process in \url{import.php} sets an end-of-sentence marker, and then reverts it in particular cases (e.g. abbreviations, acronyms, instances where the sentence-end comes within quote-marks).  

Common abbreviations  (e.g. Dr., Ltd., Jan.) are marked so that their final fullstops will not be mistaken as a sentence ending.  The list is contained in the \textit{mark_abbrevs()} function in \textit{includes/fns.php}, and can be extended as required.


\section{Tokenisation}

The tokenisation process in \url{wordify.php} is based on surrounding all non-alphanumeric characters with spaces, and then reverting those spaces in particular cases.\footnote{My current view is that off-the-shelf tokenisers, many of which require the installation of sizeable libraries (9Mb in one case) do not offer substantially better results than the (8Kb) approach adopted here, but I remain open to persuasion.}

Hyphenated words need special handling, relating especially to the ways in which they may be capitalised.  For instance, \textit{cyd-destunoli} (contextualise) would usually be capitalised as \textit{Cyd-destunoli}, but in headings may also be capitalised as \textit{Cyd-Destunoli}.  The tokenisation process joins uncapitalised post-hyphen items to capitalised or uncapitalised pre-hyphen components, so in this case \textit{destunoli} will be joined to the component \textit{Cyd}, and the whole hyphenated word will then be looked up in Eurfa as \textit{cyd-destunoli}.  This approach allows compounds such as \textit{Llafur-Plaid} (Labour-Plaid [Cymru]) or \textit{Abertawe-Caerdydd} (Swansea-Cardiff) to be left unjoined, with each side of the compound being looked up individually.  However, this also means that \textit{Cyd-Destunoli} will be treated as a compound, and the lookup will fail or be incorrect.  To handle this, a specific subset of components (e.g. \textit{cyd-}, \textit{di-}, \textit{ôl-}) is listed in the \textit{hyphcomp()} function in \textit{includes/fns.php}, and can be extended as required.


\section{Cohort generation}

Lookup routines for different languages can be added to \url{cohorts.php}, either replacing \url{lookups/cym_lookup,php}, or augmenting it with lookups for different languages.  The Bangor Autoglosser used the latter approach, with the dictionary to be looked up depending on the language tag assigned to the word by the transcriber of the conversation, but it would also be possible simply to look up the word successively in all the dictionaries.  This is done simplistically in Autoglosser2, where an English wordlist (\textit{saesneg})\footnote{Based on Alan Beale's 12dicts 5d+2a list -- \url{wordlist.aspell.net/12dicts-readme/#internat.}} is looked up as part of \url{lookups/cym_lookup,php} to identify possible English words, but unlike the fuller lookup in the Bangor Autoglosser, no parts of speech are given for those words.  This behaviour can be adjusted by augmenting the English dictionary to include parts of speech (for instance, by swapping in the one from the Bangor Autoglosser).

What each lookup file does can also be customised.  For instance, the URL identification in the default Welsh lookup is simplistic, and only covers the most basic types of URL.

The layout of the cohorts file is also variable within limits -- the constraint grammar parser offers some flexibility as regards the format of the file it will accept.  This means that data (eg location data) can be written to that file for use outside constraint grammar itself. 


\section{Gloss generation}

The data in the \url{_cgfinished} table can be formatted in a variety of ways, so it is possible to generate glosses or tags in a particular format to suit your application.  

If you need a completely different tagset, that can be created by adding another column to Eurfa and adding the tags programmatically to each word (as was done for the CorCenCC project).  They can then be used in the pipeline at the cohort generation stage.


\section{Pdf output}

The colours for the glosses or tags can be edited in \textit{pdfoutput.php}:

\verb|$gcolour="Blue";| \\
\verb|   $tcolour="Green";|

The output \textit{.tex} file includes Uwe Kern's \textit{xcolor} package,\footnote{\url{ctan.org/pkg/xcolor}} so a wide range of colours is available, as listed in Section 4 of the \textit{xcolor} manual.\footnote{\url{mirrors.ctan.org/macros/latex/contrib/xcolor/xcolor.pdf}}

John Frampton's ExPex package\footnote{\url{ctan.org/pkg/expex}} is used for the alignment of the glosses or tags with the original text.  This package offers many alternatives, and since the glossing data is contained in a database table it can be output programmatically in various different formats to leverage those alternatives.

%\footnote{For some examples of this in the context of Chinese, see the Qiezi website, \url{qiezi.org.uk}.}

LaTeX in general offers a multitude of possibilities for elegant typesetting, so there are many ways in which the glossed data can be presented to readers in an attractive format, or used in academic papers or books -- for a demonstration of its versatility outside the field of part of speech tagging, see \citet[Chapter 8]{Donnelly2017}.


\section{Txt output}

The web and text output can be adjusted to suit your own needs, and the text output can be ingested directly by other programs, so that (for instance) syntactical structure or semantic domain can be further investigated.\footnote{Of course, it is also possible to manipulate the database tables directly to create new tables for particular purposes -- see, for instance, \citet{Broersma2018} and \citet{Deuchar2016}.}

XML output is also possible, but is not offered by default, largely because there is no consensus on what a ``canonical'' XML file for NLP work should look like.\footnote{In my view, there are other reasons to avoid using XML in NLP work in any case.}


\section{Contributing}

If you would like to improve Autoglosser2, I would be happy to take suggestions on board -- fork the repo, make your changes, and then submit a pull request, or send me your code directly (kevin@dotmon.com).  The coding style is pretty obvious: blank lines between ``sections'' of code, plenty of comments, and NO brace at the end of lines -- put them on a new line.  I also prefer procedural style with functions rather than object-oriented, but I'm happy to be convinced otherwise.

