\chapter{Constraint grammar}
\label{ch:cg}

\section{Introduction}

\begin{quotation}
``Constraint Grammar (CG), launched by Fred Karlsson in 1990, is a methodological paradigm for natural language processing (NLP). Context-dependent rules written by a human linguist are compiled into a grammar that assigns grammatical tags to words or other tokens in running text.

``Typical tags address lemmatisation (lexeme or base form), inflexion, derivation, syntactic function, dependency, valency, case roles, semantic type etc. Each rule adds, removes, selects or replaces a tag or a set of grammatical tags in a given sentence context.''

\hfill From: \url{en.wikipedia.org/wiki/Constraint_Grammar} (slightly edited)
\end{quotation}

Different versions of the constraint grammar formalism have been developed over the years.  Karlsson's version \citep{Karlsson1990, Karlsson1995}  is now referred to as CG-1, and a later version that made some changes was known as CG-2.

The version of constraint grammar used by Autoglosser2 is the CG-3 variant, developed as part of the University of Southern Denmark's Visual Interactive Syntax Learning project\footnote{\url{visl.sdu.dk}} by Eckhard Bick and Tino Didriksen \citep{Bick2009, Bick2015, Didriksen2017}.  CG-3 is the most featureful and versatile variant, and the only one under current development.  It is free software, licensed under the GPL, and is being used in a number of NLP projects, including Apertium, a free (GPL) machine translation system.\footnote{\url{apertium.org}}

\section{Key attributes of CG-3}

The CG-3 constraint grammar parser applies rules in a ``grammar'' file (in Autoglosser2, \url{grammar/cym_grammar.cg3}) to text input in a specific format (in Autoglosser2, the file \url{outputs/atext/atext_cg.txt} created by \url{cohorts.php}).  The rules are applied sequentially to each cohort of POS options (readings) for a particular word until only one candidate reading is left, and can be applied in batches if desired.  The rules choose (select), remove, or add morphological, syntactic, semantic or other readings in particular contexts conditioned by surrounding words or tags.  A particular strength of constraint grammar is that it can assign readings even to unconventional (non-standard) language such as the conversations in the Bangor ESRC corpora, and can handle codeswitching well \citep{Donnelly2011a}.  Disambiguation (reduction of the readings to one correct POS option) regularly reaches 99\%, and impressive results can be obtained with a grammar of as few as 200 rules.  Grammar size can vary substantially, depending on language and tag complement -- one French grammar has 1,400 rules, while a Danish grammar has 8,000.

A rules-based system like constraint grammar differs significantly from the current focus on machine learning, where the idea is that if a computer can be given enough text it will be able to increasingly accurately extract such things as tags.  Rule-based systems tend to require  linguistic knowledge rather than heuristic data-analysis -- for this reason, they may take longer to develop, but may be the only viable option where large amounts of digital text are not available (for instance, in the case of minority languages).  They are also likely to give more accurate results over different language registers, and do not suffer from the possibility of implicit bias, where racial, gender and other biases in linguistic corpora carry over into machine learning output \citep{Caliskan2017}.


\section{Constraint grammar rules}

\citet{Donnelly2010} gives a general introduction to constraint grammar, and further details can be found in \citet{Bick2009}, \citet{Bick2015} and \citet{Didriksen2017}.  This section will give a short overview of the grammar file \url{grammar/cym_grammar.cg3}, so that users can experiment with writing their own rules or amending existing ones.

\subsection{Preamble}

At the head of the grammar file, the \textit{DELIMITERS} command specifies the items considered to mark the end of an utterance (sentence).  This is followed by a group of \textit{LIST} and \textit{SET} definitions, which create sets and combine them respectively.  These can be used to simplify the writing of the rules, but not all of them are used in the remainder of the file.

\subsection{Rule sections}

The rules begin after the \textit{SECTION} command.   There can be multiple rule sections, and they can be run sequentially, or in isolation, or in repeated groups. Possible reasons for multiple sections might be to separate "safe" rules (to be used earlier) from rules of thumb (to be used later), or to separate rules with different areas of application (for instance, rules that act on one language in the text from those that act on another language). This grammar file currently has two sections: the main one and an experimental  section near the end which adjusts the lemmas.  

\subsection{Rule application}

Rules are always applied in the order in which they occur in the grammar file, and any given rule in a section will be applied to all cohorts in the sentence before moving on to the next rule in that section.  Each section is applied iteratively.

A rule can act on any tag anywhere in a cohort reading -- in other words, tags do not need to be in a particular sequence or order.  Each rule is run against each cohort, to see if there is a reading in the cohort to which it should be applied.

Each rule ends with a semicolon (;), and begins with a keyword such as \textit{SELECT}, \textit{REMOVE} or \textit{SUBSTITUTE}.  By convention, these are written in upper-case, but they also work if written in lower-case, and this grammar file uses that option.\footnote{I find the rules easier to read in lower-case, but others may disagree.}  There are many other keywords, but these three are the only ones used in this grammar file. 

\subsection{Rule targets}

Following the keyword is the \textit{TARGET}, i.e. the word on which the rule will act.   Targets can be specified as sets, or as tags, lemmas (enclosed in double quotes), or surface words (enclosed in angle brackets and double quotes).  The last three must also be enclosed in parentheses.  So: \\
\verb|    select smtrigger if ... | \\
would act on any words in the set of words that has been defined as triggering soft mutation. \\
\verb|    select (n f) if ... | \\
would act on any words which are tagged as feminine nouns. \\
\verb|    select ("coes") if ... | \\
would act on any word whose lemma is \textit{coes}, i.e. \textit{coes}, \textit{goes}, \textit{nghoes}, \textit{coesau}, \textit{goesau}, \textit{nghoesau}, etc. \\
\verb|    select ("<goes>") if ... | \\
would act on any instance of \textit{goes}, but not on related forms such as \textit{coes}, \textit{nghoesau}, etc.  Note that the \textit{if} is included in this file for readability, but is optional -- the rules will work without it.

\subsection{Rule contexts}

The target can be followed by a set of contexts or conditions under which the rule will apply.  If more than one context is specified, all of them must be met (instantiated) before the rule will apply.   Contexts must be individually enclosed in parentheses, and usually consist of a position marker and a tagset.  A positive position marker refers to text to the right (coming after the target), while a negative position marker refers to text to the left (coming before the target).  A context can be negated by using \textit{NOT} in front of the position marker.  

If no context is specified, the rule will apply globally -- this can be used to suppress rarely-occurring reading.  For instance, the following rule will delete all readings containing a subjunctive verb, no matter what context they occur in: \\
\verb|    remove (v subj);| \\
This can also be used as a shortcut to select one meaning of a word. For instance \textit{ysgol} can mean ``school'' and also ``ladder'', but the first meaning is much more frequent.  Ideally, semantic context would be used to determine which reading to choose, but until that is possible a rule can be defined that will always choose the "school" meaning of the \textit{ysgol} lemma:\footnote{Obviously, this will give an incorrect gloss when the ``ladder'' meaning is actually intended!} \\
\verb|    select ("ysgol" :school:);| \\
or ban the "ladder" meaning: \\
\verb|    remove ("ysgol" :ladder:);|\footnote{Note that in the output from \url{cohorts.php}, the English lemma is surrounded by colons, and they need to be replicated in the rule.} \\
This sort of global rule is used in the first few parts of this grammar file, to select most frequent lemmas or delete rare ones, and also to remove incorrect readings, e.g. incorrect depluralisations (such as \textit{llewys}, ``sleeves'' being read as a plural of \textit{llew}, ``lion''), or impossible mutations (such as \textit{na}, ``than'', being read as a nasally-mutated form of \textit{da}, ``good''),


\section{Some rule examples}

\subsection{SELECT rules}

\noindent \verb|select ("a" conj) if (-1 (name)) (1 (name));| \\
\hangindent=1.5em Select the reading of \textit{a} (and) as a conjunction if the word to the left (\textit{-1}) and the word to the right (\textit{1}) are both names, as in \textit{Conwy a Gwynedd} (Conwy and Gwynedd).

\noindent \verb|select ("a" pron.rel) if (1 (subj));| \\
\hangindent=1.5em Select the reading of \textit{a} (who, which) as a relative pronoun if the following word is in the subjunctive tense, as in \textit{doed a ddelo} (come what may).

\noindent \verb|select ("a" prt.int) if (1 (pres.indef));| \\
\hangindent=1.5em Select the reading of \textit{a} as an interrogative particle if the following word is in a present indefinite verb, as in \textit{a oes heddwch?} (is there peace?).

\noindent \verb|select (prt.aff) if (not -1 (prep)) (1 inflected);| \\
\hangindent=1.5em Select the reading of \textit{mi} as an affirmative particle if the following word is an inflected verb, but not if the preceding word is a preposition.  This will apply to \textit{mi welodd} (he saw), but not to \textit{wrth i mi gyrraedd} (as I arrived).

\noindent \verb|select ("mi" pron) if (-1 ("i" prep));| \\
\hangindent=1.5em Select the reading of \textit{mi} (me) as a pronoun if the preceding word is the lemma \textit{i} used as a preposition, as in \textit{i mi} (to me).

\noindent \verb|select ("â" conj) if (-1 (adj.eq));| \\
\hangindent=1.5em Select the reading of \textit{â} (as) as a conjunction if the preceding word is an equative adjective, as in \textit{cyn belled â} (as far as).

\noindent \verb|select ("â" prep) if (1 (det.def) or (n) or (name)) (not -1 (adj.eq));| \\
\hangindent=1.5em Select the reading of \textit{â} (with) as a preposition if it is preceded by the definite article, a noun or a name, and not followed by an equative adjective, as in \textit{aeth taid â'r ddwy ferch} (grandfather went with both girls) or  \textit{ffinio â Lloegr} (bordering [with] England).

\noindent \verb|select ("yn" prt) if (1C (adj));| \\
\hangindent=1.5em Select the reading of \textit{yn} as a particle if the following words is definitely (\textit{C} for certain or careful) and adjective, as in \textit{yn hapus} (happy).

\noindent \verb|select ("yn" prep) if (1 (det.def));| \\
\hangindent=1.5em Select the reading of \textit{yn} (in) as a preposition if the following word is a definite article, as in \textit{yn y tŷ} (in the house).

\noindent \verb|select ("o" :of:) if (1 (det.def) or (adj.poss));| \\
\hangindent=1.5em Select the reading of \textit{o} (of, from) as a preposition if the following word is a definite article or a possessive adjective, as in \textit{o'r môr} (from the sea) or \textit{o'n ffrindiau} (from our friends).

\noindent \verb|select ("fo" pron) if (-1 (infin)) (1 (prt)) (2 (adj));| \\
\hangindent=1.5em Select the reading of \textit{o} (< \textit{fo}, ``he, him'') as a pronoun if the preceding word is a verbal infinitive, the following word is the particle \textit{yn}, and the word after that is an adjective, as in \textit{i wneud o'n saff} (to make it/him safe).

\noindent \verb|select ("cyn" prep) if (not 1 (adj.eq));| \\
\hangindent=1.5em Select the reading of \textit{cyn} (before) as a preposition if the following word is not an equative adjective, as in \textit{cyn mynd} (before going).

\noindent \verb|select ("deg" num) if (1 (n));| \\
\hangindent=1.5em Select the reading of \textit{deg} (ten) as a numeral if the following word is a noun, as in \textit{deg awr} (ten hours).

\noindent \verb|select ("pryd" :meal:) if (1* ("bwyd"));| \\
\hangindent=1.5em Select the reading of \textit{pryd} with the English lemma ``meal'' if the lemma \textit{bwyd} (food) follows (\textit{*}) anywhere, as in \textit{pryd o fwyd} (a meal).

\noindent \verb|select ("pryd" :time:) if (-1 ("pa" :which:));| \\
\hangindent=1.5em Select the reading of \textit{pryd} with the English lemma ``time'' if preceded by the lemma \textit{pa} with the English lemma ``which'', as in \textit{pa bryd?} (when?).

\noindent \verb|select ([eng]) if (-1 ([eng])) (1 ([eng]));;| \\
\hangindent=1.5em Select the English reading if the word is preceded and followed by English words.

\subsection{REMOVE rules}

My suggestion is that \textit{REMOVE} rules should be avoided unless the removal context is bullet-proof -- they can have unintended consequences if the context is not carefully defined, and in most cases \textit{SELECT} rules are to be preferred.

\noindent \verb|remove ("yn" prep) if (1 (sm));| \\
\hangindent=1.5em Remove the reading of \textit{yn} (in) as a preposition if it is followed by a word showing soft mutation -- the preposition \textit{yn} is followed by nasal mutation.

\noindent \verb|remove (prt.aff) if (-1 inflected);| \\
\hangindent=1.5em Remove the reading of \textit{mi} (in) as an affirmative particle if it is preceded by an inflected verb, as in \textit{sgiwsiwch fi} (excuse me).

\noindent \verb|remove ([eng]) if (0 ([cym]));| \\
\hangindent=1.5em Remove an English reading if a Welsh one exists for the current word (\textit{0}).

\subsection{SUBSTITUTE rules}
\label{ss:substitute}

Rules applying a substitution have four parameters: the items that are to be changed, the items they are to be changed to, the context in which those items occur, and the context in which they should be changed.  To paraphrase: replace A with B in the context X when conditions Y apply.\footnote{The keyword is actually \textit{SUBSTITUTE}, but the most common English construction here is ``substitute A for B'', which implies ending up with A; the CG-3 authors are using the less common construction ``substitute A with B'', which implies ending up with B, like the more frequent ``replace A with B''.  This is because \textit{REPLACE} is already being used as a keyword to replace individual tags in a reading.}   Autoglosser2 is currently only using SUBSTITUTE rules experimentally to adjust the English lemma in the glosses.

\noindent \verb|substitute (:class:) (:district:) ("dosbarth" [cym] n m sg :class:) if (-1 ("cyngor"));| \\
\hangindent=1.5em Replace the English lemma ``class'' with ``district'' when the lemma is \textit{dosbarth} and the preceding word is \textit{cyngor} (council), as in \textit{cyngor dosbarth} (district council).

\noindent \verb|substitute (adj :other:) (adv :else:) ("arall" adj :other:) if (-1 ("rhywbeth") or ("rhywle"));| \\
\hangindent=1.5em Replace the English lemma ``other'' with ``else'', and change the part of speech from adjective to adverb, when the lemma is \textit{arall} and the preceding word is \textit{rhywbeth} (something) or \textit{rhywle} (somewhere), as in \textit{rhywbeth arall} (somewhere else).

\noindent \verb|substitute (:all:) (:completely:) ("cwbl") if (1 (adj));| \\
\hangindent=1.5em Replace the English lemma ``all'' with ``completely'' when the lemma is \textit{cwbl} and the preceding word is an adjective, as in \textit{cwbl sarhaus} (completely insulting).


\section{Contributing}

The results from this Autoglosser2 grammar file are good, but still at an early stage.  As it currently stands, the rules are fairly basic -- they could be improved or made more abstract using sets.  More might also be done to group rules so that rule order has no effect.  Further work along these lines would help contribute to a useful set of NLP tools for Welsh, e.g. dependency trees, syntactic analysis, etc.  If you would like to improve things, I would be happy to take suggestions on board -- fork the repo, make your changes, and then submit a pull request, or send me your revised grammar file directly (kevin@dotmon.com).
