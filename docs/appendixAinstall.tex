\chapter{Installing Autoglosser2}
\renewcommand{\thesection}{A/\arabic{section}}  % redefine the section numbering
\setcounter{section}{0}  % reset counter
\label{appA}

\setlength{\parindent}{0in}  % no paragraph indents

\section{Introduction}

This appendix explains how to install from scratch the software that Autoglosser2 needs.  The installation has been tested on Ubuntu Linux 14.04 and 16.04,\footnote{\url{ubuntu.com}} and it should run on other platforms in a virtual machine.\footnote{e.g. \url{virtualbox.org}}  If you are having difficulties getting something installed, please email me at kevin@dotmon.com -- although I have tried the instructions below on a ``clean'' machine, I may nevertheless have missed something, or got the sequence wrong.

Most of the install is carried out by typing commands directly into a terminal or console.  This is because this method is much faster and more succinct than explaining how to point-and-click through various dialogue boxes.


\section{Conventions}

Unless otherwise indicated, lines in\\
\hphantom{space} \verb|monospaced font|\\
are commands to be typed in.  

Unless otherwise indicated, all commands should be activated by pressing \textit{Return} at the end of the command.

The symbol $\hookrightarrow$ at the beginning of a line means that it is a continuation of the previous command, and therefore \textit{Return} should only be pressed after the end of this line.

Keys separated by + should be pressed simultaneously.  Thus \textit{Ctrl+X} means ``press the Ctrl key at the same time as the X key''.

When a command starts with \textit{sudo}, you will be asked to type in your superuser (administrative) password, which you should have been asked to set up when you first installed Ubuntu, before the command is allowed to proceed.  Note that you will get no feedback from the password entry (the line will stay blank) until you press \textit{Return}.

If at any point the system suggests adding other packages (called \textit{dependencies}) based on the ones you are installing, accept those suggestions by pressing \textit{Y} or typing \textit{yes}.

Unless otherwise indicated, it is assumed that all commands are run from the suggested base directory for Autoglosser2, \textit{/var/www/autoglosser} -- see Section \ref{s:download}.


\section{Download Autoglosser2}
\label{s:download}

The main reason you're here! First, install Git, which keeps track of changes made to files:\\
\verb|    sudo apt-get install git| \\
and then move to your home directory (USER here stands for the username you set up when you installed Ubuntu; replace it with your actual username),\footnote{The terminal prompt will tell you what your username is -- it is of the form \textit{user@computer}.} and download Autoglosser2:\\
\verb|    cd /home/USER|\\
\verb|    git clone https://bitbucket.org/donnek/autoglosser2.git| \\
After a minute or two, Autoglosser2 will be downloaded into an \textit{autoglosser2} folder in \textit{/home/USER}.  In the future, if you want to update Autoglosser2, you can open a terminal in the \textit{autoglosser2} folder and type:\\
\verb|    git pull|\\
Git will automatically update those parts of Autoglosser2 which have been changed.

To allow for the web interface (for configuration, see \Cref{s:localaccess}), move the \textit{autoglosser2} directory to \textit{/var/www}, the default location for storing webpages on Ubuntu:\\
\verb|    sudo mv autoglosser2 /var/www/|\\
Note the final slash.  Give yourself (USER stands for your own username) ownership of the \textit{/var/www} directory;\\
\verb|    sudo chown -R USER.USER /var/www/| \\
and then set up a link from your \textit{/home/USER} directory to the \textit{/var/www/} directory:\\
\verb|    ln -s /var/www/ web| \\
Now when you go to \textit{/home/USER/web} in a file manager, it will take you to \textit{/var/www/}.  Move into the \textit{autoglosser2} directory for the rest of the installation:\\
\verb|    cd web/autoglosser2|


\section{Download Eurfa}
\label{s:eurfadl}
Eurfa provides a dictionary giving part of speech information about Welsh words in your text: \\
\verb|    git clone https://bitbucket.org/donnek/eurfa.git| \\
These files will be installed later as database tables (see \Cref{s:ag2db}).


\section{Install fonts}
\label{s:fonts}

These fonts are used in the pdf output: \\
\verb| sudo apt-get install fonts-sil-charis fonts-liberation fonts-linuxlibertine|\\
\hphantom{space} $\hookrightarrow$ \verb|fonts-dejavu|


\section{Install Constraint Grammar 3}
\label{s:cg}

CG-3 will apply constraint grammar rules to a file of word cohorts.  First, install wget:\\
\verb|    sudo apt-get install wget| \\
and add the repository signing key to your keyring:\\
\verb|    sudo wget http://apertium.projectjj.com/apt/apertium-packaging.public.gpg| \\
\hphantom{space} $\hookrightarrow$ \verb|-O /etc/apt/trusted.gpg.d/apertium.gpg| \\
Then add the relevant Ubuntu CG-3 repository (replace \textit{DISTRO} with your Ubuntu version, e.g. \textit{trusty}, \textit{wily}): \\
\verb|    echo "deb http://apertium.projectjj.com/apt/nightly DISTRO main" | \\
\hphantom{space} $\hookrightarrow$ \verb|sudo tee /etc/apt/sources.list.d/apertium-nightly.list| \\
and update the repository list: \\
\verb|    sudo apt-get update| \\
so that you can install CG-3 and its integrated development environment (IDE): \\
\verb|    sudo apt-get install cg3 cg3ide|


\section{Install Apache2}
\label{s:apache}

Apache2 is a webserver which displays the Autoglosser2 web interface: \\
\verb|    sudo apt-get install apache2 apache2-utils phppgadmin| \\
Start the webserver:\footnote{If you want to get rid of the (harmless) message ``Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the `ServerName' directive globally to suppress this message'', issue the following commands:\\
\texttt{echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/servername.conf} \\
\texttt{sudo a2enconf servername} \\
\texttt{sudo service apache2 restart}} \\
\verb|    sudo service apache2 start| \\
and then open a web browser (preferably Firefox) and enter:\\
\verb|    http://localhost|\\
into the address bar. A page will open, telling you that Apache2 is installed and working.


\section{Configure the web interface}
\label{s:localaccess}

Tell Apache2 where to find the Autoglosser2 webpages by opening a new configuration file: \\
\verb|    sudo nano /etc/apache2/sites-available/autoglosser2.conf| \\
Nano is a lightweight text editor: use the arrow keys on the keyboard to move around, and the \textit{Home} and \textit{End} keys to move to the beginning or end of a line. Type in the following lines: \\
\verb|    <VirtualHost *:80>| \\
\verb|    ServerName autoglosser2| \\
\verb|    DocumentRoot /var/www/autoglosser2/| \\
\verb|    </VirtualHost>| \\
Save the file: press \textit{Ctrl+X}, then press \textit{Y} to confirm you want to save the modifications, and press \textit{Return} to close the file. Activate the configuration: \\
\verb|    sudo a2ensite autoglosser2|
and restart the webserver:\\
\verb|    sudo service apache2 restart| \\
Tell your web browser that the new website is on your machine (so it will not try to look for it on the web) by opening the \textit{hosts} configuration file:\\
\verb|    sudo nano /etc/hosts| \\
After the line: \\
\verb|    127.0.0.1	localhost| \\
add the following line: \\
\verb|    127.0.0.1	autoglosser2| \\
Then save and exit the configuration file (\textit{Ctrl+X \textrightarrow\ Y \textrightarrow\ Return}), and type: \\
\verb|    http://autoglosser2|\\
into the address bar of your browser.  You will get the Autoglosser2 web interface from \textit{/var/www/autoglosser2}.


\section{Install PHP}
\label{s:php}

PHP is the scripting language used by Autoglosser2: \\
\verb|     sudo apt-get install php5 php5-cli| \\
Once installed, open the PHP command-line configuration file so that you can change some settings: \\
\verb|    sudo nano /etc/php5/cli/php.ini| \\
Press \textit{Ctrl+W}, then type\\
\verb|    max_execution_time|\\
into the searchline and press \textit{Return}. Change the line to read:\\
\verb|    max_execution_time = 300| \\
Press \textit{Ctrl+W} again, and type\\
\verb|    error_reporting| \\
and press \textit{Return}. Change that line to: \\
\verb|    error_reporting = E_ALL & ~E_NOTICE & ~E_DEPRECATED| \\
Scroll down (using the mouse or \textit{down} arrow) to the \textit{display\_errors} line a bit lower down.  Change it to read: \\
\verb|    display_errors = On| \\
Below that there is a \textit{log\_errors} line. Change it to read: \\
\verb|    log_errors = Off| \\
Save and exit the configuration file (\textit{Ctrl+X \textrightarrow\ Y \textrightarrow\ Return})

PHP is an easy computer language to get to grips with, gives immediate results, and works on both the browser and the console, but describing it lies outside the scope of this manual.  There are many tutorial sources on the internet.


\section{Install PostgreSQL}
\label{s:psql}

PostgreSQL is the database that will handle the storage your text data: \\
\verb|    sudo apt-get install postgresql postgresql-client postgresql-common| \\
\hphantom{space} $\hookrightarrow$ \verb|postgresql-contrib php5-pgsql| \\
On Ubuntu, PostgreSQL uses peer authentication by default.  Creating a database user with the same name as your system (Ubuntu) user will therefore mean that you can log in to the database without entering a password.  To set yourself up as the database user, become the \textit{postgres} master user: \\
\verb|    sudo su - postgres|\\
(note the space on either side of the dash).  Enter your superuser (administrator) password.  The prompt will change to \textit{postgres@computer}.  Create a new database user with the same name as your username (USER below):\\
\verb|    createuser -P -s -e USER| \\
Enter a password -- note that you will get no feedback (the line will stay blank).  Press \textit{Return}, and enter the password again.  Press \textit{Return} and you should get a message beginning \textit{CREATE ROLE}, meaning that the new user has been created.  Go back to being your normal user: \\
\verb|    exit|

The Structured Query Language (SQL) used by relational databases like PostgreSQL is very powerful and flexible, but describing it lies outside the scope of this manual.  There are many tutorial sources on the internet.


\section{Configure the database connection}
\label{s:ag2db}

Configure Autoglosser2 to use your new database account by opening the configuration file: \\
\verb|    nano autoglosser2/config.php| \\
and changing:\\
\verb|    user=kevin password=kevindbs|\\
to read:\\
\verb|    user=USER password=yourpassword|\\
(remember to replace USER with your username).  You need to edit two lines (one for the \textit{autoglosser2} database and one for the \textit{eurfa} database), and also edit the password (\textit{PGPASSWORD=}) and username (\textit{-U}) on the third line (the one for the \textit{psql} shell).  Exit the configuration file (\textit{Ctrl+X \textrightarrow\ Y \textrightarrow\ Return}), and then move the configuration data outside the web directory: \\
\verb|    sudo mv autoglosser2/ /opt/| \\
entering your superuser password when asked.  The database connection file is now at \textit{/opt/autglosser2}, which is where the other scripts in Autoglosser2 expect to find it.

Create the \textit{autoglosser2} database to hold the data you want to gloss: \\
\verb|    createdb autoglosser2| \\
and import the English wordlist: \\
\verb|    psql -d autoglosser2 < dbs/saesneg.sql| \\
Create the \textit{eurfa} database to hold the Welsh dictionary: \\
\verb|    createdb eurfa| \\
Import the Eurfa data (downloaded in \Cref{s:eurfadl}) into the database: \\
\verb|    psql -d eurfa < eurfa/eurfa.sql|


\section{Install phpPgAdmin}
\label{s:ppa}

phpPgAdmin is a browser interface to PostgreSQL -- it will make it easier to inspect your database tables: \\
\verb|    sudo apt-get install phppgadmin| \\
Activate the phpPgAdmin configuration file:\\
\verb|    sudo cp /etc/apache2/conf.d/phppgadmin /etc/apache2/conf-enabled/phppgadmin.conf| \\
and then restart the webserver:\\
\verb|    sudo service apache2 restart|

In a web browser, enter: \\
\verb|    http://localhost/phppgadmin|\\
into the address bar.  You should see the phpPgAdmin homepage. On the left side there is a list of servers. Click on \textit{PostgreSQL} and you should get a login form.  Fill in the username and password for PostgreSQL (which you created in \Cref{s:psql}) and click \textit{Login}.  In the left-hand panel you should get a list of your current databases -- there should be three: \textit{autoglosser2}, \textit{eurfa}, and \textit{postgres} (the system database, which you will not be using).  

Click the \textit{+} beside \textit{eurfa} in the left-hand panel.  It should open to show \textit{Schemas, public, Tables} etc. Click on \textit{Tables}. The right-hand panel should now show you the (only) table -- \textit{eurfa} --  inside the \textit{eurfa} database.  Click on \textit{eurfa} and you will see the data fields in that table. To see the contents of the table you can click on the \textit{Browse} button.  To export the table to csv (which can be opened in a spreadsheet), click \textit{Export} in the top button bar.  The \textit{SQL} link at the top right of the phpPgAdmin window can be used to make SQL queries to the database.

You are likely to be using the \textit{autoglosser2} database rather than the \textit{eurfa} database, but the same principles apply there when you want to look at the tables created by the glossing process.

The default session time for phpPgAdmin is set to 24 minutes (1440 seconds), meaning that if you do not use phpPgAdmin for 24 minutes, it will ask you to log in again before you can continue using it. You can change this by opening the PHP web configuration file:\\
\verb|    sudo nano /etc/php5/apache2/php.ini| \\
Press \textit{Ctrl+W}, then type:\\
\verb|    session.gc_maxlifetime| \\
and press \textit{Return}. Change the line to read: \\
\verb|    session.gc_maxlifetime = 144000| \\
This will allow you 40 hours before logging you out, which should be sufficient.


\section{Install SQL Workbench/J}
\label{s:workbench}

SQL Workbench/J is another interface to PostgreSQL -- its main benefit is that a result set can be directly edited to update the data.  It uses Java, and you can begin installing the Oracle version of Java by adding Andrei Alin's repository: \\
\verb|    sudo apt-get install software-properties-common| \\
\verb|    sudo add-apt-repository ppa:webupd8team/java| \\
and updating your software package list: \\
\verb|    sudo apt-get update| \\
Then install the Java installer: \\
\verb|    sudo apt-get install oracle-java8-installer| \\
which installs a script that downloads and installs Java 8 from the Oracle website. You will be asked to accept the Oracle license before the installation begins.  Once installed, runningl:\\
\verb|    java -version|\\
should return some text telling you that the Java version is 1.8.0. Now install software to allow SQL Workbench to connect to the PostgreSQL database: \\
\verb|    sudo apt-get install libpostgresql-jdbc-java| \\
Now you can install SQL Workbench/J itself.  Create a subdirectory for it: \\
\verb|    mkdir sqlworkbench| \\
and then go to the website \textit{sql-workbench.net}, click on the link for \textit{Build 123} (or whatever the current stable version is), and download the generic package.  Save it in the \textit{autoglosser2} directory, and then unzip the download into the new directory:\\
\verb|    unzip -q Workbench-Build123.zip -d sqlworkbench| \\
Make the launch script executable: \\
\verb|    chmod +x sqlworkbench/sqlworkbench.sh| \\
and launch SQL Workbench/J:\footnote{You can make a desktop shortcut or menu entry to make launching SQL Workbench easier.} \\
\verb|    sqlworkbench/sqlworkbench.sh|


\section{Configure SQL Workbench/J}

The first thing you will see is a \textit{Select Connection Profile} box, where you need to add the details of the Autoglosser2 database.  Change \textit{New profile} to read \textit{autoglosser2}.  Click the drop-down arrow on the \textit{Driver} line and select \textit{PostgreSQL}. Click \textit{Yes} when you're asked whether you want to edit the driver definition.

On the \textit{Manage Drivers} popup, click on the red \textit{postgresql} entry already there and then click X to delete it.  Click on the folder icon and navigate to \textit{/usr/share/java/postgresql-jdbc4-9.2.jar} (which you just installed -- \Cref{s:workbench}).  Click \textit{Open}, and then \textit{OK}.  Check that the \textit{URL} line reads: \\
\verb|    jdbc:postgresql://localhost:5432/andika| \\
If not, edit it to make it so. Enter your PostgreSQL username and password (Section \ref{s:psql}), and then click \textit{OK}.  You will get a connecting message.

If you like, you can also set up a connection to the Eurfa database, which will allow you to add new entries and edit existing ones.  Press Alt+C to bring up the \textit{Select Connection Profile} box again.  Click the first button in the left-hand panel (\textit{Create a new connection profile}), and change \textit{New profile} to read \textit{eurfa}.  Then proceed as for the \textit{autoglosser2} database.

Once both connection profiles are set up, select \textit{File} \textrightarrow\ \textit{Save Profiles} to save them.


\section{Install LaTeX}
\label{s:latex}

LaTeX is an unbelievably versatile typesetting system that provides attractive pdf output in Autoglosser2: \\
\verb|    sudo apt-get install texlive texlive-base texlive-latex-base texlive-latex-recommended| \\
\hphantom{space} $\hookrightarrow$ \verb|texlive-latex-extra texlive-xetex texlive-generic-extra texlive-humanities| \\
\hphantom{space} $\hookrightarrow$ \verb|texlive-publishers texlive-extra-utils texlive-pstricks texlive-bibtex-extra| \\
\hphantom{space} $\hookrightarrow$ \verb|kile kbibtex biber| \\
Note that these packages will take perhaps 20 minutes to download and install.
