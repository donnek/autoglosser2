\chapter{Eurfa}
\label{ch:eurfa}


\section{Introduction}

Work on Eurfa \citep{Donnelly2016} began in 2003, and the first version was released in 2006. It is the largest Welsh dictionary under a free (GPL) license,\footnote{\url{gnu.org/licenses/gpl.html}} and it was the first dictionary of a Celtic language to list verbal inflections and mutated forms as headwords.  Currently it contains around 10,500 lemmas.   Further information, and a web interface, is available at the Eurfa website,\footnote{\url{eurfa.org.uk}} and the contents of the dictionary are available at the Git repository\footnote{\url{bitbucket.org/donnek/eurfa}} -- see \Cref{s:eurfadl}.   A key aim for Eurfa is that it should be useable in a variety of Welsh NLP situations, so its structure reflects the need for versatility.  This chapter summarises the main features of Eurfa as a component of Autoglosser2. 

Eurfa was used in the first-ever glosser for Welsh conversational text, the Bangor Autoglosser,\footnote{\url{bangortalk.org.uk/autoglosser.php}} developed in 2009-2011 as part of a multilingual spoken corpus project run by the ESRC Centre for Research on Bilingualism at Bangor University.  One of these corpora, the Welsh-English \textit{Siarad} corpus, was the first Welsh corpus to be released under a GPL license, and the same license has been applied to the others (a Welsh-Spanish corpus from Patagonia, and an English-Spanish corpus from Miami).

Eurfa has also been used in two other Welsh NLP projects: the Welsh Natural Language Toolkit\footnote{\url{hypermedia.research.southwales.ac.uk/kos/wnlt}} and the Corpus Cenedlaethol Cymraeg Cyfoes (CorCenCC -- the National Corpus of Contemporary Welsh).\footnote{\url{corcencc.org}}

Eurfa's function in all these software programs is to provide a listing of possible parts of speech for any given word.  This listing can then be pruned to leave (hopefully) one item which applies to the word. 


\section{Structure}

The main elements of the \url{eurfa} database table are exemplified in \Cref{tab:welshdict}.

\begin{table}[H]
\centering
\begin{tabular}{lllllllll}
\textbf{surface} & \textbf{lemma} & \textbf{enlemma} & \textbf{pos} & \textbf{gender} & \textbf{number} & \textbf{tense} & \textbf{corcencc} & \textbf{plural} \\
\hline\noalign{\smallskip}
coes & coes & leg & n & f & sg & & E b u & coesau\\
dyfroedd & dŵr & water & n & m & pl & & E g ll & \\
mynd & mynd & go & v & & & infin & Be & \\
aeth & mynd & go & v & & 3s & past & B gorff 3 u & \\
hapus & hapus & happy & adj & &  & & Ans cad u & \\
rhywsut & rhywsut & somehow & adv & & & & Adf & \\
heb & heb & without & prep & & & & Ar sym & \\
\end{tabular}
\caption{Eurfa structure.}
\label{tab:welshdict}
\end{table}

The structure of Eurfa has changed at various points over the years (as can be seen from its commit history), and it is important to emphasise that this is likely to continue, but the original aim was to create a datasource that was versatile enough to be used in a variety of computerised NLP tasks, but which would be easily editable by non-specialists.  This is why the data is maintained in one database table, rather than using 
multiple tables linked by foreign keys: the layout will be familiar to anyone who has compiled a simple glossary or vocabulary list, and allows the dictionary to be opened in a spreadsheet, if desired, to add new entries or edit existing ones.\footnote{Another benefit of this simple word-based approach is that it is possible to plug a wordlist for any language into Autoglosser2 and get some output immediately (though it will not be disambiguated) – this means that it is easy to get started on producing an autoglosser for a new language.}

The \url{eurfa} database table contains 17 fields, though not all are used for each entry:

\begin{description}
\item[id] A numeric identifier for the entry.
\item[surface] The form of the word as it might appear in running text -- the ``surface'' form derived from the lemma by the addition of various morphemes.
\item[lemma] The underlying (base, stem) form of the word.  This is usually the form listed as the headword in a traditional dictionary (e.g. the infinitive for verbs, the singular for nouns),and may be identical to \textit{surface} in many cases.   \textit{Lemma} allows related words to be grouped in searches.
\item[enlemma] The lemma of the English word equivalent to the Welsh lemma.  
\item[pos] The part of speech of the word, e.g. adjective, verb, noun, interactional marker, name, punctuation, etc.  For the various components in this and the next three fields, see \Cref{appB}.
\item[gender] The grammatical gender of the word, e.g. masculine, feminine.
\item[number] The grammatical number of the word, e.g. singular, plural.  This also includes person for verbs.
\item[tense] The tense of the word if it is a verb.
\item[usage] Notes on the usage of the word, e.g. spoken form, non-standard form, error, etc.
\item[gramnotes] Notes on grammatical aspects of the word, e.g. the form occurs pre-vowel, as a verbal object, before a measurement word, etc.
\item[extended] A more detailed definition of the word, such as might occur in a dictionary.  This may contain multiple English options, while \textit{lemma} will contain only one (the most common), allowing it to be used in glosses.  For instance, \textit{gweddill} has "remainder" in \textit{enlemma} and "remainder, remnant" in \textit{extended}, meaning that we only need to maintain one entry in the dictionary instead of two (one for each meaning), and do not need to add rules in Autoglosser2 ruling out one of them.\footnote{Cases where one English equivalent might be more appropriate than another can be handled by contextual rules in the constraint grammar -- see \Cref{ss:substitute}.}  Additional clarifications may also be added here for people, places or organisations, e.g. the fact that \textit{Estyn} is an education and training inspectorate for Wales.
\item[corcencc] The tag for this word as used by CorCenCC.  These are generated by programmatically mapping existing Eurfa POS data onto CorCenCC tags -- see \Cref{appC} for a full list.  In Eurfa, the components of the tag are separated (e.g. \textit{E b u} instead of \textit{Ebu}), and joined together when the tag is printed out - this is to allow easier application of constraint grammar rules if desired.
\item[posplus] Additional part of speech information, e.g. whether a name is a toponym or a personal name, the degree of a demonstrative (near, far, etc), the type of punctuation mark, etc.  This field is also used to mark things such as incorrect double mutations.
\item[deriv] Currently unused -- available for future display of etymology.
\item[plural] The plural form of nouns where the singular can be predicted from the plural (at least 75\% of Welsh singulars are in this category).  Plurals which are not predictable are given their own main entry.  In Autoglosser2, we de-pluralise words, look up the singular, and then mark the part of speech as plural (see \Cref{s:gencohorts}), so the plural column is not used.  It is, however, necessary for dictionary use: for any given noun, we search in both the surface field (for non-predictable plurals) and the plural field (for predictable plurals), and if an entry is found in either, we can then display the entry as ``(plural of:)'' using the lemma field for (for non-predictable plurals) or the surface field (for predictable plurals).
\item[segment] The morphological segmentation of the word.  This is work in progress, and is only filled in for nouns at the minute.  Morpheme boundaries are shown with \% (e.g. \textit{iach\%awd\%wr\%i\%aeth}, salvation), and word boundaries of combined words with $\sim$ (e.g. \textit{byd$\sim$enw\%og}, world-famous). Morphemes are spelt in their ``base'' form, even where there are spelling changes in the surface word, e.g. \textit{am\%cylch\%i\%ad} \textrightarrow\ \textit{amgylchiad} (circumstance), \textit{arwain\%ydd} \textrightarrow\ \textit{arweinydd} (leader).  Segment allows derivationally-related words to be grouped in searches.
\item[status] Currently unused -- available for future development.
\end{description}


\section{Content}

Although Eurfa should cover at least 80\% of common text, it is inevitable that there will be words in the text that are not in Eurfa.  The output pdf flags missing (``unknown'') words in red, as does the web output.\footnote{Note that words which are not so marked may also be incorrectly glossed!}  Adding such words to Eurfa will not only allow them to be glossed, but may also improve the glossing of surrounding words -- for instance, if a rule for a word A depends on knowing whether a following word B is a noun or a verb, then word A is likely to remain undisambiguated if the following word B cannot be resolved as either because it is missing from Eurfa.\footnote{The long-term answer to this is of course to add more words to Eurfa, which can be a slow process.  It is unfortunate that so little lexical data for Welsh is available under a free (or even open) license, but things are slowly changing.}

The approach to content adopted for Autoglosser2 has been to seek to put as much material as possible into the dictionary, so that there is only one source that needs to be edited, rather than multiple files.  Punctuation, abbreviations,\footnote{Abbreviations and acronyms are listed in undotted form (i.e. \textit{BBC} instead of \textit{B.B.C.} or \textit{B. B. C.}).  All variants will be glossed, but the gloss will contain the undotted form even if the original word is dotted.} letters of the alphabet, etc. are therefore all included in Eurfa alongside ``normal'' words.  Eurfa also contains colloquialisms and re-spellings based on actual pronunciation (e.g. \textit{gyn} for \textit{gan}) which were used in the \textit{Siarad} conversations and reflect spoken rather than written Welsh.  A few entries also reflect errors or non-standard usage (e.g. \textit{hawsach} (easier) instead of \textit{haws}; \textit{blynyddau} (years) instead of \textit{blynyddoedd}; double mutation) as recorded in \textit{Siarad}.

Note that when used in Autoglosser2 Eurfa has a symbiotic relationship with it.  Changing aspects such as tokenisation or cohort generation may require changes in the content (new entries) or structure (new fields) in Eurfa, and these changes may in turn require changes in the rule definitions in the constraint grammar.

\section{Contributing}

If you can improve Eurfa (either by suggesting new features or donating new lexical data), I would be happy to take suggestions on board -- fork the repo, make your changes, and then submit a pull request, or send me your suggestions directly (kevin@dotmon.com).
