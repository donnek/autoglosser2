\chapter{Introduction}
\label{ch:intro}

\section{Introduction}

Being able to specify the part of speech of each word in running text (glossing or tagging) is the foundation for other natural language processing (NLP) tasks (examining word frequency or collocation, syntax analysis, machine translation, etc).

Autoglosser2 is a glosser/tagger for Welsh.\footnote{Proof-of-concept versions also exist for Gàidhlig (Scots Gaelic) and Māori.}  It is licensed under version 3 (or greater) of the Free Software Foundation's General Public License.\footnote{\url{gnu.org/licenses/gpl.html}. Components distributed with Autoglosser2 (HTML KickStart, Prototype, Jquery) have their own licenses.}  This means that, apart from costing nothing to use, it can be adapted and extended as required by the user, subject to the same license being used for any new version thus created and distributed.

Welsh is the most widely-spoken Celtic language, currently used by almost 20\% (562,000)\footnote{\url{statswales.gov.wales/Catalogue/Welsh-Language/WelshSpeakers-by-LocalAuthority-Gender-DetailedAgeGroups-2011Census}} of the population in Wales on a daily basis.  Since the 1993 Welsh Language Act, it has been an official language in Wales, with public sector bodies required to give it equal status with English.  Modern Welsh, due to its long contact with English, contains many English loans (conversation will usually include substantial amounts of code-switching), and its syntax has converged to some extent with English (e.g. periphrastic tenses have largely supplanted the native inflected tenses).


\section{The Bangor Autoglosser}

Autoglosser2 is a heavily revised version of the Bangor Autoglosser,\footnote{\url{github.com/donnekgit/autoglosser}} which was developed for Bangor University's then ESRC Centre for Research on Bilingualism in Theory and Practice in the period 2009-2011 \citep{Donnelly2011, Donnelly2011a, Donnelly2011b}.  That software, the first-ever dedicated Welsh glosser, was used to provide automatic glossing, based roughly on the proposals in \citet{Comrie2008}, for the Centre's Welsh, Spanish and English bilingual conversational corpora, the results of which are available under a GPL license at the BangorTalk site.\footnote{\url{bangortalk.org.uk}}  

A detailed account of the development and research use so far of one of those corpora, the half-million word Welsh-English \textit{Siarad} corpus, is available in \citet{Deuchar2018}.  \citet{Carter2013} and \citet{Carter2016} discuss the implications of using software such as the Bangor Autoglosser, while \citet{Deuchar2016} and \citet{Broersma2018} use data from the Autoglosser to draw significant new conclusions about the use of code-switching in Welsh.


\section{Autoglosser2}

A large part of the functionality of the Bangor Autoglosser was centred around handling conversational language which included code-switches (using English words and phrases in the Welsh, or switching entirely to English for some utterances).  Autoglosser2, on the other hand, is aimed at written rather than spoken Welsh text, and has been refactored to tidy the code and make it far faster (it now glosses at a rate of 22,000 words/minute instead of 1,000).  Like its predecessor, Autoglosser2 is rule-based -- it uses constraint grammar to decide on the appropriate gloss or tag (\Cref{ch:cg}).  In this respect it differs from taggers which apply statistical methods to decide on the appropriate tag.  The software again uses the Eurfa digital dictionary \citep{Donnelly2016}, the largest Welsh dictionary under a GPL license (\Cref{ch:eurfa}).

Eurfa and some of the principles underlying Autoglosser2 (e.g. the use of constraint grammar) are also used in the CyTag software produced by the Corpus Cenedlaethol Cymraeg Cyfoes (CorCenCC -- the National Corpus of Contemporary Welsh) project,\footnote{\url{corcencc.org}} led by Cardiff University, although that tagger has been specially developed to handle the needs of the CorCenCC corpus.  As well as the gloss format used by the Bangor ESRC corpora (\Cref{appB}), Autoglosser2 is also able to output tags in CorCenCC format (\Cref{appC})  -- see \Cref{ss:web-corcencc-hor,ss:web-corcencc-ver,s:pdfoutput,s:txtoutput}.


\section{Getting started}

Detailed instructions for installing Autoglosser2 and the other software it requires are in \Cref{appA}.

Autoglosser2 has been developed on Ubuntu Linux 14.04, and tested on Ubuntu Linux 16.04, so it should run well on current versions of any Linux distro (though the instructions in \Cref{appA} may need to be adjusted to take account of that distro's packaging approach).  It may also run on legacy platforms like Microsoft Windows or Apple OSX, but is likely to need some tweaking -- one way around this is to run Linux as a virtual machine on those platforms.\footnote{\url{virtualbox.org}}

A web interface is available at the Autoglosser2 website,\footnote{\url{autoglosser.org.uk}} and included in the download (see \Cref{ch:webint}), but this is intended only for short stretches of text, and for more demanding glossing (e.g. glossing a long file, or a directory of files) it will be best to use the command-line glosser (see \Cref{ch:pipeline}), which is also likely to fit better into existing language processing workflows.


\section{Contributing}

Since Autoglosser2 is free software, you are welcome to change and extend it to suit your own requirements.  \Cref{ch:customise} looks at areas where you might want to begin customising the output. I would be happy to take any suggestions on board -- fork the repo, make your changes, and then submit a pull request, or send me your code directly (kevin@dotmon.com). 


