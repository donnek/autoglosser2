\chapter{The Autoglosser2 pipeline}
\label{ch:pipeline}


\section{Introduction}

This chapter explains the Autoglosser2 pipeline -- a series of scripts that are run in succession to allow the import and glossing of your text.  The examples will assume that you want to gloss or tag text in a file called \url{atext.txt} -- wherever you see this, you can replace it with the name of your own file.

The commands here should be run in the command-line interface that is presented when you open a terminal (e.g. Konsole in KDE).  They should be run from the top-level directory of Autoglosser2.


\section{File format and location}

The file you want to gloss or tag should be in plain text format -- in other words, word-processor documents need to be converted to text format before import.  The UTF-8 encoding is assumed, and if your file is in a legacy encoding such as ISO-8859 or variants, or Windows-1252, it may be wise to convert them first in case you get unexpected results during the glossing process.

For tidiness, you can place your input file in the \url{inputs} folder in the Autoglosser2 directory, but this is not essential -- on first import you just need to give the full path to the input file's location.  It is a good idea to keep the input filename lower-case and all-one-word.  In contrast to Microsoft Windows, Llinux considers files with capitalised names as different files from the lower-case equivalent, and filenames containing spaces may not be handled as anticipated.  If you need to include multiple words in the filename, link them with an underscore.

Note that where database tables are created, they will be created from scratch with each invocation of the script, with the old table being deleted.  If for some reason you need to retain the old table (which is unlikely), you should back it up before running the script.


\section{Import the text file into separate utterances}

To import the file, run: 

\verb|php import.php inputs/atext.txt|\footnote{If your input file is in some other location, just give its full path before the filename.}

This will create a database table of the same name as your file, but ending in \url{_utterances} -- in this case, \url{atext_utterances} -- with each record in the table containing an utterance (which encompasses sentences, headings, list items, etc.) from your file.

The table contains four fields:
\begin{description}
\item[utterance_id] A numeric identifier for the entry.
\item[surface] The Welsh text of the sentence.
\item[translation] An existing or generated translation for the sentence -- left empty on initial import.
\item[filename] The name of the file containing the sentences -- in this case, \textit{atext}.
\end{description}

Using the example from \Cref{ch:webint}, if the contents of \url{atext.txt} were \textit{Mae Lois yn gwneud cacen.} (``Lois is making a cake.''), the resulting contents of the \url{_utterances} table after import would be as shown in \Cref{tab:utts}.  As with all the tables produced by Autoglosser2, this table can be inspected using phpPGAdmin (\Cref{s:ppa}), SQLWorkbench/J (\Cref{s:workbench}), or any other database GUI.

\begin{table}[H]
\centering
\begin{tabular}{llll}
\textbf{utterance_id} & \textbf{surface} & \textbf{translation}  & \textbf{filename} \\
\hline\noalign{\smallskip}
1 & Mae Lois yn gwneud cacen. & & atext \\
\end{tabular}
\caption{The \texttt{_utterances} table.}
\label{tab:utts}
\end{table}

The glossing process stores all output material (apart from the database tables) in the directory \url{outputs}, inside a sub-directory of the same name as your file.  During import, two files are created in the background, and are therefore to be found in the directory \url{outputs/atext}.    The first (\url{atext_sentencised.txt}) is a log file which holds the text at various stages in the import process -- this allows the segmentation procedure to be reviewed and adjusted if necessary.  The second (\url{atext_utterances.tsv}) contains the output of the segmentation, with the data items separated by tabs -- the contents of this file are copied directly into the fields of the \url{_utterances} table.  The tab-separated value (.tsv) format is used so that the file can be opened directly in a spreadsheet if desired, as well as being opened in a text editor.


\section{Tokenise the utterances}

To tokenise each utterance, run:

\verb|php wordify.php atext|

Note that you only have to give the name of your file (without the \textit{.txt} ending) -- the script will use this to locate the \url{_utterances} table, read each utterance out of that, and split it into words (which encompasses punctuation, numbers, etc.).

This will create a database table of the same name as your file, but ending in \url{_words} -- in this case, \url{atext_words} -- with each record in the table containing a word from the file, ordered in terms of the sentence it occurs in.

The table contains 11 fields:
\begin{description}
\item[word_id] A numeric identifier for the entry.
\item[utterance_id] The sentence in which the word occurs.
\item[location] The placement of the word in the sentence.
\item[surface] The Welsh word as it occurs in the sentence -- the surface form.
\item[lemma] The base form of the Welsh word - the underlying form -- left empty on initial import.
\item[enlemma] The base form of the English word corresponding to the Welsh word -- left empty on initial import.
\item[langid] An identifier for the language of the word -- left empty on initial import.  This was an important element in Autoglosser1, which was designed to tag multiple languages at once, but is of less importance in Autoglosser2.  The identifier uses the ISO-639-2 language codes,\footnote{\url{loc.gov/standards/iso639-2/php/code_list.php}} or some other three-letter abbreviation (e.g. \textit{han}, \textit{cyr} -- see below).  Where the language is non-identifiable (e.g. in the case of punctuation), the identifier is replaced with three dashes.
\item[auto] The autogloss for the word -- left empty on initial import.
\item[corcencc] The CorCenCC tag for the word -- left empty on initial import.
\item[filename] The name of the file containing the sentences -- in this case, \textit{atext}.
\item[semtag] The semantic domain of the word -- left empty on initial import.  Not currently used in Autoglosser2, but retained in case of future development.
\end{description}

\Cref{tab:words} shows the contents of the \url{_words} table after tokenisation.  In this and subsequent tables, the fieldname \textit{utterance_id} is abbreviated to \textit{uid}, and location to \textit{loc}.

\begin{table}[H]
\centering
\begin{tabular}{lllllllll}
\textbf{word_id} & \textbf{uid} & \textbf{loc} & \textbf{surface} & \textbf{lemma} & \textbf{enlemma} & \textbf{auto} & \textbf{corcencc} & \textbf{filename} \\
\hline\noalign{\smallskip}
1 & 1 & 1 & Mae &  &  &  &  & atext \\
2 & 1 & 2 & Lois & &  &  &  & atext \\
3 & 1 & 3 & yn & &  &  &  & atext \\
4 & 1 & 4 & gwneud & &  &  &  & atext \\
5 & 1 & 5 & cacen & &  &  &  & atext \\
6 & 1 & 6 & .  & &  &  &  & atext \\
\end{tabular}
\caption{The \texttt{_words} table.}
\label{tab:words}
\end{table}

During tokenisation, two files are created in the background, and stored in the directory \url{outputs/atext}.  The first (\url{text_wordified.txt}) is a log file which holds the text at various stages in the tokenisation process -- this allows the splitting procedure to be reviewed and adjusted if necessary.  The second (\url{atext_words.tsv}) contains the output of the tokenisation, with the data items separated by tabs -- the contents of this file are copied directly into the fields of the \url{_words} table.


\section{Generate a constraint grammar cohorts file}
\label{s:gencohorts}

Autoglosser2 uses constraint grammar (\Cref{ch:cg}) to select the correct tag from multiple possible tags.  This is done by applying the rules in a constraint grammar to a list (``cohort'') of the possible tags available for a particular word (``readings'').

To generate a cohorts file in a format that can be ingested by the constraint grammar parser, run:

\verb|php cohorts.php atext|

Continuing with the above example, the resulting output, stored in the file \url{outputs/atext/atext_cg.txt}, would be as in \Cref{fig:cginput}.

\begin{figure}[H]
\begin{Verbatim}[fontsize=\relsize{-1}, xleftmargin=0.6cm]
"<Mae>" atext,1,1
    "bod" [cym] v 3s pres :be: <B pres 3 u> + cap [59494] 
    "bae" [cym] n m sg :bay: <E g u> + nm + cap  [196982] 
"<Lois>" atext,1,2
    "Lois" [---] name :Lois: <E p> + cap
"<yn>" atext,1,3
    "yn" [cym] prt :: <U tra> [200654] 
    "yn" [cym] prep :in: <Ar sym> [204430] 
    "gan" [cym] prep :with: <Ar sym> + sm [196964] 
"<gwneud>" atext,1,4
    "gwneud" [cym] v infin :do: <Be> [202152] 
"<cacen>" atext,1,5
    "cacen" [cym] n f sg :cake: <E b ll> [209544] 
"<.>" atext,1,6
    "." [---] punc fullstop :.: <Atd t> [214098] 
\end{Verbatim}
\caption{A sample constraint grammar cohorts file.}
\label{fig:cginput}
\end{figure}

The words in the sentence appear in quotes and angle brackets, followed by the filename and location information.  Indented lines show relevant entries from Eurfa: the lemma in quotes, then the language identifier, then the part-of-speech information, then the English lemma, then the CorCenCC tag in angle brackets, then additional attributes such as capitalisation or mutation, and finally the number of the entry in Eurfa.

To create the \url{_cg} file, \url{cohorts.php} reads each word out of the \url{_words} table, and calls another file (\url{lookups/cym_lookup.php}) to look up that word in the Eurfa digital dictionary.  The lookup covers various aspects such as:

\begin{description}
\item[de-pluralisation] e.g. \textit{breichiau} \textrightarrow\ \textit{braich} (``arms''), \textit{canghennau} \textrightarrow\ \textit{cangen} (``branches'')
\item[de-capitalisation] e.g. \textit{Dirprwy} \textrightarrow\ \textit{dirprwy} (``deputy''), \textit{Cyd-Destunoli} \textrightarrow\ \textit{cyd-destunoli} (``contextualisation``)
\item[de-mutation] e.g. \textit{blant} \textrightarrow\ \textit{plant} (``children''), \textit{pharti} \textrightarrow\ \textit{party} (``party'')
\item[acronym resolution] e.g. B. B. C. = B.B.C. = BBC 
\item [English recognition] words that appear to be English will be marked as such, though no part of speech information will be given
\item [glyph recognition] e.g. 计划生育委员会 (\textit{jīhuàshēngyù wěiyuánhuì} -- ``family planning committee'') will be identified as Han glyphs, Российская Федерация (\textit{rossiyskaya federatsiya} -- ``Russian Federation'') will be identified as Cyrillic glyphs
\item [basic URL recognition] for addresses ending in \textit{.com, .co.uk, .org, .org.uk, ac.uk}
\item [number recognition] e.g. \textit{1980au} (decade), \textit{137} (number), \textit{18.12} (decimal), \textit{VII} (roman numeral)
\item [names] any capitalised words that still have no entry in Eurfa are tentatively marked as names
\end{description}

During cohort generation, two files are created in the background alongside the main \url{_cg} file, and stored in the directory \url{outputs/atext}.  They are intended to help the process of adding new items to Eurfa.  The first (\url{atext_names.txt}) lists in alphabetical order the capitalised words that have been guessed as names, and the second (\url{atext_unknowns.txt}) lists in alphabetical order any non-capitalised words for which an entry has not been found in Eurfa.  Both are often a useful way to pick up typos or misspellings in the original input text.


\section{Apply the constraint grammar rules}
\label{s:applycg}

To apply the constraint grammar to the generated cohorts file, run:

\verb|php apply_cg.php atext|

The constraint grammar rules will prune the cohort of possible options for each word down to (hopefully!) a single correct option.  The results will be stored in the file \url{outputs/atext/atext_cg_applied.txt}, and \Cref{fig:cgoutput} shows the sample text in \Cref{fig:cginput} after the constraint grammar rules have been applied.

\begin{figure}[H]
\begin{Verbatim}[fontsize=\relsize{-1}, xleftmargin=0.6cm]
"<Mae>" atext,1,1
    "bod" [cym] v 3s pres :is: <B pres 3 u> + cap [59494]
"<Lois>" atext,1,2
    "Lois" [---] name :Lois: <E p> + cap
"<yn>" atext,1,3
    "yn" [cym] prt :: <U tra> [200654]
"<gwneud>" atext,1,4
    "gwneud" [cym] v infin :make: <Be> [202152]
"<cacen>" atext,1,5
    "cacen" [cym] n f sg :cake: <E b ll> [209544]
"<.>" atext,1,6
    "." [---] punc fullstop :.: <Atd t> [214098]
\end{Verbatim}
\caption{The results of applying constraint grammar rules to \Cref{fig:cginput}.}
\label{fig:cgoutput}
\end{figure}

By default, two grammars are applied.  The first (\url{grammar/cym_grammar.cg3}) is a general grammar, and the second (\url{grammar/cym_multiword.cg3}) marks words which are part of a multiword expression, noting their position in the multiword (left, middle, right).  If you do not wish the multiword grammar to be applied, you can pass the option raw into \url{apply_cg.php}:

\verb|php apply_cg.php atext raw|

If you are refining rules, it is helpful to be able to compare ``before'' and ``after''.  To enable this, \url{apply_cg.php} produces two other files giving the output from previous applications of the rules.  The first (\url{atext_cg_applied_old.txt}) contains the output from the last application, and the second (\url{atext_cg_applied_old_old.txt}) contains the output from the application before that.  These files can then be compared in a difference viewer such as Meld\footnote{\url{meldmerge.org}} in order to check whether the rule changes have affected the desired word, and also whether there are any unexpected regressions relating to other words.

To investigate how rules are interacting with one another, you can also run:

\verb|php trace.php atext|

This will produce a file -- \url{outputs/atext/atext_cg_traced.txt} -- which contains the results of the rule application in a format which shows all the cohort options \Cref{fig:cginput}, but marked to show what rules were applied to arrive at \Cref{fig:cgoutput}.  The type of rule that has been applied is given at the end of the option, along with the line number of that rule in the grammar file, and the options pruned as a result of this are marked with a semicolon (;) at the beginning of the line -- see \Cref{fig:cgtraced}.

\begin{figure}[htbp]
\begin{Verbatim}[fontsize=\relsize{-1}, xleftmargin=0.6cm]
"<Mae>" atext,1,1
    "bod" [cym] v 3s pres :is: <B pres 3 u> + cap [59494] SUBSTITUTE:736
;   "bae" [cym] n m sg :bay: <E g u> + nm + cap [196982] REMOVE:155
"<Lois>" atext,1,2
    "Lois" [---] name :Lois: <E p> + cap
"<yn>" atext,1,3
    "yn" [cym] prt :: <U tra> [200654] SELECT:214
;   "gan" [cym] prep :with: <Ar sym> + sm [196964] REMOVE:138
;   "yn" [cym] prep :in: <Ar sym> [204430] SELECT:214
"<gwneud>" atext,1,4
    "gwneud" [cym] v infin :make: <Be> [202152] SUBSTITUTE:629
"<cacen>" atext,1,5
    "cacen" [cym] n f sg :cake: <E b ll> [209544]
"<.>" atext,1,6
    "." [---] punc fullstop :.: <Atd t> [214098]
\end{Verbatim}
\caption{Tracing the application of constraint grammar rules to \Cref{fig:cginput}.}
\label{fig:cgtraced}
\end{figure}


\section{Gather part-of-speech data}

Once the constraint grammar rules have been applied to the text so that (ideally) a single option has been selected for each word, the part-of-speech data for each word as given in the \url{_cg.txt} file is imported into a database table by running:

\verb|php cgfinished.php atext|

This creates a database table of the same name as your file, but ending in \url{_cgfinished} -- in this case, \url{atext_cgfinished}.

The table contains 15 fields:
\begin{description}
\item[id] A numeric identifier for the entry.
\item[filename] The name of the file containing the sentences -- in this case, \textit{atext}.
\item[utterance_id] The sentence in which the word occurs.
\item[location] The placement of the word in the sentence.
\item[surface] The Welsh word as it occurs in the sentence -- the surface form.
\item[langid] An identifier for the language of the word.
\item[lemma] The base form of the Welsh word - the underlying form.
\item[enlemma] The base form of the English word corresponding to the Welsh word.
\item[auto] The autogloss for the word.  This consists of the POS data for each word, concatenated with a full stop.
\item [dictid] The number of the entry in Eurfa.
\item[corcencc] The CorCenCC tag for the word.
\item [mutation] The type of mutation, if any, applied to the word.
\item[cap] Whether the word is capitalised in the text.
\item [adjust] Whether the word is part of a multiword expression, and if so, its location (left, middle, right) in the multiword.  This field will not be used if you have elected not to apply the \url{cym_multiword.cg3} grammar -- see \Cref{s:applycg}.
\item[semtag] The semantic domain of the word.
\end{description}

\Cref{tab:cgfinished} shows the main fields in the table once the data for the sample text has been imported.

\begin{table}[H]
\centering
\begin{tabular}{lllllllllllll}
\textbf{id} &\textbf{uid} & \textbf{loc} & \textbf{surface} & \textbf{langid} & \textbf{lemma}  & \textbf{enlemma} & \textbf{auto} & \textbf{corcencc} & \textbf{cap} \\
\hline\noalign{\smallskip}
1  & 1             & 1        & Mae     &  cym   & bod    & is      & v.3s.pres     & Bpres3u  & cap \\
2  & 1             & 2        & Lois    &  ---   & Lois   & Lois    & name          &    Ep       & cap \\
3  & 1             & 3        & yn      &  cym   & yn     &       & prt           & Utra     &     \\
4  & 1             & 4        & gwneud  &  cym   & gwneud & make    & v.infin       & Be       &     \\
5  & 1             & 5        & cacen   &  cym   & cacen  & cake    & n.f.sg        & Ebll      &     \\
6  & 1             & 6        & .       & ---   & .      & .       & punc.fullstop & Atdt     &    
\end{tabular}
\caption{Part of the \texttt{_cgfinished} table, reflecting the application of the constraint grammar rules.}
\label{tab:cgfinished}
\end{table}

During part-of-speech data collection, a \url{_cgfinished.tsv} file is created in the background, and stored in the directory \url{outputs/atext}.  This contains the generation output, with the data items separated by tabs – the contents of this file are copied directly into the fields of the \url{_cgfinished} table.


\section{Generate the glosses}

To generate the glosses, the tags need to be uppercased and attached to the English lemma (strictly speaking, this step is unnecessary for the CorCenCC tags, because they are already in final form and do not need to have the lemma attached).  But more importantly, if the word has not been disambiguated (that is, applying the constraint grammar rules have left more than one option for the word), the glosses and tags need to be combined so that they can highlight areas where the constraint grammar rules need to be refined.

To generate the glosses and any undisambiguated combinations, run:

\verb|php join_tags.php atext|

This creates a database table of the same name as your file, but ending in \url{_holding} -- in this case, \url{atext_holding}.

The table contains 10 fields:
\begin{description}
\item[id] A numeric identifier for the entry.
\item[filename] The name of the file containing the sentences -- in this case, \textit{atext}.
\item[utterance_id] The sentence in which the word occurs.
\item[location] The placement of the word in the sentence.
\item[lemma] The base form of the Welsh word - the underlying form.
\item[enlemma] The base form of the English word corresponding to the Welsh word.
\item[langid] An identifier for the language of the word.
\item[auto] The autogloss for the word.  This consists of the POS data for each word, concatenated with a full stop.
\item[corcencc] The CorCenCC tag for the word.
\item[semtag] The semantic domain of the word.
\end{description}

\Cref{tab:holding} shows the main fields in the table once the glosses have been generated.  The generation process also includes additional tweaking of the data: for instance, mutation marking is attached to the gloss; capitalisation is restored to nouns, adjectives, and infinitives; glosses for secondary words in multiword expressions are replaced with an arrow pointing towards the primary word. etc.

\begin{table}[H]
\centering
\begin{tabular}{llllllllll}
\textbf{id} & \textbf{uid} & \textbf{loc} & \textbf{lemma}  & \textbf{enlemma} & \textbf{langid} & \textbf{auto} & \textbf{corcencc} \\
\hline\noalign{\smallskip}
1  & 1             & 1        & bod    & is      & cym    & is.V.3S.PRES  & Bpres3u  \\
2  & 1             & 2        & Lois   & Lois    & ---    & Lois.NAME     & Ep \\
3  & 1             & 3        & yn     &         & cym    & PRT           & Utra \\
4  & 1             & 4        & gwneud & make    & cym    & make.V.INFIN  & Be \\
5  & 1             & 5        & cacen  & cake    & cym    & cake.N.F.SG   & Ebll \\
6  & 1             & 6        & .      & .       & ---    & PUNC.FULLSTOP & Atdt 
\end{tabular}
\caption{Part of the \texttt{_holding} table, showing generated glosses.}
\label{tab:holding}
\end{table}

During tag generation, a \url{_joined.tsv} file is created in the background, and stored in the directory \url{outputs/atext}.  This contains
the gloss and combination output, with the data items separated by tabs – the contents of this file are copied directly into the fields of the \url{_holding} table.  The relevant data is then transferred to the \url{_words} table.


\section{Create pdf output}
\label{s:pdfoutput}

Once all the words in the text have been glossed, running:

\verb|php pdfoutput.php atext|

\noindent will use the data in the \url{_utterances} and \url{_words} tables to generate a \textit{.tex} file (in this case, \url{atext.tex}), which can then be processed by the LaTeX typesetting system to create a pdf (\url{atext.tex}) of the input text, in a tiered format.  The original text is on the top tier, and below it is a tier containing the glosses -- see \Cref{fig:pdf-default}.

\begin{figure}[H]
\centering
\includegraphics{images/pdf-default.png}
\caption{Default pdf output.}
\label{fig:pdf-default}
\end{figure}

The default output in \Cref{fig:pdf-default}. can be adjusted in a number of ways by passing various options (in any order, separated by +) to \url{pdfoutput.php}:
\begin{description}
\item[corcencc] Display CorCenCC tags instead of \textit{Siarad}-style glosses.
\item[both] Display both glosses and tags on separate tiers.
\item[colour] Display glosses or tags in colour.  The colours can be specified by editing the pdfoutput.php script.
\item[nopunc] Suppress the display of glosses for punctuation marks.
\item[notrans] Suppress the translation line.
\end{description}
Multiple options can be passed in separated by a plus sign (+).  Figure \Cref{fig:pdf-corc-notrans} shows the output from running:

\verb|php pdfoutput.php atext corcencc+notrans|

\noindent and \Cref{fig:pdf-both-col} shows the output from running:

\verb|php pdfoutput.php atext both+colour+nopunc|

\begin{figure}[H]
\centering
\includegraphics{images/pdf-corc-notrans.png}
\caption{Options: corcencc+notrans.}
\label{fig:pdf-corc-notrans}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics{images/pdf-both-col.png}
\caption{Options: both+colour+nopunc.}
\label{fig:pdf-both-col}
\end{figure}


\section{Create text output}
\label{s:txtoutput}

Plain text output identical to that discussed in \Cref{ch:webint} can also be generated.  The default output is a horizontal gloss -- see \Cref{ss:web-siarad-hor} -- but three other layouts are also possible:
\begin{description}
\item[agvertical] Vertical gloss format -- see \Cref{ss:web-siarad-ver}.
\item[cchorizontal] Horizontal format using CorCenCC tags -- see \Cref{ss:web-corcencc-hor}.
\item[ccvertical] Vertical format using CorCenCC tags -- see \Cref{ss:web-corcencc-ver}.
\end{description}

The output file (\url{atext_txtoutput.txt}) can be adjusted by passing one of these options into the script -- for instance:

\verb|php txtoutput.php atext agvertical|

\noindent will produce a text file in the vertical gloss format.


\section{Glossing speed}

Glossing part of the CorCenCC Gold Corpus -- a 3,075-word extract from the 9m-word Kynulliad3 \citep{Donnelly2013} -- gives a rough indication of the speed of the pipeline.  \Cref{tab:duration} shows the average of three timings at the various stages of the pipeline when run on a 2014 laptop with an Intel i7-4500 CPU and 4Gb RAM.

\begin{table}[H]
\centering
\begin{tabular}{llr}
\textbf{Stage} & \textbf{Script} & \textbf{Duration} \\
\hline\noalign{\smallskip}
Import sentences & import.php & 0.391 \\
Tokenise sentences & wordify.php & 0.346 \\
Generate cohorts file & cohorts.php & 2.841 \\
Apply constraint grammar & apply_cg.php & 0.257 \\
Trace constraint grammar & trace.php & 0.216 \\
Collect part-of-speech data & cgfinished.php & 0.408 \\
Generate glosses & join_tags.php & 0.456 \\
Create pdf output & pdfoutput.php & 3.181 \\
Create text output & txtoutput.php & 0.166 \\
\hline\noalign{\smallskip}
\textbf{Total duration} & & \textbf{8.262}
\end{tabular}
\caption{Duration (in seconds) at the various stages of the glossing pipeline.}
\label{tab:duration}
\end{table}

\noindent Lookup and pdf generation are the slowest sections of the pipeline.  Overall, the average glossing speed is 372 words/second, or over 22,000 words/minute.\footnote{This is a considerable improvement on the speed (1,000 words/minute) of the Bangor Autoglosser.}  


\section{Running the entire pipeline}

To avoid having to run nine scripts one after the other, a compendium script (\url{do_everything.php}) is provided to do this.  Running:

\verb|php do_everything.php inputs/atext.txt|

\noindent will run each script in succession, and generate all the outputs files and tables.  The text to be imported should be given as the  argument to the script.

The options described above -- see \Cref{s:applycg,s:pdfoutput,s:txtoutput} --- can also be passed (in any order, separated by +)  to \url{do_everything.php}.  Thus:

\verb|php do_everything.php inputs/atext.txt raw+both+nopunc|

\noindent will use raw tags for multiwords, and print a pdf file with both \textit{Siarad}-type glosses and CorCenCC tags, skipping the glossing of punctuation.

\verb|php do_everything.php inputs/atext.txt corcencc+colour+notrans+ccvertical|

\noindent will print a pdf file with only CorCenCC tags, displaying these in colour, and suppressing the gist translation, and will print a txt file in vertical format.

You can of course choose to have glosses in one file and tags in another.  For instance:

\verb|php do_everything.php inputs/atext.txt colour+cchorizontal|

\noindent will produce a pdf file with coloured glosses, and a txt file with horizontal tags.


\section{Glossing a directory of files}

If you have a set of files in a directory, you can avoid having to run \url{do_everything.php} on them individually by using the shell script \url{run_do_everything}.\footnote{This is unlikely to work on Microsoft Windows without installing additional software, and may not work on Apple OSX.}  You need to edit the script before running it.  Your files should be placed in a directory in \url{inputs}, and the name of that directory should be specified in the \textit{FILES} line.  The outputs will be placed in a directory in \url{outputs}, and the name of that directory should be specified in the \textit{corpus} line.  You can then run the script:

\verb|./run_do_everything|














