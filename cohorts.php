<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script looks up each word in the words file in Eurfa, and generates a cohorts list to be fed to CG.
// Note that the language of the lookup is hard-wired at the end of this file:  include("lookups/cym_lookup.php");
// This could be adjusted by giving options for the lookup of multiple languages, as in the Bangor Autoglosser.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

//echo "Looking up the words ... please wait ...\n";

// Initialise an array to hold words that have already been looked up - this saves time on longer texts.
$streamlist=array();

// Open a file to hold a list of unknown words encountered.
$fpu = fopen("outputs/$filename/{$filename}_unknowns.txt", "w") or die("Can't create the file");
// Open a file to hold a list of putative names encountered (these will in fact often be capitalised forms of ordinary words).
$fpn = fopen("outputs/$filename/{$filename}_names.txt", "w") or die("Can't create the file");

// Open a file to hold the CG readings.
$fp = fopen("outputs/$filename/{$filename}_cg.txt", "w") or die("Can't create the file");

$sql=query("select * from $words order by filename, utterance_id, location;");
while ($row=pg_fetch_object($sql))
{
	$surface=pg_escape_string($row->surface);  // Required to allow lookup of words containing an apostrophe.
	//echo $surface."\n";
	$wordform=$row->surface;  // But we need a separate version so that the wordforms in the CG file don't have an escaped apostrophe.
	
	// Generate the location info to put after the surface form.  The CG format allows this, and it means the readings are cleaner and don't have the location data repeated multiple times.
	$place=$row->filename.",".$row->utterance_id.",".$row->location;
	//fwrite($fp, $place);
	
	$stream="\"<".$wordform.">\"";
	$loc=$stream." ".$place."\n";  // Each surface form ends in a newline
	fwrite($fp, $loc);
	
	if (isset($streamlist[$surface]))  // If the word has already been looked up, use that.  Note that  in_array() only checks the values, not the keys.  An alternative is : if (array_key_exists($surface, $streamlist))    
	{
	    //fwrite($fp, "... from cache ...\n");  // For testing.    
	    fwrite($fp, $streamlist[$surface]);  // Write out the readings associated with that surface word in the cache.
	    //echo $streamlist[$surface];
	}
	else  // If the word is not in the cache, do a new lookup.
	{
	   include("lookups/cym_lookup.php");
	    
	    // Add the results of the lookup to the cache of words already looked up.
	    $streamlist[$surface]=$fullentry;
	    unset($fullentry);	    
	}
}

fclose($fp);  // Close the CG readings file.

// Close the names  and unknowns file, then sort them for easier review.
fclose($fpn);
fclose($fpu);
exec("sort outputs/$filename/{$filename}_unknowns.txt | uniq > outputs/$filename/unknowns.txt");
exec("mv outputs/$filename/unknowns.txt outputs/$filename/{$filename}_unknowns.txt");
exec("sort outputs/$filename/{$filename}_names.txt | uniq > outputs/$filename/names.txt");
exec("mv outputs/$filename/names.txt outputs/$filename/{$filename}_names.txt");

// View cache contents and output (for testing).
// print_r($streamlist);
// $fpc = fopen("outputs/$filename/{$filename}_cache.txt", "w") or die("Can't create the file");
// foreach ($streamlist as $key=>$readings)
// {
//     fwrite($fpc, $key.": ".$readings);
// }
// fclose($fpc);

?>
