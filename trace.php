<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for the Welsh language.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script applies the CG rules to the cohorts file, and traces which rules were applied.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

//echo "Tracing the constraint grammar ... please wait ...\n";

// Specify the grammar file.  The template for the name is xxx_grammar.cg3.
$gram_file="cym";

// Open the output file.
$fp = fopen("outputs/$filename/{$filename}_cg_traced.txt", "w") or die("Can't create the file");

exec("vislcg3 -g grammar/".$gram_file."_grammar.cg3  --trace -I outputs/$filename/{$filename}_cg.txt", $cg_output);

foreach ($cg_output as $cg_line)
{
	//echo $cg_line."\n";
	fwrite($fp, $cg_line."\n");
}

// Close the output file.
fclose($fp);

unset ($cg_output);

?>
