<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for the Welsh language.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License and the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

function query($sql)
// Simplify the query writing in the current db.
// Use this as: $result=query("select * from table");
// This is for connections to the autoglosser2 import/export database.
{
    global $db_handle;
    return pg_query($db_handle, $sql);
}

function query_eurfa($sql)
// Simplify the query writing in the eurfa db.
// Use this as: $result=query_eurfa("select * from table");
// This is for connections to the dictionary database.  Used only in lookups/cym_lookup.php.
{
    global $db_handle_eurfa;
    return pg_query($db_handle_eurfa, $sql);
}

function get_filename()
// Turn the filename given to an individual script into a filename which can be used as a prefix for subsequent tables and files, and returns filepath and filename, along with tablenames based on the latter.  A directory to hold the output files is created if it does not already exist.
// $importfile = the argument to the script; this is usually a full path to a file, but it can also be the file itself
// $filename = the filename stripped of any extension like  .cha, .cex or .txt
// $utterances = $filename_utterances - the table holding the utterances from the filename
// $words = $filename_words - the table holding the words from the utterances
// $cgfinished = $filename_cgfinished - the table holding the autoglossed (POS-tagged) words
// $holding = $filename_holding - the table holding the glosses
{
	$importfile=$_SERVER['argv'][1];

	if (isset($importfile))
	{
		$filename=strtolower(basename(preg_replace("/\..*$/", "", $importfile)));  // remove the extension
// 		echo "Processing $importfile...\n\n";
	}
	else
	{
		echo "*\n*\nYou need to specify a file to handle.\n*\n*\n";
	}
	$utterances=strtolower($filename."_utterances");
	$words=strtolower($filename."_words");
	$cgfinished=strtolower($filename."_cgfinished");
	$holding=strtolower($filename."_holding");
	exec("mkdir -p outputs/$filename");  // -p suppresses the error message given when the dir already exists, and also means it won't be created if it already exists

	return array($importfile, $filename, $utterances, $words, $cgfinished, $holding);
}

function drop_existing_table($table)
// Drop the specified table in the current db so that it can be recreated.
// An alternative is: SELECT * FROM pg_tables WHERE tablename = 'mytable' AND schemaname = 'myschema';
// Use "true" here instead of "*" if preferred, but note that the "and" clause does not work in PPA, so it cannot be used there.
{
	global $db_handle;
	$det=query("select count(*) as count from pg_class where relname = '".$table."';");
	$row_exists=pg_fetch_object($det);
	if ($row_exists->count > 0) { query("drop table $table;"); }
}

function drop_existing_table_eurfa($table)
// Drop the specified table in the eurfa db so that it can be recreated.
{
	global $db_handle_eurfa;
	$dete=query_eurfa("select count(*) as count from pg_class where relname = '".$table."';");
	$row_exists=pg_fetch_object($dete);
	if ($row_exists->count > 0) { query_eurfa("drop table $table;"); }
}

function add_column_if_not_exist($table, $column)
// Add a column to the specified table if the table does not already contain that column.
{
	global $db_handle;
	$sql_exists="select count(*) as count from information_schema.columns where table_name='$table' and column_name='$column'";
	$result_exists=pg_query($db_handle, $sql_exists);
	$row_exists=pg_fetch_object($result_exists);
	if ($row_exists->count < 1)
	{
		//echo "There is no column $column - adding one";
		$sql_addcol="alter table $table add column $column character varying(20);";
		$result_addcol=pg_query($db_handle, $sql_addcol);
	}
	else
	{
		//echo "Column $column already exists";
		//exit;
	}
}

function add_integer_column_if_not_exist($table, $column)
// Add a column to the specified table if the table does not already contain that column.
{
	global $db_handle;
	$sql_exists="select count(*) as count from information_schema.columns where table_name='$table' and column_name='$column'";
	$result_exists=pg_query($db_handle, $sql_exists);
	$row_exists=pg_fetch_object($result_exists);
	if ($row_exists->count < 1)
	{
		$sql_addcol="alter table $table add column $column integer;";
		$result_addcol=pg_query($db_handle, $sql_addcol);
	}
}

function mark_abbrevs($text)
// Mark common multiletter abbreviations so that they do not disrupt sentence segmentation.
{
    $text=preg_replace("/Mr\./", "Mr¶", $text);
    $text=preg_replace("/Mrs\./", "Mrs¶", $text);
    $text=preg_replace("/Ms\./", "Ms¶", $text);
    $text=preg_replace("/Dr\./", "Dr¶", $text);
    $text=preg_replace("/Rev\./", "Rev¶", $text);
    $text=preg_replace("/Parch\./", "Parch¶", $text);
    $text=preg_replace("/Hon\./", "Hon¶", $text);
    $text=preg_replace("/Esq\./", "Esq¶", $text);
    $text=preg_replace("/Prof\./", "Esq¶", $text);
    $text=preg_replace("/Ltd\./", "Ltd¶", $text);
    $text=preg_replace("/Cyf\./", "Cyf¶", $text);
    $text=preg_replace("/Ph\./", "Ph¶", $text);  // as in Ph.D.
    $text=preg_replace("/Jan\./", "Jan¶", $text);
    $text=preg_replace("/Feb\./", "Feb¶", $text);
    $text=preg_replace("/Mar\./", "Mar¶", $text);
    $text=preg_replace("/Apr\./", "Apr¶", $text);
    $text=preg_replace("/Jun\./", "Jun¶", $text);
    $text=preg_replace("/Jul\./", "Jul¶", $text);
    $text=preg_replace("/Aug\./", "Aug¶", $text);
    $text=preg_replace("/Sep\./", "Sep¶", $text);
    $text=preg_replace("/Sept\./", "Sept¶", $text);
    $text=preg_replace("/Oct\./", "Oct¶", $text);
    $text=preg_replace("/Nov\./", "Nov¶", $text);
    $text=preg_replace("/Dec\./", "Dec¶", $text);
    $text=preg_replace("/e\.e\./", "e¶e¶", $text);
    return $text;
}

function hyphcomp($text)
// Join hyphenated items where both sides are capitalised (eg in headings), and the first side consists of listed components.  Add others as necessary.
{
    $text=preg_replace("/([Cc]yd)\s(-)\s([A-Z])/", "$1$2$3", $text);  // cyd-
    $text=preg_replace("/([Dd]i)\s(-)\s([A-Z])/", "$1$2$3", $text);  // di-
    $text=preg_replace("/([Ôô]l)\s(-)\s([A-Z])/", "$1$2$3", $text);  // ôl-
    return $text;
}

function de_soft($text)
// Remove Welsh soft mutations so that the demutated word can be looked up.  Note that the order of these regex replacements is significant - letters in the regex must be placed before their occurrence as replacements.  Couplets - [xx|xx] - are resolved by code in cym_lookup.php.
{
    $text=preg_replace("/^g/", "c", $text);
    $text=preg_replace("/^G/", "C", $text);
    $text=preg_replace("/^l([^l])/", "[g|l]l$1", $text);
    $text=preg_replace("/^L([^l])/", "[G|L]l$1", $text);
    $text=preg_replace("/^(r)([^h])/", "[gr|rh]$2", $text);
    $text=preg_replace("/^(R)([^h])/", "[Gr|Rh]$2", $text);
    $text=preg_replace("/^([aeoiuwyïŵŷ])/", "g$1", $text);
    $text=preg_replace_callback("/^([AEOIUWYÏŴŶ])/", "lower_soft_cap", $text);  // Pass a capitalised initial letter to a separate function, lower_soft_cap() below, and take the output of that, which amounts to prefixing a G and lowercasing the previous initial.
    $text=preg_replace("/^f/", "[m|b]", $text);
    $text=preg_replace("/^F/", "[M|B]", $text);
    $text=preg_replace("/^b/", "p", $text);
    $text=preg_replace("/^B/", "P", $text);
    $text=preg_replace("/^d([^d])/", "t$1", $text);
    $text=preg_replace("/^D([^d])/", "T$1", $text);
    $text=preg_replace("/^dd/", "d", $text);
    $text=preg_replace("/^Dd/", "D", $text);
    return $text;
}

function lower_soft_cap($text)
// Add a G before a capitalised (soft-mutated) vowel and lowercase the vowel.
{
    $text=$text[1];  // mb_strtolower()  expects a string, not an array, so pick out the first element in the array, namely, the letter we want to change.
    $text="G".mb_strtolower($text);  // Lowercase the letter and precede it with a capital G.
    return $text;
}

function de_nas($text)
// Remove Welsh nasal mutations so that the demutated word can be looked up.
{
    $text=preg_replace("/^ngh/", "c", $text);
    $text=preg_replace("/^Ngh/", "C", $text);
    $text=preg_replace("/^mh/", "p", $text);
    $text=preg_replace("/^Mh/", "P", $text);
    $text=preg_replace("/^nh/", "t", $text);
    $text=preg_replace("/^Nh/", "T", $text);
    $text=preg_replace("/^ng/", "g", $text);
    $text=preg_replace("/^Ng/", "G", $text);
    $text=preg_replace("/^m/", "b", $text);
    $text=preg_replace("/^M/", "B", $text);
    $text=preg_replace("/^n/", "d", $text);
    $text=preg_replace("/^N/", "D", $text);    
    return $text;
}

function de_asp($text)
// Remove Welsh aspirate mutations so that the demutated word can be looked up.
{
    $text=preg_replace("/^ch/", "c", $text);
    $text=preg_replace("/^Ch/", "C", $text);
    $text=preg_replace("/^ph/", "p", $text);
    $text=preg_replace("/^Ph/", "P", $text);
    $text=preg_replace("/^th/", "t", $text);
    $text=preg_replace("/^Th/", "T", $text);
    return $text;
}

function de_h($text)
// Remove Welsh h-mutation word-initially.
{
    $text=preg_replace("/^h/", "", $text);
    return $text;
}

function de_pluralise($text)
// Remove the commonest noun plural endings (accounting for around 75% of plurals).
// These are basic atm - further work might look at checking the number of syllables in the word (stress), and spelling rules (especially for -nn-, though the situation with -wyr suggests that that is a bit of a mess...).
// Order is important - shorter endings must come after longer endings that contain them.  
// Examples are given of compliant and (where they exist) non-compliant words - obviously we only keep the regex if the former are more numerous than the latter.  
// Note that just as adding a new regex will mean that plural entries can be removed from eurfa, so removing a regex means that plural entries need to be added or restored to eurfa.
{
   $myvalue="";
   
   if (preg_match("/iynau$/", $text)) { $myvalue=preg_replace("/iynau$/", "iwn", $text); }  // opsiynau
    elseif (preg_match("/ynau$/", $text)) { $myvalue=preg_replace("/ynau$/", "yn", $text); }  // terfynau

    elseif (preg_match("/reiau$/", $text)) { $myvalue=preg_replace("/reiau$/", "rrai", $text); }  // careiau
    elseif (preg_match("/eibiau$/", $text)) { $myvalue=preg_replace("/eibiau$/", "aib", $text); }  // seibiau
    elseif (preg_match("/eichiau$/", $text)) { $myvalue=preg_replace("/eichiau$/", "aich", $text); }  // breichiau
    elseif (preg_match("/eigiau$/", $text)) { $myvalue=preg_replace("/eigiau$/", "aig", $text); }  // creigiau
    elseif (preg_match("/eiliau$/", $text)) { $myvalue=preg_replace("/eiliau$/", "ail", $text); }  // deiliau
    elseif (preg_match("/einiau$/", $text)) { $myvalue=preg_replace("/einiau$/", "ain", $text); }  // atseiniau
    elseif (preg_match("/eintiau$/", $text)) { $myvalue=preg_replace("/eintiau$/", "aint", $text); }  // breintiau, !=seintiau!
    elseif (preg_match("/eiriau$/", $text)) { $myvalue=preg_replace("/eiriau$/", "air", $text); }  // geiriau
    elseif (preg_match("/eisiau$/", $text)) { $myvalue=preg_replace("/eisiau$/", "ais", $text); }  // dyfeisiau
    elseif (preg_match("/eithiau$/", $text)) { $myvalue=preg_replace("/eithiau$/", "aith", $text); }  // teithiau
    elseif (preg_match("/eiau$/", $text)) { $myvalue=preg_replace("/eiau$/", "ai", $text); }  // siwrneiau, !=ceiau!
    
    elseif (preg_match("/euliau$/", $text)) { $myvalue=preg_replace("/euliau$/", "aul", $text); }  // treuliau
    elseif (preg_match("/euau$/", $text)) { $myvalue=preg_replace("/euau$/", "au", $text); }  // ieuau
    
    elseif (preg_match("/hennau$/", $text)) { $myvalue=preg_replace("/hennau$/", "en", $text); }  // canghennau
    elseif (preg_match("/hwyllau$/", $text)) { $myvalue=preg_replace("/hwyllau$/", "nwyll", $text); }  // canhwyllau
    elseif (preg_match("/yllau$/", $text)) { $myvalue=preg_replace("/yllau$/", "wll", $text); }  // pyllau
    
    elseif (preg_match("/iannau$/", $text)) { $myvalue=preg_replace("/iannau$/", "iant", $text); }  // peiriannau, !=clorian!

    elseif (preg_match("/odlau$/", $text)) { $myvalue=preg_replace("/odlau$/", "awdl", $text); }  // sodlau, !=odlau!
    elseif (preg_match("/oniau$/", $text)) { $myvalue=preg_replace("/oniau$/", "awn", $text); }  // doniau
    elseif (preg_match("/oriau$/", $text)) { $myvalue=preg_replace("/oriau$/", "awr", $text); }  // oriau
    elseif (preg_match("/oeau$/", $text)) { $myvalue=preg_replace("/oeau$/", "o", $text); }  // lloeau, !=sioeau!
    elseif (preg_match("/oau$/", $text)) { $myvalue=preg_replace("/oau$/", "o", $text); }  // seroau

    elseif (preg_match("/ybiau$/", $text)) { $myvalue=preg_replace("/ybiau$/", "wb", $text); }  // clybiau
    elseif (preg_match("/yblau$/", $text)) { $myvalue=preg_replace("/yblau$/", "wbl", $text); }  // dyblau
    elseif (preg_match("/ychi?au$/", $text)) { $myvalue=preg_replace("/ychi?au$/", "wch", $text); }  // blychau, trychiau, !=[cd]rychau!
    elseif (preg_match("/ygi?au$/", $text)) { $myvalue=preg_replace("/ygi?au$/", "wg", $text); }  // jygau, jygiau
    elseif (preg_match("/ygrau$/", $text)) { $myvalue=preg_replace("/ygrau$/", "wgr", $text); }  // llygrau
    elseif (preg_match("/ylchau$/", $text)) { $myvalue=preg_replace("/ylchau$/", "wlch", $text); }  // bylchau
    elseif (preg_match("/ylltau$/", $text)) { $myvalue=preg_replace("/ylltau$/", "wllt", $text); }  // hypergysylltau
    elseif (preg_match("/ymi?au$/", $text)) { $myvalue=preg_replace("/ymi?au$/", "wm", $text); }  // consortiymau, albymau, symiau
    elseif (preg_match("/ympiau$/", $text)) { $myvalue=preg_replace("/ympiau$/", "wmp", $text); }  // lympiau
    elseif (preg_match("/ynciau$/", $text)) { $myvalue=preg_replace("/ynciau$/", "wnc", $text); }  // pynciau
    elseif (preg_match("/yngau$/", $text)) { $myvalue=preg_replace("/yngau$/", "wng", $text); }  // argyfyngau
    elseif (preg_match("/ypiau$/", $text)) { $myvalue=preg_replace("/ypiau$/", "wp", $text); }  // sypiau
    elseif (preg_match("/yrddau$/", $text)) { $myvalue=preg_replace("/yrddau$/", "wrdd", $text); }  // byrddau
    elseif (preg_match("/yrfau$/", $text)) { $myvalue=preg_replace("/yrfau$/", "wrf", $text); }  // tyrfau
    elseif (preg_match("/yrrau$/", $text)) { $myvalue=preg_replace("/yrrau$/", "wr", $text); }  // pentyrrau
    elseif (preg_match("/yrsi?au$/", $text)) { $myvalue=preg_replace("/yrsi?au$/", "wrs", $text); }  // pyrsau, sgyrsiau
    elseif (preg_match("/ysi?au$/", $text)) { $myvalue=preg_replace("/ysi?au$/", "ws", $text); }  // drysau, bysiau
    elseif (preg_match("/ytiau$/", $text)) { $myvalue=preg_replace("/ytiau$/", "wt", $text); }  // pytiau
    elseif (preg_match("/yrtiau$/", $text)) { $myvalue=preg_replace("/yrtiau$/", "wrt", $text); }  // cyrtiau

    elseif (preg_match("/nnau$/", $text)) { $myvalue=preg_replace("/nnau$/", "n", $text); }  // tonnau
    elseif (preg_match("/rrau$/", $text)) { $myvalue=preg_replace("/rrau$/", "r", $text); }  // barrau
    
    elseif (preg_match("/wysau$/", $text)) { $myvalue=preg_replace("/wysau$/", "wys", $text); }  // pwysau

    elseif (preg_match("/ïau$/", $text)) { $myvalue=preg_replace("/ïau$/", "i", $text); }  // copïau
    elseif (preg_match("/i?au$/", $text)) { $myvalue=preg_replace("/i?au$/", "", $text); }  // melinau, muriau

    elseif (preg_match("/eibion$/", $text)) { $myvalue=preg_replace("/eibion$/", "ab", $text); }  // meibion
    elseif (preg_match("/eifion$/", $text)) { $myvalue=preg_replace("/eifion$/", "af", $text); }  // cleifion
    elseif (preg_match("/eigion$/", $text)) { $myvalue=preg_replace("/eigion$/", "aig", $text); }  // ysgolheigion
    elseif (preg_match("/eilion$/", $text)) { $myvalue=preg_replace("/eilion$/", "ail", $text); }  // adfeilion
    elseif (preg_match("/eillion$/", $text)) { $myvalue=preg_replace("/eillion$/", "aill", $text); }  // cyfeillion
    elseif (preg_match("/eision$/", $text)) { $myvalue=preg_replace("/eision$/", "as", $text); }  // caethweision, !=manteision!
    elseif (preg_match("/eithion$/", $text)) { $myvalue=preg_replace("/eithion$/", "aith", $text); }  // cyfreithion
    elseif (preg_match("/eion$/", $text)) { $myvalue=preg_replace("/eion$/", "ai", $text); }  // cyflogeion
    elseif (preg_match("/euon$/", $text)) { $myvalue=preg_replace("/euon$/", "au", $text); }  // adneuon, !=dadleuon!
    
    elseif (preg_match("/oeon$/", $text)) { $myvalue=preg_replace("/oeon$/", "o", $text); }  // cloeon
    elseif (preg_match("/ofion$/", $text)) { $myvalue=preg_replace("/ofion$/", "awf", $text); }  // arbrofion

    elseif (preg_match("/ygon$/", $text)) { $myvalue=preg_replace("/ygon$/", "wg", $text); }  // arolygon
    elseif (preg_match("/ythion$/", $text)) { $myvalue=preg_replace("/ythion$/", "wth", $text); }  // bygythion

    elseif (preg_match("/ïon$/", $text)) { $myvalue=preg_replace("/ïon$/", "i", $text); }  // egnïon
    elseif (preg_match("/i?on$/", $text)) { $myvalue=preg_replace("/i?on$/", "", $text); }  // awduron, ysgolion
    
    elseif (preg_match("/feydd$/", $text)) { $myvalue=preg_replace("/feydd$/", "fa", $text); }  // swyddfeydd
    elseif (preg_match("/faoedd$/", $text)) { $myvalue=preg_replace("/faoedd$/", "fa", $text); }  // gyrfaoedd
    elseif (preg_match("/ïoedd$/", $text)) { $myvalue=preg_replace("/ïoedd$/", "i", $text); }  // ffatrïoedd
    elseif (preg_match("/ymoedd$/", $text)) { $myvalue=preg_replace("/ymoedd$/", "wm", $text); }  // cymoedd
    
    elseif (preg_match("/ennydd$/", $text)) { $myvalue=preg_replace("/ennydd$/", "en", $text); }  // tomennydd
    elseif (preg_match("/entydd$/", $text)) { $myvalue=preg_replace("/entydd$/", "ant", $text); }  // nentydd
    elseif (preg_match("/(oe|y)dd$/", $text)) { $myvalue=preg_replace("/(oe|y)dd$/", "", $text); }  // gwyntoedd, afonydd

    elseif (preg_match("/ennod$/", $text)) { $myvalue=preg_replace("/ennod$/", "en", $text); }  // colomennod
    elseif (preg_match("/ychod$/", $text)) { $myvalue=preg_replace("/ychod$/", "wch", $text); }  // cychod
    elseif (preg_match("/ïod$/", $text)) { $myvalue=preg_replace("/ïod$/", "i", $text); }  // mwncïod
    elseif (preg_match("/[eo]d$/", $text)) { $myvalue=preg_replace("/[eo]d$/", "", $text); }  // merched, genethod

    elseif (preg_match("/eiliaid$/", $text)) { $myvalue=preg_replace("/eiliaid$/", "ail", $text); }  // anifeiliaid, !=budd-ddeiliaid!
    elseif (preg_match("/ei?niaid$/", $text)) { $myvalue=preg_replace("/ei?niaid$/", "en", $text); }  // capteiniaid, capteniaid, !=cytseiniaid!
    elseif (preg_match("/entiaid$/", $text)) { $myvalue=preg_replace("/entiaid$/", "ent", $text); }  // cleientiaid
    elseif (preg_match("/ïaid$/", $text)) { $myvalue=preg_replace("/ïaid$/", "i", $text); }  // sombïaid
    elseif (preg_match("/i?aid$/", $text)) { $myvalue=preg_replace("/i?aid$/", "", $text); }  // doctoriaid
 
    elseif (preg_match("/eiddwyr$/", $text)) { $myvalue=preg_replace("/eiddwyr$/", "eiddiwr", $text); }  //rheoleiddwyr
    elseif (preg_match("/wyr$/", $text)) { $myvalue=preg_replace("/wyr$/", "wr", $text); }  // ymerawdwyr

    elseif (preg_match("/enni$/", $text)) { $myvalue=preg_replace("/enni$/", "en", $text); }  // taflenni
    elseif (preg_match("/([dt])ai$/", $text)) { $myvalue=preg_replace("/([dt])ai$/", "$1y", $text); }  // labordai
    elseif (preg_match("/(..)i$/", $text)) { $myvalue=preg_replace("/(..)i$/", "$1", $text); }  // swyddi, !=amlenni!  Dots are to stop 'ei' being tagged as plural!

    elseif (preg_match("/âu$/", $text)) { $myvalue=preg_replace("/âu$/", "a", $text); }  // camerâu

    elseif (preg_match("/ysys$/", $text)) { $myvalue=preg_replace("/ysys$/", "ws", $text); }  // bysus
    elseif (preg_match("/yrsys$/", $text)) { $myvalue=preg_replace("/yrsys$/", "wrs", $text); }  // cyrsys
    elseif (preg_match("/ys$/", $text)) { $myvalue=preg_replace("/ys$/", "", $text); }  // bocsys
    elseif (preg_match("/s$/", $text)) { $myvalue=preg_replace("/s$/", "", $text); }  // annwyds
    
    elseif (preg_match("/eirdd$/", $text)) { $myvalue=preg_replace("/eirdd$/", "ardd", $text); }  // beirdd
    elseif (preg_match("/erddi$/", $text)) { $myvalue=preg_replace("/erddi$/", "erdd", $text); }  // cerddi, !=gerddi!
    
    return $myvalue;
}

function tidy_trans($text)
// Tidy the translation.
{
    //$text=preg_replace("/([a-z])-([a-z])/", "$1 $2", $text);
    $text=preg_replace("/\s\+/", "+", $text);
    //$text=preg_replace("/\+s/", "s", $text);
    $text=preg_replace("/\s\~/", "~", $text);  // undisambiguated words
    $text=preg_replace("/\s([\?!\.])/", "$1", $text);
    $text=preg_replace("/ find\+ed/"," found", $text);
    $text=preg_replace("/ see\+ed/"," saw", $text);
    $text=preg_replace("/has go\+ed/","has gone", $text);
    $text=preg_replace("/ keep\+ed/"," kept", $text);
    $text=preg_replace("/ a ([aeiouh])/"," an $1", $text);
    $text=preg_replace("/e\+ed/","ed", $text);  # complicated
    $text=preg_replace("/(e)\+ing/","ing ", $text);  # using, managing
    //$text=preg_replace("/y\+s/","ies ", $text);  # categories
    //$text=preg_replace("/\+/", "", $text);
    $text=ucfirst($text);
    return $text;
}

function web_mark($text)
// Mark up the translation to colourise it for the webpage.
{
    $text=preg_replace("/{/", "<span style=\"color: #c55;\">", $text);
    $text=preg_replace("/}/", "</span>", $text);
    $text=preg_replace("/(~[A-Za-z]+)/", "<span style=\"color: #c55;\"><em>$1</em></span>", $text);
    return $text;
}

function tex_surface($text)
// Escape characters in the surface line that LaTeX does not like.
{
	$text=preg_replace("/_/", "\_", $text);
	$text=preg_replace("/%/", "\%", $text);
	$text=preg_replace("/\&/", "\&", $text);
	$text=preg_replace("/#/", "\#", $text);
	$text=preg_replace("/~/", "$\sim$", $text);
	$text=preg_replace("/\+\^/", "+\\textasciicircum~", $text);
	$text=preg_replace("/\+\/\//", "+$//$", $text);
	return $text;
}

function tex_auto($text)
// Escape characters in the glosses that LaTeX does not like.
{
	$text=preg_replace("/ /", ".", $text);  // get rid of any spaces in the POS string
	$text=preg_replace("/_/", "\_", $text);
	$text=preg_replace("/%/", "\%", $text);
	$text=preg_replace("/~/", "$\sim$", $text);
	return $text;
}

function array_shift2(&$array)
// This is a non-destructive version of array_shift, ie it does not reindex the numerical keys.
// Thanks to the anonymous poster at http://php.net/manual/en/function.array-shift.php
{
	reset($array);
	$key=key($array);
	$removed=$array[$key];
	unset($array[$key]);
	return $removed;
 }

function rounded($var,$n)
// rounds fields to n decimal places
{
        $var=sprintf("%.".$n."f",$var);
        return $var;
}

function show_array($array)
// shows the contents of an array
{
  foreach ($array as $key=>$value)
  {
    if (is_array($value))
    {
      show_array($value); 
    }
    else
    {
      echo $key.": ".$value."\n"; 
    } 
  }
}

function displayChildrenRecursive($xmlObj, $depth=0)
// displays the children of an xml object
//Thanks to: http://debuggable.com/posts/parsing-xml-using-simplexml:480f4dfe-6a58-4a17-a133-455acbdd56cb
// $mystuff = simplexml_load_file('myfile.xml');
// displayChildrenRecursive($mystuff);
{
    foreach($xmlObj->children() as $child)
    {
	echo str_repeat('-', $depth).">".$child->getName().": ".$subchild."\n";
	displayChildrenRecursive($child, $depth+1);
    }
}

?>
