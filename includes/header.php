<?php
// Start session at the very beginning
// session_start();

// Include language initialization file
// require_once("includes/langinit.php");
?>

<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">

<?php header("Content-Language: cy"); ?>

<head>
<title>Welsh glosser/tagger</title>
<meta charset="UTF-8">
<meta name="description" content="Autoglosser2: a GPL glosser/tagger for Welsh." />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="copyright" content="Kevin Donnelly, 2016-2018" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js">
</script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="js/prettify.js"></script>                                   
<script type="text/javascript" src="js/kickstart.js"></script> 
<script type="text/javascript" src="prototype.js"></script>
<script>
Ajax.Responders.register({
onCreate: function(){ Element.show('spinner')},
onComplete: function(){Element.hide('spinner')}
});
</script>

<!-- Thanks to: http://askowen.info/2008/03/how-do-i-add-a-spinner-to-my-ajax-application/ and http://www.ajaxload.info/ for the spinner -->
<link rel="stylesheet" type="text/css" href="css/kickstart.css" media="all" />
<link rel="stylesheet" type="text/css" href="style.css" media="all" />                          
</head>

<body>

<!--- Header --->
<div id="header">
<a href="./index.php"><img src="./images/header.jpg" width="671" height="115" alt="Autoglosser2" /></a>
</div>
<!--- End Header --->

<!--- Body wrapper--->
<div id="wrap" class="clearfix">
