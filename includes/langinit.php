<?php

session_start();

// Functions.
// ----------------

function getAvailableLangs()
{
    return [
        "en" => [_("English (Saesneg)"), "en_GB.utf8"],
        "cy" => [_("Welsh (Cymraeg)"), "cy_GB.utf8"],
    ];
}

function getLang()
{
    global $default_language;
    $lang = $_COOKIE['lang'] ?? '';
    $alangs = getAvailableLangs();
    if (empty($lang) || !isset($alangs[$lang])) 
    {
        $lang = $default_language;
    }
    return $lang;
}

// Initialisation code.
// --------------------------

// Set the default language for the pages.
$GLOBALS['default_language'] = "en";

if (isset($_GET['lang'])) 
{
    setcookie('lang', $_GET['lang'], [
        'expires' => time() + 30 * 24 * 60 * 60,
        'path' => '/',
        'secure' => true,
        'httponly' => true,
        'samesite' => 'Strict'
    ]);
    $url = http_build_query(array_diff_key($_GET, ['lang' => '']));
    $redirect_url = "http://" . $_SERVER['HTTP_HOST'] .  $_SERVER['PHP_SELF'] . ($url ? "?$url" : "");
    header("Location: " . $redirect_url);
    exit();
}

// Set up translations
$lang = getLang();
$langs = getAvailableLangs();
$locale = $langs[$lang][1];
putenv("LANG=$locale");
putenv("LANGUAGE=$locale");
setlocale(LC_ALL, $locale);

// Specify the location of the translation files.
$domain = "messages";  // Define the variable first.
bindtextdomain($domain, "./locale");
bind_textdomain_codeset($domain, 'UTF-8');
textdomain($domain);
