<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script imports text and segments it into sentences.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

// Create the utterances table.
drop_existing_table($utterances);

$sql_table = "
CREATE TABLE $utterances (
    utterance_id serial NOT NULL,
    surface text,
    translation text,
    filename character varying(50)
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$utterances." ADD CONSTRAINT ".$utterances."_pk PRIMARY KEY (utterance_id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

//echo "Segmenting the utterances ... please wait ...\n";

if (isset($importfile))  // We're running at the command line.
{
    $lines=file($importfile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
}
else  // We're running on a website.
{
    $lines=array("$instring");  // $instring is a string, so has to be set up as an array so that the foreach() below, necessary for command-line handling of files, will work.  Alternatively, specifically cast the web interface input as an array, by using: foreach ((array) $lines as $line)
}
// print_r($lines);

$fp = fopen("outputs/$filename/{$filename}_utterances.tsv", "w") or die("Can't create the file");  // Open a file for the sentencised text to be copied into the db table.
$fpl = fopen("outputs/$filename/{$filename}_sentencised.txt", "w") or die("Can't create the file");  // Open a file to hold the sentencised text at different stages for review.

foreach ($lines as $line)  // It's essential to cast the $lines to an array, to cover $instring from the web interface.
{
	$line=trim(preg_replace("/\s+/", " ", $line));  // Collapse spaces, tabs, etc.
	
	$line=mark_abbrevs($line);  // Process common abbreviations.  Add others as necessary.

	// Set an end-sentence marker.
	$line=preg_replace("/(\.|\?|!)(?!([A-Za-z0-9]|\\\"))/", "$1~", $line);  // Sentence-final punctuation, except where followed directly by an alphanum or a double quote.
	$line=preg_replace("/(\.|\?|!)(\\\")(\s)/", "$1$2~$3", $line);  // Sentence-final punctuation where followed directly by a double quote.
	//$line=preg_replace("/(\?)/", "$1~", $line);  // Question marks
	//$line=preg_replace("/(!)/", "$1~", $line);  // Exclamation marks.
	$line=preg_replace("/~+/", "~", $line);  // Collapse multiple markers to one.
	
	fwrite($fpl, $line."\n");  // Print the marked output to the log file. 

	// Remove the marker.
	$line=preg_replace("/([A-Z])(\.)(~)/", "$1.", $line);  // Where there is only one letter before the full stop - to stop names being split in the middle, eg Geraint H. Jenkins.
	$line=preg_replace("/([A-Z]{2})\.\s([A-Z])/", "$1.~ $2", $line);  // And add it back when two capitals (ie an acronym) precedes the full stop, ie an acronym at sentence-end (this will also cover longer acronyms).
	$line=preg_replace("/(\.)~(\.)~(\.)~/", "$1$2$3", $line);  // In ellipsis.
	//$line=preg_replace("/(!|\?)~(\")/", "$1$2", $line);  // Where sentence-final punctuation marks are inside a quoted section.
	$line=preg_replace("/(e\.e\.)~/", "$1", $line);  // Abbreviations.

	fwrite($fpl, $line."\n");  // Print the revised marked output to the log file.
	
	// ¶ (AltGr+R) = fullstop-as-abbrev (ie not as an end-sentence marker)
	// § (Shift+AltGr+S) = fullstop-as-abbrev+space
	// Recode fullstops in 70s-style acronyms like B.B.C. (without spaces) so that they are tokenised as 1 item.
	$line=preg_replace("/([A-Z])\.([A-Z])\.([A-Z])\./u", "$1¶$2¶$3¶", $line);  // 3-character acronyms.
	$line=preg_replace("/([A-Z])\.([A-Z])\./u", "$1¶$2¶", $line);  // 2-character acronyms.
	// Recode fullstops in older-style acronyms like B. B. C.  (with spaces) so that they are tokenised as one item.  
	$line=preg_replace("/([A-Z])\.\s([A-Z])\.\s([A-Z])\./u", "$1§$2§$3¶", $line);  // 3-character acronyms.
	$line=preg_replace("/([A-Z])\.\s([A-Z])\./u", "$1§$2¶", $line);  // 2-character acronyms.
	
	fwrite($fpl, $line."\n\n");  // Print the revised marked output to the log file. 
	//echo $line."\n\n";

	$sents=array_filter(explode("~", $line));  // Split at the marker.  Use array_filter to throw away empty parts.

	foreach ($sents as $sent)
	{
		$sent=trim(preg_replace("/’/", "'", $sent));  // Replace curly quotes and trim.
		//echo $sent."\n\n";
		
		$sentline=$sent."\t".$filename."\n";
		fwrite($fp, $sentline);
	}
}

fclose($fp);
fclose($fpl);

exec($pg_handle." -c \"\copy {$filename}_utterances(surface,filename) from 'outputs/$filename/{$filename}_utterances.tsv' delimiter '\t' quote E'\b' csv;\"");
// To allow double quotes to be imported, we need to change the default quote character to backspace (E'\b'), which should not normally occur in texts.  There seems no way to just switch off the default quote character entirely.
//http://stackoverflow.com/questions/7376322/ignore-quotation-marks-when-importing-a-csv-file-into-postgresql

?>