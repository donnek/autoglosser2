<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for the Welsh language.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License and the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script formats the output of the Autoglosser2 pipeline so that it can be displayed on the web-interface, and also produces a text file containing the output.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

$collection=explode("+", $argv[2]);  // Take a list of options from the second command-line argument (if any) - these should be separated by a +.

$fp=fopen("outputs/$filename/{$filename}_txtoutput.txt", "w") or die("Can't create the file");  // Open a downloadable file which will hold the tagged output.

// Initialise variables.
$surface='';
$corcencc='';
$auto='';
// An empty variable has to be set up here, otherwise the concatenation $surface.=$row_w->surface." "; below will result in the first line of the text having a preceding dot.

if (!isset($importfile))  // We're running on a website.
{
    // Give a heading and an option to download the output.
    echo "<h6>Bras-gyfieithiad a tagiau / Gist translation and tags</h6><p class=\"right\">(De-gliciwch a dewiswch \"Cadw\" i lawrlwytho <a href=\"outputs/$filename/{$filename}_txtoutput.txt\" target=_blank>y testun wedi'i dagio</a>.)<br />(Right-click and select \"Save\" to download <a href=\"outputs/$filename/{$filename}_txtoutput.txt\" target=_blank>the tagged text</a>.)</p><br />";
}

$sql_s=query("select * from $utterances order by utterance_id;");
while ($row_s=pg_fetch_object($sql_s))
{
	$utterance_id=$row_s->utterance_id;
	$uttline=$row_s->surface;
	$uttline=preg_replace("/¶/", ".", $uttline);
	$uttline=preg_replace("/§/", ". ", $uttline);
	$transline=$row_s->translation;  // Use a klecs translation (massaged, using additional constraint grammars) if one exists. Choose below whether to use it or a gist translation ($translation) produced by simply concatenating the English lemmas.
	$translineweb=web_mark($transline);  // Adjust the translation on the webpage to draw attention to unknown and undisambiguated words.
	
	$sql_w=query("select * from $words where utterance_id=$utterance_id and surface!='¬' order by location;");  // Don't display the end-of-heading marker.
	while ($row_w=pg_fetch_object($sql_w))
	{
		$location=$row_w->location;
		$surface=$row_w->surface;
		$enlemma=$row_w->enlemma;
		$auto=$row_w->auto;
		$corcencc=$row_w->corcencc;
		
		if (preg_match("/^PUNC/", $auto)) 
                {
                    if (in_array("nopunc", $collection) or isset($_POST['nopunc']))  // If this option is used ...
                    {
                        $auto="";  // ... don't tag punctuation marks.
                    }
                }

		// Generate a gist translation (concatenated lemmas) instead of a (massaged) klecs one.
		// Add a marker for plurals, because noun lemmas in Eurfa are expressed in the singular.
		if (preg_match("/\.PL($|\+)/", $auto)) 
		{ 
                    if (!isset($importfile))  // We're running on a website.
                    {
                        $enlemma=$enlemma."<sup>(pl)</sup>";
                    }
                    else
                    {
                        $enlemma=$enlemma."(pl)";
                    }
                }  
		$translation.=$enlemma." ";
		
		// Choose the tags listed (Siarad-style glosses or CorCenCC tags) based on the output format chosen.
		if ($layout=="cchorizontal" or $layout=="ccvertical" or in_array("cchorizontal", $collection) or in_array("ccvertical", $collection))
		{
		    $tagchoice=$corcencc;
		}
		else
		{
		    $tagchoice=$auto;
		}

		$values[$location."#".$surface]=$tagchoice;  // We need a unique tag for $surface (provided by $location) if it is used as an index, otherwise if there are two (or more) instances of the same word in a sentence, the second (or later) will be assumed to be the same index as the first, and will not be displayed in the output.
	}

	// Set up location and translation.
	if (!isset($importfile))  // We're running on a website.
	{
	    echo "<p>(".$utterance_id.") ".$uttline."</p>";
	    echo "<p class=\"note\">[".$translation."]</p>";  // Give a gist translation.
	    //echo "<p class=\"note\">[".$translineweb."]</p>";  // Give a klecs translation.
	}
	fwrite($fp, "(".$utterance_id.") ".$uttline."\n");
	fwrite($fp, "[".$translation."]\n");  // Give a gist translation.
	//fwrite($fp, "[".$transline."]\n\n");  // Give a klecs translation.

	// Do the layout depending on which of the four options has been chosen.
	if ($layout=="agvertical" or in_array("agvertical", $collection))  // Use a vertical layout with Siarad-type glosses.
	{
	    foreach ($values as $mysurf=>$myauto)
	    {
			$locsurf=explode("#", $mysurf);
			if (!empty($myauto))  // Remove final slash if punctuation is not being tagged.
			{
				$gloss="#".$myauto;
			}
			else
			{
				$gloss=$myauto;
			}
		
			if (!isset($importfile))  // We're running on a website.
			{
			// If the word is unknown, or the gloss is undisambiguated, mark the word in red. 
			if (preg_match("/~/", $gloss) or preg_match("/UNK/", $gloss))
			{
				$wordclass="warnword";
				$glossclass="warngloss";
			}
			else
			{
				$wordclass="translation";
				$glossclass="close";
			}
			
			// Mark up the location, word and gloss.
			echo "($utterance_id, $locsurf[0]) <span class=\"$wordclass\">$locsurf[1]</span><span class=\"$glossclass\">$gloss</span><br />";
		}
		
		// Write out the tagged text in vertical format in a text file.
		$textline="{".$utterance_id.",".$locsurf[0]."} ".$locsurf[1]."#".$myauto."\n";
		$textline=preg_replace("/\/\n$/", "\n", $textline);  // Remove final slash if punctuation is not being tagged.
		fwrite($fp, $textline);
	    }
	    if (!isset($importfile)) { echo "<br />"; }
	    fwrite($fp, "\n"); 
	}
	elseif ($layout=="cchorizontal" or in_array("cchorizontal", $collection))  // Use a horizontal layout with CorCenCC tags.
	{
	    foreach ($values as $mysurf=>$mycor)
	    {
		$locsurf=explode("#", $mysurf);
		
		if (!isset($importfile))  // We're running on a website.
                {
                    // If the word is unknown, or the gloss is undisambiguated, mark the word in red. 
                    if (preg_match("/\|/", $mycor) or preg_match("/Gwann/", $mycor))
                    {
                        $wordclass="warnword";
                        $glossclass="warngloss";
                    }
                    else
                    {
                        $wordclass="translation";
                        $glossclass="close";
                    }

                    // Mark up the location, word and tag.
                    echo "<sup class=\"loc\">".$locsurf[0]."</sup><span class=\"$wordclass\"> ".$locsurf[1]."</span><span class=\"$glossclass\">#".$mycor."</span>&nbsp;&nbsp;";
                }
		
		// Write out the tagged text in horizontal format in a text file.
		$textline="{".$utterance_id.",".$locsurf[0]."}".$locsurf[1]."#".$mycor." ";
		fwrite($fp, $textline);
	    }
	    if (!isset($importfile)) { echo "<br /><br />"; }
	    fwrite($fp, "\n\n");
	}
	elseif ($layout=="ccvertical" or in_array("ccvertical", $collection))  // Use a vertical layout with CorCenCC tags.
	{
	    foreach ($values as $mysurf=>$mycor)
	    {
		$locsurf=explode("#", $mysurf);

		if (!isset($importfile))  // We're running on a website.
                {
                    // If the word is unknown, or the gloss is undisambiguated, mark the word in red. 
                    if (preg_match("/\|/", $mycor) or preg_match("/Gwann/", $mycor))
                    {
                        $wordclass="warnword";
                        $glossclass="warngloss";
                    }
                    else
                    {
                        $wordclass="translation";
                        $glossclass="close";
                    }

                    // Mark up the location, word and tag.
                    echo "($utterance_id, $locsurf[0]) <span class=\"$wordclass\">$locsurf[1]</span><span class=\"$glossclass\">#$mycor</span><br />";
		}
		
		// Write out the tagged text in vertical format in a text file.
		$textline="{".$utterance_id.",".$locsurf[0]."} ".$locsurf[1]."#".$mycor."\n";
		fwrite($fp, $textline);
	    }
	    if (!isset($importfile)) { echo "<br />"; }
	    fwrite($fp, "\n"); 
	}
        else  // Default - use a horizontal layout with Siarad-type glosses.
        {
            if (!isset($importfile))  // We're running on a website.  Give a heading and an option to download the output.
            {
                $chunks=array_chunk($values, 4, true);  // Break the output into sets of 4, to give 4 (tiered) slots per line.  Adjust this if you want more slots per line (eg if you have a wider screen), but note that in that case it would also be worth adjusting the CSS page layout.
    
                // Output the tagged text in a two-row, n-column table (n being the value given in array_chunk() above).
                echo "<table class=\"tight\">";
            
                // Loop through the chunks - each will start a new table row.
                foreach ($chunks as $chunk)
                {
                    echo "<tr>";
                    
                    // Loop through the chunk content.
                    foreach($chunk as $mysurf=>$myauto)
                    {
                        // Mark up the location and word in the top tier.
                        $locsurf=explode("#", $mysurf);
                        // If the word is unknown, or the gloss is undisambiguated, mark the word in red. 
                        if (preg_match("/~/", $myauto) or preg_match("/UNK/", $myauto))
                        {
                            $wordclass="warnword";
                        }
                        else
                        {
                            $wordclass="translation";
                        }
                        echo "<td class=\"surface\"><sup class=\"loc\">$locsurf[0]</sup><span class=\"$wordclass\"> $locsurf[1]</span></td>";
                    }
                    echo "</tr><tr>";  // End the top row and begin the next.
                    
                    // Loop through the chunk content again.
                    foreach($chunk as $myauto)
                    {
                        // Mark up the gloss in the lower tier.
                        // If the word is unknown, or the gloss is undisambiguated, mark the word in red. 
                        if (preg_match("/~/", $myauto) or preg_match("/UNK/", $myauto))
                        {
                            $glossclass="warngloss";
                        }
                        else
                        {
                            $glossclass="close";
                        }
                        echo "<td class=\"$glossclass\">$myauto</td>";
                    }
                    echo "</tr>";  // End the lower row.
                }
                echo "</table><br />";  // Close the table.
            }
            
            // Write out the tagged text in horizontal format in a text file.
            foreach ($values as $mysurf=>$myauto)
            {
                $locsurf=explode("#", $mysurf);
                $textline="{".$utterance_id.",".$locsurf[0]."}".$locsurf[1]."#".$myauto." ";
                $textline=preg_replace("/# $/", "", $textline);  // Remove final slash if punctuation is not being tagged.
                fwrite($fp, $textline);
            }
            fwrite($fp, "\n\n");
        }
	
	// Uncomment this section to debug the arrays.
// 	echo '<pre>'; 
// 	print_r($values);
// 	echo "<br /><br />";
// 	print_r($chunks);
// 	echo '</pre>';

	unset($uttline, $surface, $auto, $corcencc, $enlemma, $translation, $values, $chunks);
}

fclose($fp);

?>
