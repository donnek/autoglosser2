<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for the Welsh language.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License and the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script outputs a pdf version of the glossed file.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

$collection=explode("+", $argv[2]);  // Take a list of options from the second command-line argument (if any) - these should be separated by a +.
// The options are as follows:
// corcencc: The default is to print glosses, but this option prints CorCenCC tags.
// both: The default is to print either glosses or (with the corcencc option) tags, but this option will print both glosses and tags on separate tiers.
// colour: The default is to print in black, but this option will print glosses or tags in the colours selected below (under colour choices).
// nopunc: The default is to print glosses for punctuation marks, but this option will suppress the glosses.
// notrans: The default is to print a translation, but this option will suppress the translation.

// Set up colour choices.
// A wide range of colours is available - see Section 4 of the manual for Uwe Kern's xcolor package: http://mirrors.ctan.org/macros/latex/contrib/xcolor/xcolor.pdf
$gcolour="Blue"; 
$tcolour="Green"; 

//echo "Producing glossed pdf ... please wait ...\n";

$fp = fopen("outputs/$filename/$filename.tex", "w") or die("Can't create the file");

$lines=file("tex/tex_header.tex");  // Open header file containing LaTeX markup to set up the document.
foreach ($lines as $line)
{
	str_replace("filename", $filename, $line);
	fwrite($fp, $line);
}

fwrite($fp, "\n");

// Create a document title based on the filename.
$titletext=tex_surface($filename);
$title="\\title{".$titletext."}\n\author{}\n\date{}\n\n\begin{document}\n\n\maketitle\n\n";
fwrite($fp, $title);

$surface='';
$auto='';
$corcencc='';
$sem='';
$translation='';
$transline='';
// An empty variable has to be set up here, otherwise the concatenation $surface.=$row_w->surface." "; below will result in the first line of the text having a preceding dot.  A similar point applies to $auto, which will otherwise attach the last gloss in the file to the first item.

$sql_s=query("select * from $utterances order by utterance_id;");  // Collect all the utterances.
while ($row_s=pg_fetch_object($sql_s))
{
	$utterance_id=$row_s->utterance_id;
	$transline=$row_s->translation;  // Use a klecs translation (massaged, using additional constraint grammars) if one exists.  Choose below whether to use it or a gist translation ($translation) produced by simply concatenating the English lemmas.
	
	$sql_w=query("select * from $words where utterance_id=$row_s->utterance_id and surface!='¬' order by location;");  // Don't display the end-of-heading marker.
	while ($row_w=pg_fetch_object($sql_w))
	{
		$row_w->surface=tex_surface($row_w->surface);  // Escape stuff to keep LaTeX happy.
		
		if (preg_match("/ /", $row_w->surface)) { $row_w->surface="{".$row_w->surface."}"; }
		if (preg_match("/^http/", $row_w->surface)) { $row_w->surface="{".$row_w->surface."}"; }  // URLs
		if (preg_match("/@/", $row_w->surface)) { $row_w->surface="{".$row_w->surface."}"; }  // emails
		if (preg_match("/^\{$/", $row_w->surface)) { $row_w->surface="\{~"; }  // handle right brace
		if (preg_match("/^\}$/", $row_w->surface)) { $row_w->surface="\}~"; }  // handle left brace
		
		// If the word is unknown, or the gloss is undisambiguated, mark the word in red. 
		if (preg_match("/~/", $row_w->auto) or preg_match("/UNK/", $row_w->auto))
		{
                    $row_w->surface="\\textcolor{red}{".$row_w->surface."}";
		}

		//echo $row_w->surface."\n";
		// Concatenate the surface words.
		$surface.=$row_w->surface." ";  // Note that you  need to set up an empty $surface first - see above.
		
		// Glosses.
		if (preg_match("/^PUNC/", $row_w->auto)) 
		{
                    if (in_array("nopunc", $collection))  // If this option is used ...
                    {
                        $row_w->auto="";  // ... don't tag punctuation marks.
                    }
		}
		
		if (in_array("colour", $collection))  // If this option is used ...
                {
                    $myauto="\\textcolor{{$gcolour}}{".tex_auto($row_w->auto)."}";  // ... print the glosses in the gloss colour (blue by default).  Escape stuff to keep LaTeX happy.
                }
                else
                {
                    $myauto=tex_auto($row_w->auto);
                }
                
                // If the word is unknown, or the gloss is undisambiguated, mark the gloss in red. 
                if (preg_match("/~/", $row_w->auto) or preg_match("/UNK/", $row_w->auto))
                {
                    $myauto="\\textcolor{red}{".tex_auto($row_w->auto)."}";
                }
                
                // Concatenate the glosses.
		$auto.=$myauto." "; // Note that you  need to set up an empty $auto first - see above.
		
		// CorCenCC tags.
		if (in_array("colour", $collection))  // If this option is used ...
                {
                    $mycorc="\\textcolor{".$tcolour."}{".tex_auto($row_w->corcencc)."}";  // ... print the tags in the tag colour (dark green by default).
                }
                else
                {
                    $mycorc=tex_auto($row_w->corcencc);
                }
                
                // If the word is unknown, or the gloss is undisambiguated, mark the tag in red. 
                if (preg_match("/~/", $row_w->auto) or preg_match("/UNK/", $row_w->auto))
                {
                    $mycorc="\\textcolor{red}{".tex_auto($row_w->corcencc)."}";
                }
                
                // Concatenate the tags.
		$corcencc.=$mycorc." "; // Note that you need to set up an empty $corcencc first - see above.
		
// 		$mysem=$row_w->semtag;
// 		$mysem=preg_replace("/%/", "\%", $mysem);
// 		$mysem="\\textcolor{Brown}{".$mysem."}";
// 		$sem.=$mysem." "; // Note that you  need to set up an empty $sem first - see above.
		
		$myenlemma="\\textcolor{Gray}{".tex_surface($row_w->enlemma)."}";  // Generate a gist translation.
		$translation.=$myenlemma." ";
	}
	
        // Create an ExPex entry for the sentence.
	$begingl="\\ex\n\begingl\n";
	fwrite($fp, $begingl);
	
	if ($surface!='')  // Provided there is verbal content in the line ...
	{
		$wsurface="\gla ".$surface." //\n";
		//echo $wsurface."\n";
		fwrite($fp, $wsurface);
		
		$wauto="\glc ".$auto." //\n";  // Autogloss tier.
		//echo $wauto."\n";		
		$wcorcencc="\glc ".$corcencc." //\n";  // Corcencc tier.
		//echo $wcorcencc."\n";
		
		if (in_array("corcencc", $collection))  // If this option is used, print CorCenCC tags instead of glosses.
		{
                    fwrite($fp, $wcorcencc);
		}
		else
		{
                    fwrite($fp, $wauto);
		}

		if (in_array("both", $collection))  // If this option is used, print tags as well as glosses.
                {
                    fwrite($fp, $wcorcencc);  // The glosses are already printed by virtue of the above if clause.
                }
                
		//$wsem="\glc ".$sem." //\n";  // Semtag tier.
		//echo $wsem."\n";
		//fwrite($fp, $wsem);
	}
	
	
	if (!in_array("notrans", $collection))  // If this option is used, don't print a translation.
	{
            // Use a gist translation.
            $wtrans="\glft ".$translation." //\n";
            fwrite($fp, $wtrans);

//             // Use a klecs translation.
//             $transline=preg_replace("/\{/", "\{", $transline);
//             $transline=preg_replace("/\}/", "\}", $transline);
//             $transline="\\textcolor{Gray}{".tex_surface($transline)."}";
//             $wtransline="\glft ".$transline." //\n";
//             fwrite($fp, $wtransline);
        }

	$endgl="\\endgl\n\\xe\n";
	fwrite($fp, $endgl);

	fwrite($fp, "\n");

	unset($surface, $auto, $corcencc, $mysem, $sem, $translation, $wuttline, $wsurface, $wauto, $wcorcencc, $wsem, $wtrans);
}

$lines=file("tex/tex_footer.tex");  // Open footer file.
foreach ($lines as $line)
{
	fwrite($fp, $line);
}

fclose($fp);

exec("xelatex -interaction=nonstopmode -output-directory=outputs/$filename outputs/$filename/$filename.tex 2>&1");
// -interaction=nonstopmode will prevent halt on error. 
// Alternatively, -interaction=batchmode: nonstopmode will print all usual lines, it just won't stop. batchmode will suppress all but a handful of declarative lines ("this is pdfTeX v3.14...").
// and then output to stdout: 2>&1

?>
