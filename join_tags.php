<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script joins the gloss data and combines undisambiguated alternative readings.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

// Create a holding table to hold the gloss data.
drop_existing_table($holding);

$sql_table = "
CREATE TABLE $holding (
    id serial NOT NULL,
    filename character varying(50),
    utterance_id integer,
    location integer,
    lemma character varying(100),
    enlemma character varying(100),
    langid character varying(50),
    auto character varying(250),
    corcencc character varying(250),
    semtag character varying(250)
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$holding." ADD CONSTRAINT ".$holding."_pk PRIMARY KEY (id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

//echo "Aligning glosses with words ... please wait ...\n";

// Initialise variables.
$holder=array();

// Open the output file.
$fp=fopen("outputs/$filename/{$filename}_joined.tsv", "w") or die("Can't create the file");  

$sql=query("select * from $cgfinished order by filename, utterance_id, location, auto;");  // The last is so that undisambiguated tags are listed in alphabetical order.
while ($row=pg_fetch_object($sql))
{
    $lemma=$row->lemma;
    $enlemma=$row->enlemma;
    $langid=$row->langid;
    $auto=strtoupper($row->auto);  // Uppercase the autogloss.
    $corcencc=$row->corcencc;
    // Replace null semtags with Z99 (unmatched).
    //$semtag=($row->semtag=='') ? "Z99" : $row->semtag;
    
    // Attach mutation tags.
    $mutation=($row->mutation=="") ? "" : "+".$row->mutation;
    // Generate mutation tags  in Welsh for corcencc.
    // Note that the ternary version ($cmut=($mutation=="+sm")? "+tm" : null;) does not work here for anything but +h.
    if ($mutation=="+sm") { $cmut="+tm"; }
    if ($mutation=="+am") { $cmut="+tll"; }
    if ($mutation=="+nm") { $cmut="+tt"; }
    if ($mutation=="+h") { $cmut="+h"; }
    
    // Recapitalise enlemmas if they were originally capitalised and are nouns, adjectives, letters or infinitives.  
    if ($row->cap=="cap" and preg_match("/^(n|adj|letter|v\.infin)/", $row->auto))
    {
	$enlemma=ucfirst($enlemma);
    }

    // For multiwords, change adjusted entries to show an arrow pointing to the main word.
    if ($row->adjust=="mw-ls" or $row->adjust=="mw-mid")
    {
	$auto="→";
	$corcencc="→";
    }
    
    // Create the gloss by concatenating the enlemma, auto and mutation fields.
    if (preg_match("/PUNC/", $auto))  // Don't attach punctuation marks to the postag ...
    {
	$gloss=$auto;
    }
    else
    {
	$gloss=pg_escape_string($enlemma.".".$auto.$mutation);  // ... but attach other tags to the enlemma.
    }
    
    $corcencc=$corcencc.$cmut;  // Attach mutation info to the corcencc tags.
    
    if ($row->filename==$subfile and $row->utterance_id==$utt and $row->location==$loc)  // There is non-disambiguation, ie the location data for this record is the same as for the previous one, combine the info ...
    {
        $glo=$glo."~".preg_replace("/^\./", "", $gloss);  // Remove initial dot where there is no preceding enlemma to attach to.
        $corc=$corc."|".$corcencc;
        $lem=($lem==$lemma) ? $lem=$lemma : $lem."~".$lemma;  // Collapse the options if they are identical.
        $enlem."~".$enlemma;
        $lang=($lang==$langid) ? $lang=$lang : $lang=$lang."~".$langid;  // Collapse the options if they are identical.
        $semt=(empty($semt)) ? $semt=$semt : $semt."~".$semtag;
        //echo "repeat: "; 
    }
    else  // ... otherwise, give it as is.
    {
	// Give new names to the variables, so that they can be compared with the ones in the entry - if they are the same, the if clause above will apply, and they will be presented as alternatives (ie undisambiguated).
	$glo=preg_replace("/^\./", "", $gloss);  // Remove initial dot where there is no preceding enlemma to attach to.
        $corc=$corcencc;
        $lem=$lemma;
        $enlem=$enlemma;
        $lang=$langid;
        $semt=$semtag;
        $subfile=$row->filename;
        $utt=$row->utterance_id;
        $loc=$row->location;
        //echo "new: ";
    }
    
    //echo $subfile.",".$utt.",".$loc.": ".$glo." - ".$corc." - ".$lem." - ".$enlem." - ".$lang." - ".$semt."\n";  
    
    $line=$subfile."\t".$utt."\t".$loc."\t".$glo."\t".$corc."\t".$lem."\t".$enlem."\t".$lang."\t".$semt."\n";
    fwrite($fp, $line);
    
    // The above write line writes multiple entries into the holding table when words are undisambiguated, with the entry extended by one gloss each time.  When the entries are transferred from the holding to the words table, this should not matter, because the relevant location will be overwritten repeatedly, leaving only the longest undisambiguated gloss.  However, if this causes problems, the above two lines could be commented and the additional code below uncommented so that only the final (longest) entry is actually written into the holding table.
    
    //----------------------
    // Additional code
    // ---------------------
    // Generate an array keyed to the place info.  With undisambiguated words, the place's value will be overwritten repeatedly as the string is built up, leaving the last (longest) one as the value.
//     $holder[$subfile][$utt][$loc]['filename']=$subfile;
//     $holder[$subfile][$utt][$loc]['utt']=$utt;
//     $holder[$subfile][$utt][$loc]['loc']=$loc;
//     $holder[$subfile][$utt][$loc]['glo']=$glo;
//     $holder[$subfile][$utt][$loc]['corc']=$corc;
//     $holder[$subfile][$utt][$loc]['lem']=$lem;
//     $holder[$subfile][$utt][$loc]['enlem']=$enlem;
//     $holder[$subfile][$utt][$loc]['lang']=$lang;
//     $holder[$subfile][$utt][$loc]['semtag']=$semt;
    
    unset($cmut);
}

//----------------------
// Additional code
// ---------------------
// Loop through the array to build up a line with all the values for that word's info, and write it out to a file.
// foreach ($holder as $subfile)
// {
//     foreach ($subfile as $utt)
//     {
// 	foreach ($utt as $loc)
// 	{
// 	    foreach ($loc as $key => $value)
// 	    {
// 		//echo $key.": ".$value."\n";
// 		$list.=$value."\t";
// 	    }
// 	    $list=substr($list, 0, -1);
// 	    //echo $list."\n";
// 	    fwrite($fp, $list."\n"); 
// 	    unset($list);
// 	}
//     }
// }

// Close the output file.
fclose($fp);

// Import the file to a holding table, and then copy the relevant info from that table to the words table.  We do it this way for speed: it would also be possible to write the array info directly into the words table, but this takes noticeably longer.

// Import the file.
exec($pg_handle." -c \"\copy {$filename}_holding (filename, utterance_id, location, auto, corcencc, lemma, enlemma, langid, semtag) from 'outputs/$filename/{$filename}_joined.tsv' delimiter '\t' quote E'\b' csv;\"");
// To allow double quotes to be imported, we need to change the default quote character to backspace (E'\b'), which should not normally occur in texts.  There seems no way to just switch off the default quote character entirely.
//http://stackoverflow.com/questions/7376322/ignore-quotation-marks-when-importing-a-csv-file-into-postgresql

// Copy over the data.
$sql_t=query("update $words w set lemma=h.lemma, enlemma=h.enlemma, langid=h.langid, auto=h.auto, corcencc=h.corcencc, semtag=h.semtag from $holding h where h.filename=w.filename and h.utterance_id=w.utterance_id and h.location=w.location;");

//Reinsert full stops after abbreviations.
//$sql_a=query("update $words set lemma=lemma||'.', enlemma=enlemma||'.' where surface~'\.$';");


?>