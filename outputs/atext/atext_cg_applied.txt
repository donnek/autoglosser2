"<Mae>" atext,1,1
	"bod" [cym] v 3s pres :is: <B pres 3 u> + cap [59494]
"<Lois>" atext,1,2
	"Lois" [---] name :Lois: <E p> + cap
"<yn>" atext,1,3
	"yn" [cym] prt :: <U tra> [200654]
"<gwneud>" atext,1,4
	"gwneud" [cym] v infin :make: <Be> [202152]
"<cacen>" atext,1,5
	"cacen" [cym] n f sg :cake: <E b ll> [209544]
"<.>" atext,1,6
	"." [---] punc fullstop :.: <Atd t> [214098]

"<A>" atext,2,1
	"a" [cym] prt.int :: <U gof> + cap [201646]
"<fydd>" atext,2,2
	"bod" [cym] v 3s fut :will-be: <B dyf 3u> + sm [61921]
"<rhywfaint>" atext,2,3
	"rhywfaint" [cym] n m sg :amount: <E g u> [206228]
"<o>" atext,2,4
	"o" [cym] prep :of: <Ar sym> [203019]
"<'r>" atext,2,5
	"y" [cym] det.def :the: <YFB> [212393]
"<arian>" atext,2,6
	"arian" [cym] n m sg :money: <E g u> [201313]
"<hwn>" atext,2,7
	"hwn" [cym] adj.dem m sg near :this: <Rha dang g> [196760]
"<yn>" atext,2,8
	"yn" [cym] prt :: <U tra> [200654]
"<cael>" atext,2,9
	"cael" [cym] v infin :get: <Be> [207494]
"<ei>" atext,2,10
	"ei" [cym] adj.poss m 3s :his: <Rha dib 3 g u> [206479]
"<ddefnyddio>" atext,2,11
	"defnyddio" [cym] v infin :use: <Be> + sm [206694]
"<i>" atext,2,12
	"i" [cym] prep :to: <Ar sym> [202346]
"<sicrhau>" atext,2,13
	"sicrhau" [cym] v infin :assure: <Be> [203300]
"<bod>" atext,2,14
	"bod" [cym] v infin :that: <Be> [205519]
"<modd>" atext,2,15
	"modd" [cym] n m sg :means: <E g u> [202816]
"<defnyddio>" atext,2,16
	"defnyddio" [cym] v infin :use: <Be> [206694]
"<tocynnau>" atext,2,17
	"tocyn" [cym] n m pl :ticket: <E g ll> [206496]
"<rhatach>" atext,2,18
	"rhad" [cym] adj.comp :cheaper: <Ans cym> [196833]
"<yn>" atext,2,19
	"yn" [cym] prep :in: <Ar sym> [204430]
"<Lloegr>" atext,2,20
	"Lloegr" [cym] name f sg place :England: <E p b> + cap [208800]
"<yn>" atext,2,21
	"yn" [cym] {mw-ls}
"<ogystal>" atext,2,22
	"ogystal" [cym] adj.eq :as well: <Ans cyf> {mw-rs}
"<ag>" atext,2,23
	"â" [cym] prep :as: <Ar sym> [207161]
"<yng>" atext,2,24
	"yn" [cym] prep :in: <Ar sym> [199349]
"<Nghymru>" atext,2,25
	"Cymru" [cym] name f sg place :Wales: <E p b> + nm + cap [198200]
"<?>" atext,2,26
	"?" [---] punc questmark :?: <Atd t> [214099]

"<Roedd>" atext,3,1
	"bod" [cym] v 3s imperf :was: <B amhen 3 u> + cap [75070]
"<yn>" atext,3,2
	"yn" [cym] prt :: <U tra> [200654]
"<bleser>" atext,3,3
	"pleser" [cym] n m sg :pleasure: <E g u> + sm [197104]
"<gennyf>" atext,3,4
	"gan" [cym] prep+pron 1s :with me: <Ar 1 u> [209258]
"<weld>" atext,3,5
	"gweld" [cym] v infin :see: <Be> + sm [204308]
"<y>" atext,3,6
	"y" [cym] det.def :the: <YFB> [204199]
"<Dirprwy>" atext,3,7
	"dirprwy" [cym] nq m sg :deputy: <E g u> + cap [205555]
"<Weinidog>" atext,3,8
	"gweinidog" [cym] n m sg :minister: <E g u> + sm + cap [201619]
"<dros>" atext,3,9
	"tros" [cym] prep :over: <Ar sym> + sm [203150]
"<Blant>" atext,3,10
	"plentyn" [cym] n m pl :child: <E g ll> + sm + cap [202234]
"<yn>" atext,3,11
	"yn" [cym] prt :: <U tra> [200654]
"<cyhoeddi>" atext,3,12
	"cyhoeddi" [cym] v infin :publish: <Be> [196901]
"<buddsoddiad>" atext,3,13
	"buddsoddiad" [cym] n m sg :investment: <E g u> [204257]
"<o>" atext,3,14
	"o" [cym] prep :of: <Ar sym> [203019]
"<£>" atext,3,15
	"punt" [cym] curr pound :£: <Gw sym> [214089]
"<3>" atext,3,16
	"3" [---] number decimal :3: <Gw dig>
"<miliwn>" atext,3,17
	"miliwn" [cym] n f sg :million: <E b u> [202809]
"<yn>" atext,3,18
	"yn" [cym] prt :: <U tra> [200654]
	"yn" [cym] prep :in: <Ar sym> [204430]
"<rhaglen>" atext,3,19
	"rhaglen" [cym] n f sg :programme: <E b u> [208067]
"<Teuluoedd>" atext,3,20
	"teulu" [cym] n m pl :family: <E g ll> + cap [207129]
"<yn>" atext,3,21
	"yn" [cym] prt :: <U tra> [200654]
"<Gyntaf>" atext,3,22
	"un" [cym] ord :first: <Rhi tref d> + sm + cap [205114]
"<cyn>" atext,3,23
	"cyn" [cym] prep :before: <Ar sym> [202058]
"<y>" atext,3,24
	"y" [cym] det.def :the: <YFB> [204199]
"<Nadolig>" atext,3,25
	"Nadolig" [cym] n m sg :Christmas: <E g u> + cap [196685]
"<.>" atext,3,26
	"." [---] punc fullstop :.: <Atd t> [214098]

"<A>" atext,4,1
	"a" [cym] prt.int :: <U gof> + cap [201646]
"<ydych>" atext,4,2
	"bod" [cym] v 2p pres :be: <B pres 2 ll> [125592]
"<chi>" atext,4,3
	"chi" [cym] pron 2p :you: <Rha pers 2 ll> [205379]
"<felly>" atext,4,4
	"felly" [cym] adv :thus: <Adf> [206323]
"<'n>" atext,4,5
	"yn" [cym] prt :: <U tra> [214067]
"<cefnogi>" atext,4,6
	"cefnogi" [cym] v infin :support: <Be> [208987]
"<mwy>" atext,4,7
	"mawr" [cym] adj.comp :more: <Ans cym> [201716]
"<o>" atext,4,8
	"o" [cym] prep :of: <Ar sym> [203019]
"<dreth>" atext,4,9
	"treth" [cym] n f sg :tax: <E b u> + sm [207151]
"<ar>" atext,4,10
	"ar" [cym] prep :on: <Ar sym> [204320]
"<fonysau>" atext,4,11
	"bonws" [cym] n m pl :bonus: <E g ll> + sm [203602]
"<bancwyr>" atext,4,12
	"bancwr" [cym] n m pl :banker: <E g ll> [214080]
"<neu>" atext,4,13
	"neu" [cym] conj :or: <Cys cyd> [203322]
"<a>" atext,4,14
	"a" [cym] prt.int :: <U gof> [201646]
"<ydych>" atext,4,15
	"bod" [cym] v 2p pres :be: <B pres 2 ll> [125592]
"<chi>" atext,4,16
	"chi" [cym] pron 2p :you: <Rha pers 2 ll> [205379]
"<,>" atext,4,17
	"," [---] punc comma :,: <Atd can> [214061]
"<fel>" atext,4,18
	"fel" [cym] conj :like: <Cys cyd> [204895]
"<gweddill>" atext,4,19
	"gweddill" [cym] n m sg :remainder: <E g u> [198730]
"<eich>" atext,4,20
	"eich" [cym] adj.poss 2p :your: <Rha dib 2 ll> [201308]
"<plaid>" atext,4,21
	"plaid" [cym] n f sg :party: <E b u> [205210]
"<,>" atext,4,22
	"," [---] punc comma :,: <Atd can> [214061]
"<yn>" atext,4,23
	"yn" [cym] prt :: <U tra> [200654]
"<dweud>" atext,4,24
	"dweud" [cym] v infin :say: <Be> [208622]
"<ein>" atext,4,25
	"ein" [cym] adj.poss 1p :our: <Rha dib 1 ll> [208632]
"<bod>" atext,4,26
	"bod" [cym] v infin :be: <Be> [205519]
"<oll>" atext,4,27
	"holl" [cym] adj :all: <Ans cad u> [209474]
"<yn>" atext,4,28
	"yn" [cym] prt :: <U tra> [200654]
	"yn" [cym] prep :in: <Ar sym> [204430]
"<rhan>" atext,4,29
	"rhan" [cym] n f sg :part: <E b u> [204385]
"<o>" atext,4,30
	"o" [cym] prep :of: <Ar sym> [203019]
"<hyn>" atext,4,31
	"hwn" [cym] pron.dem sp near :this: <Rha dang d> [204160]
"<gyda>" atext,4,32
	"gyda" [cym] prep :with: <Ar sym> [209044]
"<'n>" atext,4,33
	"yn" [cym] prt :: <U tra> [214067]
"<gilydd>" atext,4,34
	"cilydd" [cym] n m sg :other: <E g u> + sm [198637]
"<,>" atext,4,35
	"," [---] punc comma :,: <Atd can> [214061]
"<ond>" atext,4,36
	"ond" [cym] conj :but: <Cys cyd> [201902]
"<bod>" atext,4,37
	"bod" [cym] v infin :be: <Be> [205519]
"<rhai>" atext,4,38
	"rhai" [cym] pron :some: <> [198549]
"<ynddi>" atext,4,39
	"ynddi" [cym] prep+pron f 3s :in her: <Ar 3 b u> [204868]
"<'n>" atext,4,40
	"yn" [cym] prt :: <U tra> [214067]
"<fwy>" atext,4,41
	"mawr" [cym] adj.comp :more: <Ans cym> + sm [201716]
"<nag>" atext,4,42
	"na" [cym] conj :than: <Cys cyd> [208865]
"<eraill>" atext,4,43
	"arall" [cym] adj pl :others: <Ans cad ll> [208834]
"<?>" atext,4,44
	"?" [---] punc questmark :?: <Atd t> [214099]

"<Argyfwng>" atext,5,1
	"argyfwng" [cym] n m sg :emergency: <E g u> + cap [201892]
"<2010>" atext,5,2
	"2010" [---] number arabic :2010: <Gw dig>
"<¬>" atext,5,3
	"¬" [---] punc end-head :: <Gw sym> [214772]

"<Maent>" atext,6,1
	"bod" [cym] v 3p pres :be: <B pres 3 ll> + cap [153410]
"<wedi>" atext,6,2
	"wedi" [cym] prep :after: <Ar sym> [207472]
"<ymrwymo>" atext,6,3
	"ymrwymo" [cym] v infin :commit oneself: <Be> [204033]
"<i>" atext,6,4
	"i" [cym] prep :to: <Ar sym> [202346]
"<wneud>" atext,6,5
	"gwneud" [cym] v infin :do: <Be> + sm [202152]
"<hynny>" atext,6,6
	"hwnnw" [cym] pron.dem sp far :that: <Rha dang d> [208539]
"<,>" atext,6,7
	"," [---] punc comma :,: <Atd can> [214061]
"<ac>" atext,6,8
	"a" [cym] conj :and: <Cys cyd> [209088]
"<maent>" atext,6,9
	"bod" [cym] v 3p pres :be: <B pres 3 ll> [153410]
"<yn>" atext,6,10
	"yn" [cym] prt :: <U tra> [200654]
"<gweithio>" atext,6,11
	"gweithio" [cym] v infin :work: <Be> [198783]
"<mor>" atext,6,12
	"mor" [cym] adv :so: <Adf> [208738]
"<galed>" atext,6,13
	"caled" [cym] adj :hard: <Ans cad u> + sm [208814]
"<ag>" atext,6,14
	"â" [cym] prep :with: <Ar sym> [207161]
"<y>" atext,6,15
	"y" [cym] det.def :the: <YFB> [204199]
"<gallant>" atext,6,16
	"gallant" [eng] eng unspec :gallant: <Gw saes>
"<,>" atext,6,17
	"," [---] punc comma :,: <Atd can> [214061]
"<ond>" atext,6,18
	"ond" [cym] conj :but: <Cys cyd> [201902]
"<mae>" atext,6,19
	"bod" [cym] v 3s pres :is: <B pres 3 u> [59494]
"<agweddau>" atext,6,20
	"agwedd" [cym] n f pl :aspect: <E b ll> [203181]
"<cyfreithiol>" atext,6,21
	"cyfreithiol" [cym] adj :legal: <Ans cad u> [208739]
"<ar>" atext,6,22
	"ar" [cym] prep :on: <Ar sym> [204320]
"<hyn>" atext,6,23
	"hwn" [cym] pron.dem sp near :this: <Rha dang d> [204160]
"<,>" atext,6,24
	"," [---] punc comma :,: <Atd can> [214061]
"<ac>" atext,6,25
	"a" [cym] conj :and: <Cys cyd> [209088]
"<ni>" atext,6,26
	"ni" [cym] prt.neg :not: <U neg> [205130]
"<all>" atext,6,27
	"gallu" [cym] v 3s pres :can: <B pres 3 u> + sm [58880]
"<y>" atext,6,28
	"y" [cym] det.def :the: <YFB> [204199]
"<Llywodraeth>" atext,6,29
	"llywodraeth" [cym] n f sg :government: <E b u> + cap [202099]
"<—>" atext,6,30
	"—" [---] punc emdash :—: <Atd can> [214092]
"<Llaw>" atext,6,31
	"llaw" [cym] n f sg :hand: <E b u> + cap [203257]
"<farw>" atext,6,32
	"marw" [cym] adj :dead: <Ans cad u> + sm [209010]
"<dibyniaeth>" atext,6,33
	"dibyniaeth" [cym] n f sg :dependency: <E b u> [205528]
"<wladol>" atext,6,34
	"gwladol" [cym] adj :national: <Ans cad u> + sm [204834]
"<sosialaidd>" atext,6,35
	"sosialaidd" [cym] adj :socialist: <Ans cad u> [202209]
"<yw>" atext,6,36
	"bod" [cym] v 3s pres :is: <B pres 3 u> [60828]
"<hyn>" atext,6,37
	"hwn" [cym] pron.dem sp near :this: <Rha dang d> [204160]
"<.>" atext,6,38
	"." [---] punc fullstop :.: <Atd t> [214098]

"<Rydym>" atext,7,1
	"bod" [cym] v 1p pres :be: <B pres 1 ll> + cap [96237]
"<yn>" atext,7,2
	"yn" [cym] prt :: <U tra> [200654]
"<gwrando>" atext,7,3
	"gwrando" [cym] v infin :listen: <Be> [207781]
"<arnoch>" atext,7,4
	"ar" [cym] prep+pron 2p :on you: <Ar 2 ll> [197460]
"<yn>" atext,7,5
	"yn" [cym] prt :: <U tra> [200654]
"<siarad>" atext,7,6
	"siarad" [cym] v infin :talk: <Be> [204733]
"<am>" atext,7,7
	"am" [cym] prep :for: <Ar sym> [201919]
"<gefnogi>" atext,7,8
	"cefnogi" [cym] v infin :support: <Be> + sm [208987]
"<'r>" atext,7,9
	"y" [cym] det.def :the: <YFB> [212393]
"<economi>" atext,7,10
	"economi" [cym] n mf sg :economy: <E gb u> [198353]
"<,>" atext,7,11
	"," [---] punc comma :,: <Atd can> [214061]
"<ac>" atext,7,12
	"a" [cym] conj :and: <Cys cyd> [209088]
"<yna>" atext,7,13
	"yna" [cym] adv :there: <Adf> [208848]
"<rydych>" atext,7,14
	"bod" [cym] v 2p pres :be: <B pres 2 ll> [125075]
"<yn>" atext,7,15
	"yn" [cym] prt :: <U tra> [200654]
"<cyflwyno>" atext,7,16
	"cyflwyno" [cym] v infin :introduce: <Be> [200945]
"<cynigion>" atext,7,17
	"cynnig" [cym] n m pl :proposals: <E g ll> [196811]
"<fel>" atext,7,18
	"fel" [cym] conj :like: <Cys cyd> [204895]
"<hwn>" atext,7,19
	"hwn" [cym] pron.dem m sg near :this: <Rha dang g> [196760]
"<.>" atext,7,20
	"." [---] punc fullstop :.: <Atd t> [214098]

"<Hyd>" atext,8,1
	"hyd" [cym] n m sg :as far: <E g u> + cap [208306]
"<y>" atext,8,2
	"y" [cym] pron.rel :that: <Rha perth> [200652]
"<gwn>" atext,8,3
	"gwybod" [cym] v 1s pres :know: <B pres 1 u> [209351]
"<i>" atext,8,4
	"mi" [cym] pron 1s :I: <Rha pers 1 u> [204322]
"<,>" atext,8,5
	"," [---] punc comma :,: <Atd can> [214061]
"<nid>" atext,8,6
	"nid" [cym] prt.neg :not: <U neg> [209021]
"<yw>" atext,8,7
	"bod" [cym] v 3s pres :is: <B pres 3 u> [60828]
"<awdurdodau>" atext,8,8
	"awdurdod" [cym] n mf pl :authority: <E gb ll> [201796]
"<lleol>" atext,8,9
	"lleol" [cym] adj :local: <Ans cad u> [208624]
"<wedi>" atext,8,10
	"wedi" [cym] prep :after: <Ar sym> [207472]
"<clywed>" atext,8,11
	"clywed" [cym] v infin :hear: <Be> [204358]
"<eto>" atext,8,12
	"eto" [cym] adv :again: <Adf> [206509]
"<gan>" atext,8,13
	"gan" [cym] prep :by: <Ar sym> [198442]
"<y>" atext,8,14
	"y" [cym] det.def :the: <YFB> [204199]
"<Llywodraeth>" atext,8,15
	"llywodraeth" [cym] n f sg :government: <E b u> + cap [202099]
"<i>" atext,8,16
	"i" [cym] prep :to: <Ar sym> [202346]
"<ddweud>" atext,8,17
	"dweud" [cym] v infin :say: <Be> + sm [208622]
"<bod>" atext,8,18
	"bod" [cym] v infin :that: <Be> [205519]
"<y>" atext,8,19
	"y" [cym] det.def :the: <YFB> [204199]
"<cynllun>" atext,8,20
	"cynllun" [cym] n m sg :plan: <E g u> [204621]
"<yn>" atext,8,21
	"yn" [cym] prt :: <U tra> [200654]
"<mynd>" atext,8,22
	"mynd" [cym] v infin :go: <Be> [198448]
"<i>" atext,8,23
	"i" [cym] prep :to: <Ar sym> [202346]
"<gael>" atext,8,24
	"cael" [cym] v infin :get: <Be> + sm [207494]
"<ei>" atext,8,25
	"ei" [cym] adj.poss m 3s :his: <Rha dib 3 g u> [206479]
"<gyflwyno>" atext,8,26
	"cyflwyno" [cym] v infin :introduce: <Be> + sm [200945]
"<fesul>" atext,8,27
	"fesul" [cym] adv :per: <Adf> [208058]
"<cam>" atext,8,28
	"cam" [cym] n m sg :step: <E g u> [206818]
"<,>" atext,8,29
	"," [---] punc comma :,: <Atd can> [214061]
"<felly>" atext,8,30
	"felly" [cym] adv :thus: <Adf> [206323]
"<ni>" atext,8,31
	"ni" [cym] prt.neg :not: <U neg> [205130]
"<allwn>" atext,8,32
	"gallu" [cym] v 1p pres :can: <B pres 1 ll> + sm [95101]
"<ond>" atext,8,33
	"ond" [cym] conj :but: <Cys cyd> [201902]
"<tybio>" atext,8,34
	"tybio" [cym] v infin :suppose: <Be> [206520]
"<ei>" atext,8,35
	"ei" [cym] adj.poss m 3s :his: <Rha dib 3 g u> [206479]
"<fod>" atext,8,36
	"bod" [cym] v infin :be: <Be> + sm [205519]
"<ar>" atext,8,37
	"ar" [cym] {mw-ls}
"<fin>" atext,8,38
	"fin" [cym] adv :about to: <Adf> + sm {mw-rs}
"<dod>" atext,8,39
	"dod" [cym] v infin :come: <Be> [203977]
"<i>" atext,8,40
	"i" [cym] prep :to: <Ar sym> [202346]
"<ben>" atext,8,41
	"pen" [cym] n m sg :end: <E g u> + sm [207903]
"<.>" atext,8,42
	"." [---] punc fullstop :.: <Atd t> [214098]

