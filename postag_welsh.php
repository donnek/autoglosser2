<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a part-of-speech tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script takes the input from the web-interface starting page and runs it through the Autoglosser2 pipeline.

mb_internal_encoding("UTF-8");
include("./includes/fns.php");
include("/opt/autoglosser2/config.php");

$instring=trim($_POST['welsh_in']);
// echo $instring;
$layout=$_POST['layout'];
//echo $layout."<br />";

// Truncate the text to 300 characters, and strip HTML/PHP tags.
$instring=strip_tags(substr($instring, 0, 300));
//$instring=strip_tags($instring);

$instring=nl2br($instring);  // We need to mark newlines, and then convert them to end-sentence markers, or "headings" that have no end-sentence punctuation will be concatenated to the first sentence following.
$instring=str_replace("<br />", "~", $instring);
//echo $instring."<br />";

// Generate a filename based on date and time, and use that to generate other names.
//$filename="on".exec("date +%Y_%m_%d_%H%M%S");  // for deployment use
$filename="on";  // for local and testing use
$utterances=strtolower($filename."_utterances");
$words=strtolower($filename."_words");
$cgfinished=strtolower($filename."_cgfinished");
$holding=strtolower($filename."_holding");
exec("mkdir -p outputs/$filename");

include("import.php");  // Outputs: _utterances.txt, _sentencised.txt, _utterances table
include("wordify.php");  // Outputs: _words.txt, _wordified.txt, _words table
include("cohorts.php");  // Outputs: _unknowns.txt, _names.txt, _cg.txt
include("apply_cg.php");  // Outputs: _cg_applied.txt, _cg_applied_old, _cg_applied_old_old
include("cgfinished.php");  // Outputs: _cgfinished.txt, _cgfinished table
include("join_tags.php");  // Outputs: _joined.txt, _holding table, updates to _words table
include("txtoutput.php");  // Outputs: _txtoutput.txt
// include("pdfoutput.php");  // Outputs: .tex, pdf

?>