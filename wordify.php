<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script splits each sentence in the utterances table into words.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

// Create the words table.
drop_existing_table($words);

$sql_table = "
CREATE TABLE $words (
    word_id serial NOT NULL,
    utterance_id integer,
    location integer,
    surface character varying(50),
    lemma character varying(100),
    enlemma character varying(100),
    langid character varying(10),
    auto character varying(250),
    corcencc character varying(250),
    filename character varying(50),
    semtag character varying(250)
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$words." ADD CONSTRAINT ".$words."_pk PRIMARY KEY (word_id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

//echo "Tokenising the words ... please wait ...<br />";

// Initialise variables.
$wordstring="";

$fp=fopen("outputs/$filename/{$filename}_words.tsv", "w") or die("Can't create the file");  // Create a file to be read into the db.
$fpnu=fopen("outputs/$filename/{$filename}_wordified.txt", "w") or die("Can't create the file");  // Create a file to hold the tokenised text for review.

$sql=query("select * from $utterances order by utterance_id;");
while ($row=pg_fetch_object($sql))
{
	$i=1; 
	
	$newutt=trim($row->surface);
	
	$newutt=preg_replace("/([[:alnum:],;:\"-])$/", "$1¬", $newutt);

	// Spaced: space out non-text characters.
	$newutt=preg_replace("/([^[:alnum:]\p{M}§¶])/u", " $1 ", $newutt);  // Put spaces around all characters which are not alphanumeric characters, Unicode combining marks (for accents), or the recoding characters § and ¶.
	
	fwrite($fpnu, "Spaced: ".$newutt."\n");  // Print the spaced output to the file.

	// Collapsed: revert the spacing where required.
	$newutt=preg_replace("/ (\\') (ch|di|d|i|ma|m|na|ny|n|re|r|s|th|t|u|ve|w) /", " $1$2 ", $newutt);  // Join the apostrophe to elided words - this should cover both Welsh and English elisions.
	
	$newutt=preg_replace("/ p \\' un/", " p'un ", $newutt);  // p'un (whether)

	//$newutt=preg_replace("/ e \. e \. /", " e.e. ", $newutt);  // Abbreviations.

	$newutt=preg_replace("/ \. (htaccess) /", " .$1 ", $newutt);  // dot files
	$newutt=preg_replace("/ (Last) \. (fm) /", " $1.$2 ", $newutt);  // webnames

	$newutt=preg_replace("/ (https?|ftp) :  \/  \/ (.[^\s]+) \. (.[^\s]+) \. (com|co \. uk|org|org \. uk|ac \. uk) \/ (.[^\s]+) \/ (.[^\s]+) \/ /", " $1://$2.$3.$4/$5/$6/ ", $newutt);  // URLs of form http://www.something.com/dir/subdir/
	$newutt=preg_replace("/ (https?|ftp) :  \/  \/ (.[^\s]+) \. (.[^\s]+) \. (com|co \. uk|org|org \. uk|ac \. uk) \/ (.[^\s]+) \/ /", " $1://$2.$3.$4/$5/ ", $newutt);  // URLs of form http://www.something.com/dir/
	$newutt=preg_replace("/ (https?|ftp) :  \/  \/ (.[^\s]+) \. (.[^\s]+) \. (com|co \. uk|org|org \. uk|ac \. uk) /", " $1://$2.$3.$4 ", $newutt);  // URLs of form http://www.something.com
	$newutt=preg_replace("/\s(www)\s\.\s(.[^\s]+)\s\.\s(com|co \. uk|org|org \. uk|ac \. uk)\s/", " $1.$2.$3 ", $newutt);  // URLs of form www.something.com
	$newutt=preg_replace("/\s(ww)\s\.\s(.[^\s]+)\s\.\s(com|co \. uk|org|org \. uk|ac \. uk)\s/", " $1.$2.$3 ", $newutt);  // URLs of form ww.something.com - ie incorrect
	$newutt=preg_replace("/ (.[^\s]+) @ (.[^\s]+) \. (com|co \. uk|org|org \. uk|ac \. uk) /", " $1@$2.$3 ", $newutt);  // Email addresses of form user@something.com

	$newutt=preg_replace("/(\d)\s(\.|,|-|\/)\s(\d)/", "$1$2$3", $newutt);  // Join digits separated by a decimal divider or a hyphen (2013-14) or a slash (2013/14).
	
	$newutt=preg_replace("/([a-z])\s_\s([a-z])/", "$1_$2", $newutt);  // Join words separated by an underscore.

	$newutt=preg_replace("/\.\s+\.\s+\./", "...", $newutt);  // Join dots in an ellipsis.
	
	$newutt=preg_replace("/([A-Z])\s+(\.)\s+([^\n])/", "$1$2 $3", $newutt);  // Join an acronym fullstop to its previous capital, except when the acronym is at the end of the sentence. Note that we need to allow for the fact that a newline might not be the next character, and repeat the character that is.
	
	$newutt=preg_replace("/([a-zA-Z])\s(-)\s([a-z]+)\s(-)\s([a-z])/", "$1$2$3$4$5", $newutt);  // Join hyphenated items where the second half is not capitalised.  Hyphen is defined here as a dash with only one tokeniser space on either side (a hyphen used as a dash will have more than one tokeniser  space on each side). This deals with words containing two hyphens, eg du-a-gwyn, di-ben-draw.
	$newutt=preg_replace("/([a-zA-Z])\s(-)\s([a-z])/", "$1$2$3", $newutt);  // Join hyphenated items where the second half is not capitalised.  This covers ordinary words with hyphenated components (cyd-destunoli), but leaves dual nouns (Llafur-Plaid, Abertawe-Caerdydd) untouched. This deals with words containing one hyphen.
	//$newutt=preg_replace("/([Cc]yd|[Dd]i|[Ôô]l)\s(-)\s([A-Z])/", "$1$2$3", $newutt);  
        $newutt=hyphcomp($newutt);  // Join hyphenated items where both sides are capitalised (eg in headings), and the first side consists of listed components.  Add others as necessary.

	//$newutt=preg_replace("/ ([D|d])on (\\'t) /", " $1o n$2 ", $newutt);  // Re-segment: don't.  Needs more work.
	
	// Tidy up.
	$newutt=preg_replace("/\s+/", " ", $newutt);  // Collapse spaces, tabs, etc.
	
	//echo $newutt."\n\n";
	fwrite($fpnu, "Collapsed: ".$newutt."\n");  // Print the revised spaced output to the file, so that it can be compared with the initial output.

	$surface_words=array_filter(explode(' ', $newutt));  // Split on whitespace.  Use array_filter to throw away empty parts.
	
	foreach ($surface_words as $surface_word)
	{
	    if (preg_match("/(¶|§)/", $surface_word))  // AltGr+R, Shift+AltGr+S
	    {
		$surface_word=preg_replace("/¶/", ".", $surface_word);  // Replace with fullstop.
		$surface_word=preg_replace("/§/", ". ", $surface_word);  // Replace with fullstop+space.
	    }
	    else
	    {
		$surface_word=trim($surface_word);
		//echo $surface_word."\n";
	    }
	    
	    $wordline=$row->utterance_id."\t".$i."\t".$surface_word."\t".$row->filename."\n";
	    fwrite($fp, $wordline);
	    
	    $wordstring.=$surface_word." ";

	    $i=++$i; 
	}
	
	fwrite($fpnu, "Final: ".$wordstring."\n\n");  // Print the final tokenised text to the file.
	
	unset($newutt, $wordstring);
}

fclose($fpnu);
fclose($fp);

exec($pg_handle." -c \"\copy {$filename}_words(utterance_id,location,surface,filename) from 'outputs/$filename/{$filename}_words.tsv' delimiter '\t' quote E'\b' csv;\"");
// To allow double quotes to be imported, we need to change the default quote character to backspace (E'\b'), which should not normally occur in texts.  There seems no way to just switch off the default quote character entirely.
//http://stackoverflow.com/questions/7376322/ignore-quotation-marks-when-importing-a-csv-file-into-postgresql

?>
