<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script imports the data from a CyTag xml output file.

include("includes/fns.php");
include("/opt/autoglosser2/config.php");

// Enter the name of the xml source file and the name of the evaluation table to be created from it.
$xmlfile="tagger_evaluation/inputs/cytag.xml";
$eval="cytag_eval";

// Create a comparison table.
drop_existing_table($eval);

$sql_table = "
CREATE TABLE $eval (
    id serial NOT NULL,
    filename character varying(50),
    utterance_id integer,
    location integer,
    token_id integer,
    surface character varying(50),
    lemma character varying(100),
    corcencc character varying(100),
    para_id integer,
    sentence_id integer
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$eval." ADD CONSTRAINT ".$eval."_pk PRIMARY KEY (id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

$fp = fopen("tagger_evaluation/outputs/$eval/{$eval}.txt", "w") or die("Can't create the file");  // Open a file for the sentencised text to be copied into the db table.

//$feed = file_get_contents($xmlfile);  // These two lines give the same result as the _load_file line below
//$mysents = simplexml_load_string($feed);

$mysents = simplexml_load_file($xmlfile);  // This loads the file using the outer node, in this case <corpus>, so we don't need to repeat that in subsequent calls.

// Remove empty nodes.
// http://stackoverflow.com/questions/5559551/remove-multiple-empty-nodes-with-simplexml#12706060
$xpath = '//*[not(text())]';
foreach ($mysents->xpath($xpath) as $remove)
{
    unset($remove[0]);
}

echo "Importing from $xmlfile to $eval\n";

foreach ($mysents->children() as $file)
{
    $subfile=$file['name'];
    $subfile=strtolower(basename(preg_replace("/\..*$/", "", $subfile)));  // remove the extension
    //echo $filename ."\n";
    
    foreach ($file->children() as $para)	
    { 
	$para_id=$para['id'];
	//echo $para_id."\n";
	
	foreach ($para->children() as $sentence)	
	{ 
	    $sentence_id=$sentence['para_id'];
	    //echo $sentence_id."\n";
	    $utterance_id='';
	    
	    foreach ($sentence->children() as $token)	
	    { 
		$location=$token['sent_id'];
		$token_id=$token['id'];
		$lemma=$token['lemma'];
		$corcencc=$token['rich_pos'];
		$surface=$token;
		fwrite($fp, $subfile."\t".$utterance_id."\t".$location."\t".$token_id."\t".$surface."\t".$lemma."\t".$corcencc."\t".$para_id."\t".$sentence_id."\n");
	    }
	}
    }
}

fclose($fp);

exec($pg_handle." -c \"\copy $eval (filename, utterance_id, location, token_id, surface, lemma, corcencc, para_id, sentence_id) from 'outputs/{$eval}.txt' delimiter '\t' quote E'\b' csv;\"");
// To allow double quotes to be imported, we need to change the default quote character to backspace (E'\b'), which should not normally occur in texts.  There seems no way to just switch off the default quote character entirely.
//http://stackoverflow.com/questions/7376322/ignore-quotation-marks-when-importing-a-csv-file-into-postgresql

//======================
// The following section generates a new set of utterance_ids for each subfile in the imported file, so that we can bypass CyTag's paragraph and sentence numbers - those sequences can have gaps, and the exact numbers depend on the whitespace layout of the input text.

echo "Generating new utterance_ids\n";

$sql=query("select distinct filename from $eval order by filename;");
while ($row=pg_fetch_object($sql))
{
    $subfile=$row->filename;
    //echo "File: ".$subfile."\n";
    
    $i=1;
    
    $sql2=query("select distinct para_id from $eval where filename='$subfile' order by para_id;");
    while ($row2=pg_fetch_object($sql2))
    {
	$para=$row2->para_id;
	//echo "Paragraph: ".$para."\n";
    
	$sql3=query("select distinct sentence_id from $eval where filename='$subfile' and para_id=$para order by sentence_id;");
	while ($row3=pg_fetch_object($sql3))
	{
	    $sent=$row3->sentence_id;
	    //echo $sent." > ".$i."\n";

	    query("update $eval set utterance_id=$i where filename='$subfile' and para_id=$para and sentence_id=$sent;");

	    $i++;
	}
    }
    unset($i);
}

?>