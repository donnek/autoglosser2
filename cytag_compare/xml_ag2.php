<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script loads words from an imported CyTag xml file into a _words table, so that an Autoglosser2 pipeline can be started.
// Run as: php tagger_evaluation/xml_ag2.php cytag_eval.ag2 (or whatever the AG2 reference name is to be).
if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

$eval="cytag_eval";  // The CyTag table to be used as the input.

echo "Copying words from $eval to $words\n";

// Create the words table.
drop_existing_table($words);

$sql_table = "
CREATE TABLE $words (
    word_id serial NOT NULL,
    utterance_id integer,
    location integer,
    surface character varying(50),
    lemma character varying(100),
    enlemma character varying(100),
    langid character varying(50),
    auto character varying(250),
    corcencc character varying(250),
    filename character varying(50),
    semtag character varying(250),
    para_id integer,
    sentence_id integer
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$words." ADD CONSTRAINT ".$words."_pk PRIMARY KEY (word_id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

$sql_e=query("insert into $words (utterance_id, location, surface, filename, para_id, sentence_id) select utterance_id, location, surface, filename, para_id, sentence_id from $eval;");

?>