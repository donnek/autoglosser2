<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script creates a pdf aligning the output of the taggers being evaluated.

include("/opt/autoglosser2/config.php");
include("includes/fns.php");

$filename="cytag_eval_ag2";  // reference name
$target="acyeval";  // db table with the combined evaluation output

$fp = fopen("tagger_evaluation/outputs/$filename/{$filename}.tex", "w") or die("Can't create the file");

$lines=file("tex/tex_header.tex");  // Open header file containing LaTeX markup to set up the document.
foreach ($lines as $line)
{
	str_replace("filename", $filename, $line);
	fwrite($fp, $line);
}

fwrite($fp, "\n");
$titletext=tex_surface($filename);
$title="\\title{".$titletext."}\n\author{}\n\date{}\n\n\begin{document}\n\n\maketitle\n\n";
fwrite($fp, $title);

$intro="\\noindent The first line is the Welsh text.  The second line is the human postagging.  The third line is the CyTag postagging.  The fourth line is the Autoglosser2 (AG2) postagging.  When a postag is the same as in the line above (eg a CyTag postag is the same as the human one, or an AG2 postag is the same as the CyTag one), it is marked by ··. When a postag is the same as in two lines above (eg an AG2 postag is the same as the human one), it is marked by •.  Words tagged \"unk\" (CyTag) or \"Gwann\" (AG2) are missing from the version of Eurfa being used.  Words tagged -?- in AG2 have no CorCenCC tag in the version of Eurfa being used.  Each sentence is given its own number, with the file, paragraph and sentence in the original XML files appended in square brackets at the end of the sentence.  The superscript numbers on the Welsh line give the location of that word in that sentence.\n\n";
fwrite($fp, $intro);

$surface="";
$ctag="";
$htag="";
$atag="";

$sql=query("select distinct filename from $target order by filename;");
while ($row=pg_fetch_object($sql))
{
    $subfile=$row->filename;
    //echo "\nFile: ".$subfile."\n";
    $tsubfile=tex_surface($row->filename);
    
    $sql2=query("select distinct para_id from $target where filename='$subfile' order by para_id;");
    while ($row2=pg_fetch_object($sql2))
    {
	$para=$row2->para_id;
	//echo "\nParagraph: ".$para."\n";
    
	$sql3=query("select distinct sentence_id from $target where filename='$subfile' and para_id=$para order by sentence_id;");
	while ($row3=pg_fetch_object($sql3))
	{
	    $sent=$row3->sentence_id;
	    //echo "\n(".$utt.") ";
	    
	    $sql4=query("select * from $target where filename='$subfile' and para_id=$para and sentence_id=$sent order by location;");
	    while ($row4=pg_fetch_object($sql4))
	    {
		// ·· = same as line one up. • = same as line two up.
		$row4->ctag=($row4->ctag==$row4->htag ? $row4->ctag="··" : $row4->ctag);
		$row4->atag=($row4->atag==$row4->ctag ? $row4->atag="··" : $row4->atag);
		$row4->atag=($row4->atag==$row4->htag ? $row4->atag="•" : $row4->atag);

		$loc="\\textsuperscript{".$row4->location."}";
		
		$row4->surface=tex_surface($row4->surface);  // comment out _ and % to keep LaTeX happy.
		if (preg_match("/ /", $row4->surface)) { $row4->surface="{".$row4->surface."}"; }  // If there are spaces.
		$surface.=$row4->surface.$loc." ";
		
		$ctag.=$row4->ctag." ";
		
		if (preg_match("/ /", $row4->htag)) { $row4->htag="{".$row4->htag."}"; }  // If there are spaces.
		$htag.=$row4->htag." ";
		
		if (preg_match("/^$/", $row4->atag)) { $row4->atag="{-?-}"; }  // If there are nulls (ie no corcencc tag in the dictionary entry.
		$atag.=$row4->atag." ";
		
	    }
	    
	    $begingl="\\ex\n\begingl\n";
	    fwrite($fp, $begingl);

	    $wsurface="\gla ".$surface." //\n";
	    fwrite($fp, $wsurface);
	    
	    $whtag="\glc ".$htag." //\n";
	    fwrite($fp, $whtag);
	    
	    $wctag="\glc ".$ctag." //\n";
	    fwrite($fp, $wctag);
	    
	    $watag="\glc ".$atag." //\n";
	    fwrite($fp, $watag);
	    
	    $locdata="\glft \\trailingcitation{\\rm [".$tsubfile.", ".$para.", ".$sent."]} //\n";
	    fwrite($fp, $locdata);
	    
	    $endgl="\\endgl\n\\xe\n";
	    fwrite($fp, $endgl);

	    fwrite($fp, "\n");
	
	    unset ($utt, $surface, $ctag, $htag, $atag);

	}
    }
}

$lines=file("tex/tex_footer.tex");  // Open footer file.
foreach ($lines as $line)
{
	fwrite($fp, $line);
}

fclose($fp);

exec("xelatex -interaction=nonstopmode -output-directory=tagger_evaluation/outputs/$filename tagger_evaluation/outputs/$filename/$filename.tex 2>&1");
// -interaction=nonstopmode will prevent halt on error. 
// Alternatively, -interaction=batchmode: nonstopmode will print all usual lines, it just won't stop. batchmode will suppress all but a handful of declarative lines ("this is pdfTeX v3.14...").
// and then output to stdout: 2>&1

?>