<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for the Welsh language.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script imports the output of the CyTag process into a PostgreSQL table.

include("includes/fns.php");  // ...  so load some necessary functions ...
include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...

$inpath="/home/kevin/data/corcencc/WP2/CyTag/outputs/myk3/myk3_output.csv";
$outtable="kynulliad3_cytag";

// Create the utterances table.
drop_existing_table($outtable);

$sql_table = "
CREATE TABLE $outtable (
    word_id serial NOT NULL,
    tok_id integer,
    uttloc character varying(50),
    utterance_id integer,
    location integer,
    surface character varying(50),
    lemma character varying(50),
    cyt character varying(50),
    cytag character varying(50),
    mutation character varying(10)
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$outtable." ADD CONSTRAINT ".$outtable."_pk PRIMARY KEY (word_id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

exec($pg_handle." -c \"\copy $outtable (tok_id, surface, uttloc, lemma, cyt, cytag, mutation) from '$inpath' delimiter '\t' quote E'\b' csv;\"");
// To allow double quotes to be imported, we need to change the default quote character to backspace (E'\b'), which should not normally occur in texts.  There seems no way to just switch off the default quote character entirely.
//http://stackoverflow.com/questions/7376322/ignore-quotation-marks-when-importing-a-csv-file-into-postgresql

// Split the uttloc string.
$sql_utt=query("update kynulliad3_cytag set utterance_id=substring(uttloc, '^(\d+),')::int;");
$sql_loc=query("update kynulliad3_cytag set location=substring(uttloc, ',(\d+)$')::int;");

// The following queries allow CyTag and AG2 output to be compared...

// Compare the cytag output table with the autoglosser2 words table:
//create table acomp as select k.word_id, k.utterance_id, k.location, k.surface, k.lemma, k.enlemma, k.auto, k.corcencc, c.word_id as cword_id, c.tok_id, c.utterance_id as cutterance_id, c.location as clocation, c.surface as csurface, c.lemma as clemma, c.cytag, c.mutation from kynulliad3_words k left join kynulliad3_cytag c on k.word_id=c.tok_id order by k.word_id, cword_id, clocation;

//select * from acomp where word_id=tok_id and utterance_id!=cutterance_id order by word_id limit 1;

//update kynulliad3_cytag set tok_id=tok_id+1 where tok_id>=3329;

//drop table acomp;

//select * from acomp order by word_id;

//select word_id, utterance_id, location, surface, auto, corcencc, cytag from acomp where word_id=tok_id and surface=csurface and corcencc!=cytag order by corcencc, cytag;


?>
