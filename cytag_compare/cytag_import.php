<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script imports text already tokenised by CyTag.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

// Create the utterances table.
drop_existing_table($utterances);

$sql_table = "
CREATE TABLE $utterances (
    utterance_id serial NOT NULL,
    surface text,
    translation text,
    filename character varying(50)
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$utterances." ADD CONSTRAINT ".$utterances."_pk PRIMARY KEY (utterance_id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

//echo "Segmenting the utterances ... please wait ...\n";

if (isset($importfile))  // We're running at the command line.
{
    $lines=file($importfile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
}
else  // We're running on a website.
{
    $lines=$instring;
}
//print_r($lines);

$fp = fopen("outputs/$filename/{$filename}_utterances.tsv", "w") or die("Can't create the file");  // Open a file for the sentencised text to be copied into the db table.

foreach ((array) $lines as $line)
{
	$line=trim(preg_replace("/\s+/", " ", $line));  // Collapse spaces, tabs, etc.
	
	$lineline=$line."|".$filename."\n";
	fwrite($fp, $lineline);
}

fclose($fp);

exec($pg_handle." -c \"\copy {$filename}_utterances(surface,filename) from 'outputs/$filename/{$filename}_utterances.txt' delimiter '|' quote E'\b' csv;\"");
// To allow double quotes to be imported, we need to change the default quote character to backspace (E'\b'), which should not normally occur in texts.  There seems no way to just switch off the default quote character entirely.
//http://stackoverflow.com/questions/7376322/ignore-quotation-marks-when-importing-a-csv-file-into-postgresql

?>