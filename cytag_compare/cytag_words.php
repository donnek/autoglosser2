<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for the Welsh language.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script generates a populated words table for text already tokenised by CyTag.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished, $holding)=get_filename();  // ... and generate some variable names.
}

// Create the words table.
drop_existing_table($words);

$sql_table = "
CREATE TABLE $words (
    word_id serial NOT NULL,
    utterance_id integer,
    location integer,
    surface character varying(50),
    lemma character varying(100),
    enlemma character varying(100),
    langid character varying(50),
    auto character varying(250),
    corcencc character varying(250),
    filename character varying(50),
    semtag character varying(250)
);
";
$result_table=pg_query($db_handle, $sql_table);

$sql_pkey = "
ALTER TABLE ONLY ".$words." ADD CONSTRAINT ".$words."_pk PRIMARY KEY (word_id);
";
$result_pkey=pg_query($db_handle, $sql_pkey);

//echo "Tokenising the words ... please wait ...<br />";

// Initialise variables.
$wordstring="";

$fp=fopen("outputs/$filename/{$filename}_words.txt", "w") or die("Can't create the file");  // Create a file to be read into the db.

$sql=query("select * from $utterances order by utterance_id;");
while ($row=pg_fetch_object($sql))
{
	$i=1; 
	
	$newutt=trim($row->surface);
	$newutt=preg_replace("/\s+/", " ", $newutt);  // Collapse spaces, tabs, etc.
	//echo $newutt."\n\n";

	$surface_words=array_filter(explode(' ', $newutt));  // Split on whitespace.  Use array_filter to throw away empty parts.
	
	foreach ($surface_words as $surface_word)
	{
	    $surface_word=trim($surface_word);
	    
	    $wordline=$row->utterance_id."\t".$i."\t".$surface_word."\t".$row->filename."\n";
	    fwrite($fp, $wordline);
	    
	    $i=++$i; 
	}
	unset($newutt);
}

fclose($fp);

exec($pg_handle." -c \"\copy {$filename}_words(utterance_id,location,surface,filename) from 'outputs/$filename/{$filename}_words.txt' delimiter E'\t' quote E'\b' csv;\"");
// To allow double quotes to be imported, we need to change the default quote character to backspace (E'\b'), which should not normally occur in texts.  There seems no way to just switch off the default quote character entirely.
//http://stackoverflow.com/questions/7376322/ignore-quotation-marks-when-importing-a-csv-file-into-postgresql

?>
