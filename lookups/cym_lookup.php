<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for the Welsh language.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License and the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This file handles dictionary lookups in the Welsh dictionary, eurfa.  First, the surface word is demutated, and then each of these is looked up, but only where the demutated word is actually different from the surface word.  Note that demutated words need to be looked up separately since in a few cases there are homophonous pairs, one with a mutation and one without.

// This revised file simplifies the lookup by decapitalising words and expanding the [mb] and [gl] output of the desoft() function.  It also uses the same code for each lookup, instead of repeating the code as in the previous version.  Finally, it caches the readings so that they don't need to be looked up again.

// Uncomment these lines for stand-alone testing.
//include("includes/fns.php");
//include("/opt/autoglosser2/config.php");
//$surface='lan';
//echo $surface."\n";

// Initialise variables.
$dictlist=array();
$mutarray=array();
$cap="";
$myupper="";
$fullentry="";

// Store the surface form.
$mutarray['surf']=$surface;
//echo $mutarray['surf']."<br />";

// If the word is capitalised, lowercase it and store that; also note that it was capitalised.
$lowsurface=lcfirst($surface);
if ($lowsurface!=$surface)
{
    $cap='+ cap';
    $mutarray['low']=$lowsurface;
}

// If removing soft mutation gives a different word, store that.  Expand [mb] and [gl] so that we can use the same sql query for all entries.
$desoftsurface=de_soft($lowsurface);
if ($desoftsurface!=$lowsurface)
{
    if ($desoftsurface[0]=="[")  // If we have a couplet starting with [ ...
    {
        $bits=explode("]", $desoftsurface);  // ... split at the end of the couplet ....
        $inits=explode("|", substr($bits[0], 1));  // ... then split within the couplet (also removing the [ ) ...
        $init1=$inits[0];  // ... get the first element of the couplet  ...
        $init2=$inits[1];  // ... and the second element ...
        $final=$bits[1];  // ... and the rest of the word ...
        $mutarray['soft1']=$init1.$final;  // ... and create two new words without the couplet ...
        $mutarray['soft2']=$init2.$final;
        $mutarray['soft1u']=ucfirst($init1.$final);  // ... and also capitalise them.
        $mutarray['soft2u']=ucfirst($init2.$final);	
    }
    else
    {
        $mutarray['soft1']=$desoftsurface;
        $mutarray['soft1u']=ucfirst($desoftsurface);  // Check for capitalised words too, otherwise "Geltaidd" will not be found.
    }
}

// If removing nasal mutation gives a different word, store that.
$denassurface=de_nas($lowsurface);
if ($denassurface!=$lowsurface)
{
    $mutarray['nas']=$denassurface;
    $mutarray['nasu']=ucfirst($denassurface);
}

// If removing aspirate mutation gives a different word, store that.
$deaspsurface=de_asp($lowsurface);
if ($deaspsurface!=$lowsurface)
{
    $mutarray['asp']=$deaspsurface;
    $mutarray['aspu']=ucfirst($deaspsurface);
}

// If removing h gives a different word, store that.
$dehsurface=de_h($lowsurface);
if ($dehsurface!=$lowsurface)
{
    $mutarray['h']=$dehsurface;
    $mutarray['hu']=ucfirst($dehsurface);
}

$nodotsurface=preg_replace("/(\.|\s)/", "", $surface);  // Remove fullstops and spaces in acronyms, to allow lookup of all 3 types of acronym (BBC, B.B.C. and B. B. C.) against one dictionary entry (BBC), and store that.  Note that changing $surface above directly to remove dot or space will result in fullstops being misclassified as decimal numbers.
if ($nodotsurface!=$surface)
{
    $mutarray['dots']=$nodotsurface;
}
// Note that the above means that acronyms and abbreviations will lose their fullstops in the gloss, eg "Dr." in the input will be output as "Dr", and "B.B.C." as "BBC".  If retention of the fullstops in the gloss is necessary (unlikely), these lines need to be removed, and multiple entries ("Dr", "Dr.") would need to be added to the dictionary.  Alternatively, fullstops could be inserted after each capital in an identified acronym, or the surface could be copied into lemma and enlemma.

// Lowercase everything to handle things like "Cyd-Destunoli" in titles, and store that; also note that it was capitalised.
if(mb_strtoupper($surface, 'UTF-8')!=$surface)  // Ha!  Only run this code if the word is not all uppercase.  If it is all uppercase, the word (eg TED) may be processed (eg depluralised), leaving an initial (eg T) that is found in the dictionary; this will then be preferred instead of the code to class the word as an abbreviation.
{
    $hylow=mb_strtolower($surface, 'UTF-8');  // We need to handle UTF-8 accents. 
    if ($hylow!=$surface)
    {
        $cap='+ cap';
        $mutarray['hyl']=$hylow;  // Note: This array entry is not used anywhere else ...
    }
}

// Remove all empty elements from the array.
$slim=array_filter($mutarray);
//echo "<pre>";
//print_r($mutarray);
//print_r($slim);
//echo "</pre>";
// Loop through the array, marking which mutations, if any, were removed.
foreach($slim as $mutation=>$value)
{
    if ($mutation=='surf')
    {
        $mutation='';    
    }
    elseif ($mutation=='low' or $mutation=='dots')
    {
        $mutation='';    
    }
    elseif ($mutation=='soft1' or $mutation=='soft1u' or $mutation=='soft2' or $mutation=='soft2u')
    {
        $mutation='+ sm';
    }
    elseif ($mutation=='nas' or $mutation=='nasu')
    {
        $mutation='+ nm';    
    }
    elseif ($mutation=='asp' or $mutation=='aspu')
    {
        $mutation='+ am';    
    }
    elseif ($mutation=='h' or $mutation=='hu')
    {
        $mutation='+ h';    
    }
    //echo $mutation.": ".$value."\n";

    $myvalue=de_pluralise($value);  // Generate a new value with possible plural endings removed, and look that up too.
    //echo $myvalue."\n";
        
    // Lookup each stored item: "order by id" in the query ensures that readings will always be listed in the same order, important if you want to diff multiple versions of the _cg file.
    $sql1=query_eurfa("select * from eurfa where surface='$value' or surface='$myvalue' order by id;");
    while ($row1=pg_fetch_object($sql1))
    {   
        if (!in_array($row1->id, $dictlist))  // In other words, if we haven't already picked out this entry.
        {
            $dictlist[]=$row1->id;  // Store the id.
            $subsurface=pg_escape_string($row1->surface);  // The original lookup was escaped; we therefore need to escape this lookup too, or when we test for depluralisation below ($surface!=$value), we will get a disparity in entries which contain a quote (eg 'n), causing them to be wrongly marked as plural.  Note also that for the lookup we need to choose a different variable name for the surface field, otherwise the last $surface in this lookup's sequence (which may, for instance, be demutated) will be copied in as the [$surface] in the cache: so the lookup entries for "hyn" will end up getting listed in the cache as the lookup entries for "yn", and erroneously quoted for other instances of "yn". 
            
            // Keep any spaces or brackets in the code in this section, not in the $entry line below, so that if an item is not present space will not be left for it in the $entry - additional spaces may upset some of the CG rules.
            $lemma=$row1->lemma;
            $pos=$row1->pos." ";
            $langid=(preg_match("/punc/", $pos)) ? "[---] " : "[cym] ";  // Punctuation is in Eurfa too, but mark it as having no language rather than being Welsh.
            $gender=($row1->gender=='') ? "" : $row1->gender." ";
            $number=($row1->number=='') ? "" : $row1->number." ";
            if ($subsurface!=$value) // If the lookup was different from the original (ie it has been depluralised) ...
            {
                if ($pos=='n ')  //  ... and the item is a noun (note that we need a final space because of the $pos row above) ...
                {
                    $number='pl ';  // ... set $number to pl instead of sg.
                    if ($row1->corcencc=='E g u') { $row1->corcencc="E g ll"; }
                    if ($row1->corcencc=='E b u') { $row1->corcencc="E b ll"; }
                    if ($row1->corcencc=='E gb u') { $row1->corcencc="E gb ll"; }
                }
                else
                {
                    $number='invalid-pl ';  // ... set $number to invalid.
                }
            }
            $tense=($row1->tense=='') ? "" : $row1->tense." ";
            $posplus=($row1->posplus=='') ? "" : $row1->posplus." ";
            $corcencc="<".$row1->corcencc."> ";
            $mutation=($mutation=='') ? "" : $mutation." ";
            $cap=($cap=='') ? "" : $cap." ";
            $id="[".$row1->id."] ";
            //$semtag="!!!".$row1->semtag." ";
            $enlemma=":".$row1->enlemma.": ";

            $entry="\t\"".$lemma."\" ".$langid.$pos.$gender.$number.$tense.$posplus.$enlemma.$corcencc.$mutation.$cap.$id."\n";
            
            fwrite($fp, $entry);  // Write.
            //echo $entry;  // Show the readings from new lookups.
            
            // Chain all the readings for the word into a single entry, to be added to the list of words already looked up.
            $fullentry.=$entry;
            
            unset($entry);  // Clear the decks.
        }
    }
}

// Do a very basic English lookup.  This simply checks whether a word might be English - it doesn't give POS info.
$sql2=query("select * from saesneg where word='$lowsurface' order by id;");
while ($row2=pg_fetch_object($sql2))
{
    $dictlist[]=1001;  // Store the id - a placeholder in this case.
    
    $entry="\t\"".$surface."\" [eng] eng misc :".$surface.": <Gw saes> ".$cap."\n";
    //echo $entry;  // Show the readings from the English lookup.
    fwrite($fp, $entry); // Write.
    
    // Chain all the readings for the word into a single entry, to be added to the list of words already looked up.
    $fullentry.=$entry;
    
    unset($entry);  // Clear the decks.
}

// Mark words using Chinese characters.
if (preg_match("/\p{Han}+/u", $surface))
{
    $dictlist[]=1501;  // Store the id - a placeholder in this case.
    
    $entry="\t\"".$surface."\" [han] han misc :".$surface.": <Gw han> ".$cap."\n";
    fwrite($fp, $entry); // Write.
    
    // Chain all the readings for the word into a single entry, to be added to the list of words already looked up.
    $fullentry.=$entry;
    
    unset($entry);  // Clear the decks.
}

// Mark words using Cyrillic characters.
if (preg_match("/\p{Cyrillic}+/u", $surface))
{
    $dictlist[]=1601;  // Store the id - a placeholder in this case.
    
    $entry="\t\"".$surface."\" [cyr] cyr misc :".$surface.": <Gw cyr> ".$cap."\n";
    fwrite($fp, $entry); // Write.
    
    // Chain all the readings for the word into a single entry, to be added to the list of words already looked up.
    $fullentry.=$entry;
    
    unset($entry);  // Clear the decks.
}

// Mark URLs.  This is ver basic, and could be improved.
if (preg_match("/\.(com|co.uk|org|org.uk|ac.uk)/", $surface))
{
    $dictlist[]=4040;  // Store the id - a placeholder in this case.
    
    $entry="\t\"".$surface."\" [---] url :".$surface.": <Gw ann> \n";
    fwrite($fp, $entry); // Write.
    
    // Chain all the readings for the word into a single entry, to be added to the list of words already looked up.
    $fullentry.=$entry;
    
    unset($entry);  // Clear the decks.
}

// Guess unallocated words (where we haven't found anything in the dictionary after the lookups of the various forms above).
if (count($dictlist)<1)  
{
    if (preg_match("/^[\d,][^\.]+au$/", $surface))  // If the item only consists of digits and thousands separator (,) only, followed by -au, assume it's a decade with an -s after.
    {
	    $decade=substr($surface, 0, -2);
	    $entry="\t\"".$surface."\" [---] num yr pl :".$decade."s: <Gw dig>\n";
    }
    elseif (preg_match("/^[\d,][^\.]+$/", $surface))  // If the item only consists of digits and thousands separator (,) only, assume it's a number in Arabic numerals.
    {
	    $entry="\t\"".$surface."\" [---] num ar :".$surface.": <Gw dig>\n";
    }
    elseif (preg_match("/^[\d\.,]+$/", $surface))  // If the item only consists of digits and and a decimal point or a comma, assume it's a decimal number.
    {
	    $entry="\t\"".$surface."\" [---] num dec :".$surface.": <Gw dig>\n";
    }
    elseif (preg_match("/^[IVXLDCM]+$/", $surface))  // If the item only consists of Roman numerals, assume it's a number in Roman numerals.
    {
	    $entry="\t\"".$surface."\" [---] num rom :".$surface.": <Gw rhuf>\n";
    }
    elseif (preg_match("/^[A-Z\.?\s ?]{2,}$/", $surface))  // If the item only consists of at least two capital letters, with or without a following fullstop, with or without a following space, assume it's an abbreviation or acronym.
    {
	    $entry="\t\"".$surface."\" [---] let abb :".$surface.": <Gw talf>\n";
	    fwrite($fpn, $surface."\n");  // And make a note of it in the names list.
    }
    elseif (preg_match("/^[A-Z]/", $surface))  // If the item begins with a capital, assume it's a name.
    {
	    $entry="\t\"".$surface."\" [---] name :".$surface.": <E p> ".$cap."\n";
	    fwrite($fpn, $wordform."\n");  // And make a note of it in the names list.  Since $surface is escaped, we need to use the unescaped form ($wordform), or the entries will have an escaped apostrophe.
    }
    else  // Otherwise, mark it as unknown.
    {
	    $entry="\t\"".$surface."\" [---] unk :".$surface.": <Gw ann>\n";
	    fwrite($fpu, $wordform."\n");  // And make a note of it in the unknowns list.  Since $surface is escaped, we need to use the unescaped form ($wordform), or the entries will have an escaped apostrophe.
    }
    
    //echo $entry;  // Show residual readings.
    fwrite($fp, $entry); // Write.
   
   // Chain all the readings for the word into a single entry, to be added to the list of words already looked up.
    $fullentry.=$entry;
    
    unset($entry);  // Clear the decks.
}


?>