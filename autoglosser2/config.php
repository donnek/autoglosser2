<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// IMPORTANT!  The "live" version of this file is at /opt/autoglosser2.  Changes made to this file will therefore have no effect on an autoglosser install - they have to be made (ie copied) to the one at /opt/autoglosser2.

// PHP connection details for the Autoglosser2 db - the one which holds the output of the autoglosser.
$db_handle=pg_connect('host=localhost dbname=autoglosser2 user=kevin password=kevindbs');

// PHP connection details for the Eurfa db.  The dictionary is in a separate db, which allows a single copy to be accessed by multiple NLP applications.
$db_handle_eurfa=pg_connect('host=localhost dbname=eurfa user=kevin password=kevindbs');

// PostgreSQL connection details for the autoglosser2 db.  These are used when calling the shell from PHP.
$pg_handle="PGPASSWORD='kevindbs' psql -h localhost -U kevin -d autoglosser2";

?>
