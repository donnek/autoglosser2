<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a part-of-speech tagger for Welsh.  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This page provides the starting page for the web-interface to Autoglosser2.

// ini_set('display_errors', 1); error_reporting(E_ALL);

// Bring in the header and functions.
include("includes/header.php");
include("includes/fns.php");

include("includes/langinit.php");  // Langswitcher. Note that commenting out without also commenting out the lang_changer include below will mean that nothing is displayed on the page.

?>

<!--- Body holder --->
<div class="col_12">

<?php

echo "<table><tr><td>";
echo _("<h2>Gloss parts of speech in Welsh</h2>");
echo "</td><td class=\"right\">";
include("includes/lang_changer.php");
echo "</td></tr></table>";

echo _("<p>You can test Autoglosser2 by using the web interface below, which handles up to 300 characters of text (on average, around 50 words) - anything over that will not be processed.  Just type your Welsh text into the box, and press <strong>Gloss it!</strong>. If you can't think of any Welsh to use, try some of the samples below the box.  The glossing works best if you enter \"written\" Welsh, i.e. with correct spelling and punctuation.  Words that are not in Eurfa yet (which may also affect the glossing accuracy) are marked in red.  If you need to tag large amounts of text, it's best to install Autoglosser2 on your local machine - see the <a href=\"./resources/manual.pdf\">manual</a> - Appendix A has full installation instructions.</p>");

?>

<!--- Box area --->  
<div id="box">

<!--- Welsh box input --->
<div class="col_5">

<form class="vertical" id="welsh_form">
	<!--Absolutely essential for the form item(s) to be given a name - this is what the passed-on data will be known as.-->
	<textarea name="welsh_in" id="welsh_in" placeholder="<?php echo _("Enter up to 300 characters of Welsh text here."); ?>"></textarea>
	<input type="checkbox" id="nopunc" name="nopunc"/>
	<label for="nopunc" class="inline"><?php echo _("In glosses, don't tag punctuation"); ?></label>
	<br />
	<h6>Layout format:</h6> <!-- Choose between 4 different output formats. -->
	<input type="radio" id="autoglosser" name="layout" value="autoglosser" checked/>
	<label for="autoglosser" class="inline"><?php echo _("Glosses, horizontal layout"); ?></label>
	<br />
	<input type="radio" id="agvertical" name="layout" value="agvertical"/>
	<label for="agvertical" class="inline"><?php echo _("Glosses, vertical layout"); ?></label>
	<br />
	<input type="radio" id="cchorizontal" name="layout" value="cchorizontal"/>
	<label for="cchorizontal" class="inline"><?php echo _("CorCenCC tags, horizontal layout"); ?></label>
	<br />
	<input type="radio" id="ccvertical" name="layout" value="ccvertical"/>
	<label for="ccvertical" class="inline"><?php echo _("CorCenCC tags, vertical layout"); ?></label>
	<br />
	<br />
	<!--The dosubmit() calls the Javascript function below, which passes on the data and pastes the result back into this page.-->
	<input type="button" onclick="dosubmit_box()" value="<?php echo _("Gloss it!"); ?>">
</form>

<!--- Submit the Welsh text for conversion --->
<script>
function dosubmit_box() 
{
	new Ajax.Updater
	( 
		'postagged_text',  // The name of the <div> where the results will be shown.
		'postag_welsh.php',  // The script doing the processing.
		{ 
			method: 'post',  // Submission method for the form data.
			parameters: $('welsh_form').serialize()  // Name of the form (id=this).
		} 
	);
	//$('welsh_form').reset();  // Uncomment to clear the input form on each submit.
}
</script>

<br />

<?php echo _("<h6>Samples for testing</h6>"); ?>

<p>Mae Lois yn gwneud cacen. Mae Steffan wedi mynd i'r siop.  Mae Owain yn darllen llyfr.  Lle mae Rhian? Ydy hi yn yr ardd?</p>

<p>Dim ond lleuad borffor<br />
Ar fin y mynydd llwm,<br />
A sŵn hen afon Prysor<br />
Yn canu yn y cwm.</p>

<p>Tra'n astudio yno, mynychodd sioe gan yr hypnotydd Martin Taylor a chafodd ei ysbrydoli i ddilyn gyrfa mewn hypnosis a lledrith.</p>

<p>Cadwyd at ffiniau siroedd Lloegr ond yng Nghymru cymhlethwyd y sefyllfa drwy ychwanegu cynghorau dosbarth at rai siroedd a rhannu eraill.</p>

<p>Mae'r polisi wedi cael ei gysylltu â chynnydd yn y nifer o erthyliadau gorfodol, babanladdiad benywaidd, a than-adrodd genedigaethau benywaidd, ac awgryma rhai taw dyma yw'r rheswm tu ôl anghydbwysedd rhyw Tsieina.</p>

<p>Fe'i lleolir mewn ardal ffrwythlon, diolch i ddyfroedd yr Ewffrates; tyfu ffrwythau, grawnfwyd a chynhyrchu brethyn yw'r prif ddiwydiannau.</p>

<p>Y tro hwn, bydd Dai Jones, Llanilar yn cael cwmni'r gantores Linda Griffiths, wrth iddi droedio bro ei mebyd yn Sir Drefaldwyn.  Byddant hefyd yn mwynhau'r golygfeydd ac awyr iach o gwmpas ei chartref presennol yn ardal Penybont.</p>

</div><!--- End Welsh box input --->

<!--- POStagged box output --->
<div class="col_7">
	<div id="postagged_text">
	<p class="info">
	    <?php echo _("The glossed/tagged text will appear here."); ?><br />
	</p>
	<img alt="spinner" id="spinner" src="images/ajax-loader.gif" style="display:none;" />
	</div>
</div><!--- End POStagged box output --->

</div><!--- End box area --->

<hr>

<?php

echo _("<h4>Background</h4>");

echo _("<p>Autoglosser2 is a glosser/tagger for Welsh, using the <a href=\"http://eurfa.org.uk\">Eurfa</a> Welsh dictionary.  The Autoglosser2 code (GPL) is in a <a href=\"https://donnek@bitbucket.org/donnek/autoglosser2.git\">Bitbucket repository</a>, and a detailed <a href=\"./resources/manual.pdf\">manual</a> is available.  Autoglosser2 is a heavily revised version of the <a href=\"https://github.com/donnekgit/autoglosser\">Bangor Autoglosser</a>, which was developed to gloss the <a href=\"http://bangortalk.org.uk\">Bangor corpora</a> of multilingual conversational text.  Autoglosser2, on the other hand, is aimed at written rather than spoken Welsh text, and has been refactored to tidy the code and make it far faster (over 22,000 glosses per minute).</p>");

echo _("<p><strong>References:</strong></p>");

?>

<p>Broersma, M., D. Carter, and K. Donnelly (2020). <a href="https://kevindonnelly.org.uk/resources/words/Broersma2020_Triggered_Codeswitching.pdf" target="_blank">Triggered codeswitching: Lexical processing and conversational dynamics.</a> <em>Bilingualism: Language and Cognition.</em> 23(2):295-308.</p>

<p>Deuchar, M., P. Webb-Davies, and K. Donnelly (2018). <em><a href="https://www.benjamins.com/catalog/scl.81" target="_blank">Building and Using the Siarad Corpus: Bilingual conversations in Welsh and English.</a></em> Studies in Corpus Linguistics 81. John Benjamins.</p>

<p>Carter, D., M. Broersma, and K. Donnelly (2016). <a href="https://kevindonnelly.org.uk/resources/words/Carter2016_Computing_Innovations.pdf" target="_blank">Applying computing innovations to bilingual corpus analysis.</a> In: A. Alba de la Fuente, E. Valenzuela, and C. Martínez-Sanz (Eds.), <em>Language Acquisition Beyond Parameters: Studies in Honour of Juana M. Liceras</em>, Number 51 in Studies in Bilingualism, pp. 281–301. John Benjamins.</p>

<p>Deuchar, M., K. Donnelly, and C. Piercy (2016). <a href="https://kevindonnelly.org.uk/resources/words/Deuchar2016_Monolingual_Minority.pdf" target="_blank">Mae pobl monolingual yn minority: Factors favouring the production of code-switching by Welsh-English speakers.</a> In: M. Durham and J. Morris (Eds.), <em>Sociolinguistics in Wales</em>, pp. 209–239. Palgrave Macmillan.</p>

<p>Donnelly, K. and M. Deuchar (2011). <a href="https://kevindonnelly.org.uk/resources/words/Donnelly2011_CG_Multilingual.pdf" target="_blank">Using constraint grammar in the Bangor Autoglosser to disambiguate multilingual spoken text.</a> In: <em>Constraint Grammar Applications: Proceedings of the NODALIDA 2011 Workshop, Riga, Latvia</em>, NEALT Proceedings Series, Tartu, pp. 17–25.</p>

<?php
// Bring in the footer.
include("includes/footer.php");
?>
