<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/

// This script imports the data from a GPC xml headword file.  Place this script in a folder containing the headword files, and run it, piping the output into a tsv file that can be opened in a spreadsheet: php xml_get_gpc.php > extraction.tsv.

foreach (glob("gpc*") as $xmlfile)  // Loop through the files.
{   
    $data = simplexml_load_file($xmlfile);  // This loads the file using the outer node, in this case <entry>, so we don't need to repeat that in subsequent calls.
    
    // Get the headword(s).
    // Where the node names contain a dot or hyphen, we need to bracket them, otherwise the "-" in "->" will be interpreted as a minus sign - https://stackoverflow.com/questions/3214153/parse-error-syntax-error-unexpected-t-object-operator#3214171.
    foreach ($data->head->{"headword.grp"}->children() as $headwords)  // Only a single node (headword-form) is below this, so no need to specify it.
    {
	$wheadwords.=$headwords."+";  // Add a + to separate multiple entries.
    }
    
    $pron=$data->head->pronunciation->i;
    
    // Define a variable to check for the existence of a tail node.
    $tail=$data->tail->variants->variant;
    
    // Get non-standard headword variants from the tail section, if it exists.
    if (!empty($tail))
    {
	foreach ($data->tail->variants->variant->children() as $varwords)
	{
	    $wvarwords.=$varwords."+";  // Add a + to separate multiple entries.
	}
    }
    
    $allpos=$data->head->{"part-of-speech"}->asXml();
    $nallpos=preg_replace("/<\/?part-of-speech>/", "", $allpos);
    $allpos=preg_replace("/<part-of-speech.[^>]*>/", "", $nallpos);
  
    // Get the part-of-speech.
    $pos=$data->head->{"part-of-speech"}->pos;
    
    // Get the plural(s).
    foreach ($data->head->{"part-of-speech"}->i as $pl)
    {
	$wpl.=$pl."+";  // Add a + to separate multiple entries.
    }
    
    // Define a variable to check for one sense or many.
    $enguniq=$data->body->sense->english;
    $engmulti=$data->body->sense->sense;
    
    // Get the meaning(s).
    if (!empty($enguniq))  // If there is only one sense.
    {
	$meaning=preg_replace("/[,;].*$/", "", $enguniq);  // Extract only as far as the first comma or semicolon.
	$english=$meaning."+";  // We need to add a + because of the $wenglish removal below; otherwise part of the word will be removed.
    }
    elseif (!empty($engmulti))  // If there is more than one sense.  We need to specifically check for this because some files do not have these nodes (eg -eiddio).
    {     
	foreach ($data->body->sense->children() as $sense)  // Multiple nodes (welsh, english) are below this, so unlike the headwords one, we need to specify which one.
	{ 
	    $meaning=$sense->english;
	    $meaning=preg_replace("/[,;].*$/", "", $meaning);  // Extract only as far as the first comma or semicolon.
	    $english.=$meaning."+";  // Add a + to separate multiple entries.
	}
    }
    
    // Tidy up the extracted data.
    $wxmlfile=substr($xmlfile, 0, -4);  // Strip off the .xml file extension.
    $wxmlfile=substr($wxmlfile, 3);  // Strip off the gpc before the number.

    $wheadwords=trim(substr($wheadwords, 0, -1));  // Strip off the final +.
    $wheadwords=preg_replace("/^.*\+\+/", "", $wheadwords);  // Remove first person singular forms given as verb headwords.
    
    $wvarwords=preg_replace("/\+[0-9]\+$/", "+", $wvarwords);  // These lines deal with daft XML shit which means you can't select the correct node.
    $wvarwords=preg_replace("/\+[0-9]\+/", "++", $wvarwords);
    $wvarwords=preg_replace("/^.*\+\+/", "", $wvarwords);  // Remove first person singular forms given as verb varwords.
    $wvarwords=trim(substr($wvarwords, 0, -1));  // Strip off the final +.

    $wpl=trim(substr($wpl, 0, -1));  // Strip off the final +.

    $wenglish=trim(substr($english, 0, -1)); // Strip off the final +.
     
    // Print the extracted data in a tab-separated line - this will be piped into the .tsv file.
    echo $wxmlfile."\t".$wheadwords."\t".$pron."\t".$wvarwords."\t".$pos."\t".$wpl."\t".$wenglish."\t".$allpos."\n"; 
    
    // Clear the variables, ready for the next file.
    unset($wxmlfile, $wheadwords, $pron, $wvarwords, $allpos, $pos, $wpl, $english, $wenglish);
}

?>