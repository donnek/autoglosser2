<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License and the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This file can be used (with edits) to generate a number of tagged layouts which include a list of the semtags in each sentence.

if (empty($filename))  // If the filename hasn't been provided by the do_everything script, we're running standalone ...
{
	include("includes/fns.php");  // ...  so load some necessary functions ...
	include("/opt/autoglosser2/config.php");  // ... get connection details for the db ...
	list($importfile, $filename, $utterances, $words, $cgfinished)=get_filename();  // ... and generate some variable names.
}

$fp = fopen("outputs//$filename/{$filename}.tex", "w") or die("Can't create the file");

$lines=file("tex/tex_header.tex");  // Open header file containing LaTeX markup to set up the document.
foreach ($lines as $line)
{
	str_replace("filename", $filename, $line);
	fwrite($fp, $line);
}

fwrite($fp, "\n");
$titletext="Alternative layouts for semtagging";
$title="\\title{".$titletext."}\n\author{}\n\date{}\n\n\begin{document}\n\n\maketitle\n\n";
fwrite($fp, $title);

$surface='';
$auto='';
$sem='';
$egs='';
// An empty variable has to be set up here, otherwise the concatenation $surface.=$row_w->surface." "; below will result in the first line of the text having a preceding dot.  A similar point applies to $auto, which will otherwise attach the last gloss in the file to the first item.

$sql_s=query("select * from $utterances order by utterance_id;");
while ($row_s=pg_fetch_object($sql_s))
{	
	$uttline=tex_surface($row_s->surface);
	$trans=tex_surface($row_s->translation);
		
    $sql_w=query("select * from $words where utterance_id=$row_s->utterance_id order by location;");
	while ($row_w=pg_fetch_object($sql_w))
	{
		if (preg_match("/ /", $row_w->surface))
		{
			$row_w->surface="{".$row_w->surface."}";
		}
		
		$row_w->surface=tex_surface($row_w->surface);  // comment out _ and % to keep LaTeX happy.
		$surface.=$row_w->surface." ";  // Note that you  need to set up an empty $surface first - see above.
		
		$row_w->auto=tex_auto($row_w->auto);
		$auto.=$row_w->auto."$$ "; // Note that you  need to set up an empty $auto first - see above.
		
		$mysem=$row_w->semtag;
		if (!in_array($mysem, $semegs))
		{
		    $semegs[]=$mysem;
		}
		$semegs=array_filter($semegs);
		asort($semegs);
		$mysem="\\textcolor{Blue}{".$mysem."}";
		$sem.=$mysem." "; // Note that you  need to set up an empty $sem first - see above.
	    }

// 	$begingl="\\ex\n\begingl\n";
// 	fwrite($fp, $begingl);
	$beginpanel="\\ex[everypanel=\\footnotesize]\n\beginglpanel[ssratio=0.6,glhangstyle=none]\n";
	fwrite($fp, $beginpanel);
	
	if ($surface!='')  // Provided there is verbal content in the line ...
	{
		$wsurface="\gla ".$surface." //\n";
		echo $wsurface."\n";
		fwrite($fp, $wsurface);
		
		$wauto="\glb ".$auto." //\n";  // Autogloss tier.
		echo $wauto."\n";
		fwrite($fp, $wauto);
		
		$wsem="\glc ".$sem." //\n";  // Semtag tier.
		echo $wsem."\n";
		fwrite($fp, $wsem);
	}
	
	$wtrans="\glft ".$trans." //\n";
	echo $wtrans."\n";
	fwrite($fp, $wtrans);
	
// 	$endgl="\\endgl\n\\xe\n";
	$endgl="\\endgl\n";
	fwrite($fp, $endgl);
	
	//print_r($semarray);
	foreach ($semegs as $semeg)
	{
		$sql_sem=query("select * from semtags where semtag='$semeg';");
		while ($row_sem=pg_fetch_object($sql_sem))
		{
			$semwords=preg_replace("/ \(.[^\)]+\)/", "", $row_sem->examples);  // remove the Brown tags
			$semwords=preg_replace("/%/", "\%", $semwords);
			$semwords=preg_replace("/&/", "\&", $semwords);
			$semitem="\\textcolor{Blue}{\\textbf{".$row_sem->semtag."}}: \\textbf{".$row_sem->welsh."} (\\textit{".$semwords."}) \\\\ \n";
// 			$semitem="{\\footnotesize\\textcolor{Blue}{\\textbf{".$row_sem->semtag."}}: \\textbf{".$row_sem->welsh."} (\\textit{".$semwords."})} -- \n";

			fwrite($fp, $semitem);
		}
	}

	$endpanel="\\endpanel\n\\xe\n";
	fwrite($fp, $endpanel);

	fwrite($fp, "\n");

	unset($uttline, $surface, $auto, $mysem, $sem, $wuttline, $wsurface, $wauto, $wsem, $semegs);
}

$lines=file("tex/tex_footer.tex");  // Open footer file.
foreach ($lines as $line)
{
	fwrite($fp, $line);
}

fclose($fp);

exec("xelatex -interaction=nonstopmode -output-directory=outputs/$filename outputs/$filename/$filename.tex 2>&1");
// -interaction=nonstopmode will prevent halt on error. 
// Alternatively, -interaction=batchmode: nonstopmode will print all usual lines, it just won't stop. batchmode will suppress all but a handful of declarative lines ("this is pdfTeX v3.14...").
// and then output to stdout: 2>&1

?>
