<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

include("includes/fns.php");
include("/opt/autoglosser2/config.php");

/*
$sql=query_cylist("update cylist set corcencc='E g u' where pos='n' and gender='m' and number='sg';");
$sql=query_cylist("update cylist set corcencc='E b u' where pos='n' and gender='f' and number='sg';");
$sql=query_cylist("update cylist set corcencc='E g ll' where pos='n' and gender='m' and number='pl';");
$sql=query_cylist("update cylist set corcencc='E b ll' where pos='n' and gender='f' and number='pl';");
$sql=query_cylist("update cylist set corcencc='E gb u' where pos='n' and gender='mf' and number='sg';");
$sql=query_cylist("update cylist set corcencc='E gb ll' where pos='n' and gender='mf' and number='pl';");
$sql=query_cylist("update cylist set corcencc='E p g' where pos='name' and gender='m';");
$sql=query_cylist("update cylist set corcencc='E p b' where pos='name' and gender='f';");
$sql=query_cylist("update cylist set corcencc='E p' where pos='name';");

$sql=query_cylist("update cylist set corcencc='YFB' where pos='det.def';");

$sql=query_cylist("update cylist set corcencc='Ar sym' where pos='prep';");
$sql=query_cylist("update cylist set corcencc='Ar 1 u' where pos='prep+pron' and number='1s';");
$sql=query_cylist("update cylist set corcencc='Ar 2 u' where pos='prep+pron' and number='2s';");
$sql=query_cylist("update cylist set corcencc='Ar 3 g u' where pos='prep+pron' and gender='m' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Ar 3 b u' where pos='prep+pron' and gender='f' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Ar 1 ll' where pos='prep+pron' and number='1p';");
$sql=query_cylist("update cylist set corcencc='Ar 2 ll' where pos='prep+pron' and number='2p';");
$sql=query_cylist("update cylist set corcencc='Ar 3 ll' where pos='prep+pron' and number='3p';");

$sql=query_cylist("update cylist set corcencc='Cys cyd' where pos='conj';");  # Edit for Cys is.

$sql=query_cylist("update cylist set corcencc='Rhi fol g' where pos='num' and gender='m';");
$sql=query_cylist("update cylist set corcencc='Rhi fol b' where pos='num' and gender='f';");
$sql=query_cylist("update cylist set corcencc='Rhi fol d' where pos='num';");
$sql=query_cylist("update cylist set corcencc='Rhi fol t' where pos='numt';");
$sql=query_cylist("update cylist set corcencc='Rhi tref' where pos='ord' and gender='m';");
$sql=query_cylist("update cylist set corcencc='Rhi tref' where pos='ord' and gender='f';");
$sql=query_cylist("update cylist set corcencc='Rhi tref d' where pos='ord' and gender='';");
$sql=query_cylist("update cylist set corcencc='Rhi tref t' where pos='ordt' and gender='';");

$sql=query_cylist("update cylist set corcencc='Ans cad u' where pos='adj';");  # Edit for Ans cad b u, Ans cad ll
$sql=query_cylist("update cylist set corcencc='Ans cyf' where pos='adj.eq';");
$sql=query_cylist("update cylist set corcencc='Ans cym' where pos='adj.comp';");
$sql=query_cylist("update cylist set corcencc='Ans eith' where pos='adj.sup';");

$sql=query_cylist("update cylist set corcencc='Adf' where pos='adv';");

$sql=query_cylist("update cylist set corcencc='B e' where pos='v' and tense='infin';");

$sql=query_cylist("update cylist set corcencc='B presdyf 1 u' where pos='v' and tense='pres' and number='1s';");
$sql=query_cylist("update cylist set corcencc='B presdyf 2 u' where pos='v' and tense='pres' and number='2s';");
$sql=query_cylist("update cylist set corcencc='B presdyf 3 u' where pos='v' and tense='pres' and number='3s';");
$sql=query_cylist("update cylist set corcencc='B presdyf 1 ll' where pos='v' and tense='pres' and number='1p';");
$sql=query_cylist("update cylist set corcencc='B presdyf 2 ll' where pos='v' and tense='pres' and number='2p';");
$sql=query_cylist("update cylist set corcencc='B presdyf 3 ll' where pos='v' and tense='pres' and number='3p';");
$sql=query_cylist("update cylist set corcencc='B presdyf amhers' where pos='v' and tense='pres' and number='0';");

$sql=query_cylist("update cylist set corcencc='B presdyf 3 perth' where pos='v' and tense='pres.rel' and number='3s';");

$sql=query_cylist("update cylist set corcencc='B gorb 1 u' where pos='v' and tense='pluperf' and number='1s';");
$sql=query_cylist("update cylist set corcencc='B gorb 2 u' where pos='v' and tense='pluperf' and number='2s';");
$sql=query_cylist("update cylist set corcencc='B gorb 3 u' where pos='v' and tense='pluperf' and number='3s';");
$sql=query_cylist("update cylist set corcencc='B gorb 1 ll' where pos='v' and tense='pluperf' and number='1p';");
$sql=query_cylist("update cylist set corcencc='B gorb 2 ll' where pos='v' and tense='pluperf' and number='2p';");
$sql=query_cylist("update cylist set corcencc='B gorb 3 ll' where pos='v' and tense='pluperf' and number='3p';");
$sql=query_cylist("update cylist set corcencc='B gorb amhers' where pos='v' and tense='pluperf' and number='0';");

$sql=query_cylist("update cylist set corcencc='B amhen 1 u' where pos='v' and tense='imperf' and number='1s';");
$sql=query_cylist("update cylist set corcencc='B amhen 2 u' where pos='v' and tense='imperf' and number='2s';");
$sql=query_cylist("update cylist set corcencc='B amhen 3 u' where pos='v' and tense='imperf' and number='3s';");
$sql=query_cylist("update cylist set corcencc='B amhen 1 ll' where pos='v' and tense='imperf' and number='1p';");
$sql=query_cylist("update cylist set corcencc='B amhen 2 ll' where pos='v' and tense='imperf' and number='2p';");
$sql=query_cylist("update cylist set corcencc='B amhen 3 ll' where pos='v' and tense='imperf' and number='3p';");
$sql=query_cylist("update cylist set corcencc='B amhen amhers' where pos='v' and tense='imperf' and number='0';");

$sql=query_cylist("update cylist set corcencc='B gorff 1 u' where pos='v' and tense='past' and number='1s';");
$sql=query_cylist("update cylist set corcencc='B gorff 2 u' where pos='v' and tense='past' and number='2s';");
$sql=query_cylist("update cylist set corcencc='B gorff 3 u' where pos='v' and tense='past' and number='3s';");
$sql=query_cylist("update cylist set corcencc='B gorff 1 ll' where pos='v' and tense='past' and number='1p';");
$sql=query_cylist("update cylist set corcencc='B gorff 2 ll' where pos='v' and tense='past' and number='2p';");
$sql=query_cylist("update cylist set corcencc='B gorff 3 ll' where pos='v' and tense='past' and number='3p';");
$sql=query_cylist("update cylist set corcencc='B gorff amhers' where pos='v' and tense='past' and number='0';");

$sql=query_cylist("update cylist set corcencc='B gorff sef' where surface='daru';");

#$sql=query_cylist("update cylist set corcencc='B gorch 1 u' where pos='v' and tense='imper' and number='1s';");  # No entries
$sql=query_cylist("update cylist set corcencc='B gorch 2 u' where pos='v' and tense='imper' and number='2s';");
$sql=query_cylist("update cylist set corcencc='B gorch 3 u' where pos='v' and tense='imper' and number='3s';");
$sql=query_cylist("update cylist set corcencc='B gorch 1 ll' where pos='v' and tense='imper' and number='1p';");
$sql=query_cylist("update cylist set corcencc='B gorch 2 ll' where pos='v' and tense='imper' and number='2p';");
$sql=query_cylist("update cylist set corcencc='B gorch 3 ll' where pos='v' and tense='imper' and number='3p';");
$sql=query_cylist("update cylist set corcencc='B gorch amhers' where pos='v' and tense='imper' and number='0';");

$sql=query_cylist("update cylist set corcencc='B dibdyf 1 u' where pos='v' and tense='subj' and number='1s';");
$sql=query_cylist("update cylist set corcencc='B dibdyf 2 u' where pos='v' and tense='subj' and number='2s';");
$sql=query_cylist("update cylist set corcencc='B dibdyf 3 u' where pos='v' and tense='subj' and number='3s';");
$sql=query_cylist("update cylist set corcencc='B dibdyf 1 ll' where pos='v' and tense='subj' and number='1p';");
$sql=query_cylist("update cylist set corcencc='B dibdyf 2 ll' where pos='v' and tense='subj' and number='2p';");
$sql=query_cylist("update cylist set corcencc='B dibdyf 3 ll' where pos='v' and tense='subj' and number='3p';");
$sql=query_cylist("update cylist set corcencc='B dibdyf amhers' where pos='v' and tense='subj' and number='0';");

$sql=query_cylist("update cylist set corcencc='B amod 1 u' where pos='v' and tense='cond' and number='1s';");
$sql=query_cylist("update cylist set corcencc='B amod 2 u' where pos='v' and tense='cond' and number='2s';");
$sql=query_cylist("update cylist set corcencc='B amod 3 u' where pos='v' and tense='cond' and number='3s';");
$sql=query_cylist("update cylist set corcencc='B amod 1 ll' where pos='v' and tense='cond' and number='1p';");
$sql=query_cylist("update cylist set corcencc='B amod 2 ll' where pos='v' and tense='cond' and number='2p';");
$sql=query_cylist("update cylist set corcencc='B amod 3 ll' where pos='v' and tense='cond' and number='3p';");
$sql=query_cylist("update cylist set corcencc='B amod amhers' where pos='v' and tense='cond' and number='0';");

$sql=query_cylist("update cylist set corcencc='Rha pers 1 u' where pos='pron' and number='1s';");
$sql=query_cylist("update cylist set corcencc='Rha pers 2 u' where pos='pron' and number='2s';");
$sql=query_cylist("update cylist set corcencc='Rha pers 3 g u' where pos='pron' and gender='m' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Rha pers 3 b u' where pos='pron' and gender='f' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Rha pers 1 ll' where pos='pron' and number='1p';");
$sql=query_cylist("update cylist set corcencc='Rha pers 2 ll' where pos='pron' and number='2p';");
$sql=query_cylist("update cylist set corcencc='Rha pers 3 ll' where pos='pron' and number='3p';");

$sql=query_cylist("update cylist set corcencc='Rha dib 1 u' where pos='adj.poss' and number='1s';");
$sql=query_cylist("update cylist set corcencc='Rha dib 2 u' where pos='adj.poss' and number='2s';");
$sql=query_cylist("update cylist set corcencc='Rha dib 3 g u' where pos='adj.poss' and gender='m' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Rha dib 3 b u' where pos='adj.poss' and gender='f' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Rha dib 1 ll' where pos='adj.poss' and number='1p';");
$sql=query_cylist("update cylist set corcencc='Rha dib 2 ll' where pos='adj.poss' and number='2p';");
$sql=query_cylist("update cylist set corcencc='Rha dib 3 ll' where pos='adj.poss' and number='3p';");

# Edit for Rha medd

$sql=query_cylist("update cylist set corcencc='Rha cys 1 u' where pos='pron.emph' and number='1s';");
$sql=query_cylist("update cylist set corcencc='Rha cys 2 u' where pos='pron.emph' and number='2s';");
$sql=query_cylist("update cylist set corcencc='Rha cys 3 g u' where pos='pron.emph' and gender='m' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Rha cys 3 b u' where pos='pron.emph' and gender='f' and number='3s';");
$sql=query_cylist("update cylist set corcencc='Rha cys 1 ll' where pos='pron.emph' and number='1p';");
$sql=query_cylist("update cylist set corcencc='Rha cys 2 ll' where pos='pron.emph' and number='2p';");
$sql=query_cylist("update cylist set corcencc='Rha cys 3 ll' where pos='pron.emph' and number='3p';");

$sql=query_cylist("update cylist set corcencc='Rha gof' where pos='int';");

$sql=query_cylist("update cylist set corcencc='Rha dang g' where pos='adj.dem' and gender='m' and number='sg';");
$sql=query_cylist("update cylist set corcencc='Rha dang b' where pos='adj.dem' and gender='f' and number='sg';");
$sql=query_cylist("update cylist set corcencc='Rha dang d' where pos='adj.dem' and number='sp';");

$sql=query_cylist("update cylist set corcencc='Rha perth' where pos='pron.rel';");

// Edit for Rha atb

// Edit for Rha cil

*/
$sql=query_cylist("update cylist set corcencc='U neg' where pos='prt.neg';");
$sql=query_cylist("update cylist set corcencc='U cad' where pos='prt.aff';");
$sql=query_cylist("update cylist set corcencc='U gof' where pos='prt.int';");
$sql=query_cylist("update cylist set corcencc='U tra' where pos='prt';");

/*
$sql=query_cylist("update cylist set corcencc='Ebych' where pos='im';");

$sql=query_cylist("update cylist set corcencc='Gw acr' where pos='acron';");
$sql=query_cylist("update cylist set corcencc='Gw talf' where pos='n' and gender='mf' and number='pl';");

// Edit for At

*/

?>