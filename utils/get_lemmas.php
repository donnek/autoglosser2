<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script takes a list of words, demutates them, and puts their lemma and enlemma into a new table.

include("includes/fns.php");
include("/opt/autoglosser2/config.php");

$newwords="semtestwords";
$mylemmas="semtestlemmas";

$sql=query("select word from $newwords order by word;");
while ($row=pg_fetch_object($sql))
{
	$surface=$row->word;
// 	echo $surface.": ";
	$line=$surface.": ";
	$desoftsurface=de_soft($surface);  // Surface form without soft mutation.
	$denassurface=de_nas($surface);  // Surface form without nasal mutation.
	$deaspsurface=de_asp($surface);  // Surface form without aspirate mutation.
	
	$sql_cy=query_cylist("select * from cylist where surface='$surface';");
	while ($row_cy=pg_fetch_object($sql_cy))
	{
// 		echo $row_cy->lemma.":";
// 		echo $row_cy->enlemma.", ";
		$line.=$row_cy->lemma.":";
		$line.=$row_cy->enlemma.", ";
	}
	
	if ($desoftsurface!=$surface)
	{
		$sql_sm=query_cylist("select * from cylist where surface~'^$desoftsurface$';");
		while ($row_sm=pg_fetch_object($sql_sm))
		{
// 			echo $row_sm->lemma.":";
// 			echo $row_sm->enlemma.", ";
			$line.=$row_sm->lemma.":";
			$line.=$row_sm->enlemma.", ";
		}
	}
	
	if ($denassurface!=$surface)
	{
		$sql_nm=query_cylist("select * from cylist where surface='$denassurface';");
		while ($row_nm=pg_fetch_object($sql_nm))
		{
// 			echo $row_nm->lemma.":";
// 			echo $row_nm->enlemma.", ";
			$line.=$row_nm->lemma.":";
			$line.=$row_nm->enlemma.", ";
		}
	}
	
	if ($deaspsurface!=$surface)
	{
		$sql_am=query_cylist("select * from cylist where surface='$deaspsurface';");
		while ($row_am=pg_fetch_object($sql_am))
		{
// 			echo $row_am->lemma.":";
// 			echo $row_am->enlemma.", ";
			$line.=$row_am->lemma.":";
			$line.=$row_am->enlemma.", ";
		}
	}	
	
// 	echo "\n";
// 	echo $line."\n";
	
	$entries=array_filter(explode(": ", $line));
	$pairs=array_filter(explode(", ", $entries[1]));
	
	foreach ($pairs as $pair)
	{
// 		echo $pair."\n";
		$lems=explode(":", $pair);
		$lemma=$lems[0];
		$enlemma=$lems[1];
		echo $lemma." + ".$enlemma."\n";
		$sql_u=query("insert into $mylemmas (lemma, enlemma) values ('$lemma', '$enlemma');");
	}
}

?>