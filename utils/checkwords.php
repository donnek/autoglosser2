<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License or the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script takes a list of words (in a database table) and checks whether they are already in Eurfa.  Those which are not are output to a new list.

include("includes/fns.php");
include("/opt/autoglosser2/config.php");

$newwords="semtestwords";

$sql=query("select word from $newwords;");
while ($row=pg_fetch_object($sql))
{
	$surface=$row->word;  // Surface form.
	$decapsurface=strtolower($surface);  // Surface form without capitalisation.
	$desoftsurface=de_soft($surface);  // Surface form without soft mutation.
	$decapdesoftsurface=strtolower($desoftsurface);  // Surface form without soft mutation or capitalisation.
	$denassurface=de_nas($surface);  // Surface form without nasal mutation.
	$decapdenassurface=strtolower($denassurface);  // Surface form without nasal mutation orcapitalisation.
	$deaspsurface=de_asp($surface);  // Surface form without aspirate mutation.
	$decapdeaspsurface=strtolower($deaspsurface);  // Surface form without aspirate mutation or capitalisation.
	$dehsurface=de_h($surface);  // Surface form without
	
	// Check surface form.
	$sql_cy="select * from cylist where surface='$surface'";
	$result_cy=pg_query($db_handle, $sql_cy) or die("Can't get the items");
	if (pg_num_rows($result_cy)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "Surface: ".$surface."<".$surface."\n";
	}
	
	// Remove capitalisation and check.
if ($decapsurface!=$surface)
{
	$sql_cyc="select * from cylist where surface='$decapsurface'";
	$result_cyc=pg_query($db_handle, $sql_cyc) or die("Can't get the items");
	if (pg_num_rows($result_cyc)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "nocaps; ".$decapsurface."<".$surface."\n";
	}
}	

	// Remove soft mutation and check.
if ($desoftsurface!=$surface and $desoftsurface!=$decapsurface)
{
	$sql_sm="select * from cylist where surface~'^$desoftsurface$'";
	$result_sm=pg_query($db_handle, $sql_sm) or die("Can't get the items");
	if (pg_num_rows($result_sm)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "nosoft: ".$desoftsurface."<".$surface."\n";
	}
}

	// Remove soft mutation capitalisation and check.
if ($decapsoftsurface!=$surface and $decapsoftsurface!=$desoftsurface)
{
	$sql_smc="select * from cylist where surface~'^$decapdesoftsurface$'";
	$result_smc=pg_query($db_handle, $sql_smc) or die("Can't get the items");
	if (pg_num_rows($result_smc)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "nosoftnocaps: ".$decapdesoftsurface."<".$surface."\n";
	}
}

	// Remove nasal mutation and check.
if ($denassurface!=$surface and $denassurface!=$decapsurface)
{
	$sql_nm="select * from cylist where surface='$denassurface'";
	$result_nm=pg_query($db_handle, $sql_nm) or die("Can't get the items");
	if (pg_num_rows($result_nm)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "nonas: ".$denassurface."<".$surface."\n";
	}
}

	// Remove nasal mutation capitalisation and check.	
if ($decapdenassurface!=$surface and $decapdenassurface!=$denassurface)
{
	$sql_nmc="select * from cylist where surface='$decapdenassurface'";
	$result_nmc=pg_query($db_handle, $sql_nmc) or die("Can't get the items");
	if (pg_num_rows($result_nmc)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "nonasnocaps: ".$decapdenassurface."<".$surface."\n";
	}
}

	// Remove aspirate mutation and check.	
if ($deaspsurface!=$surface and $deaspsurface!=$decapsurface)
{
	$sql_am="select * from cylist where surface='$deaspsurface'";
	$result_am=pg_query($db_handle, $sql_am) or die("Can't get the items");
	if (pg_num_rows($result_am)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "noasp: ".$deaspsurface."<".$surface."\n";
	}
}

	// Remove aspirate mutation capitalisation and check.
if ($decapaspsurface!=$surface and $decapaspsurface!=$deaspsurface)
{
	$sql_amc="select * from cylist where surface='$decapdeaspsurface'";
	$result_amc=pg_query($db_handle, $sql_amc) or die("Can't get the items");
	if (pg_num_rows($result_amc)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "noaspnocaps: ".$decapdeaspsurface."<".$surface."\n";
	}
}
	// Remove h- prefixation and check.
if ($dehsurface!=$surface and $dehsurface!=$decapsurface)
{
	$sql_h="select * from cylist where surface='$dehsurface'";
	$result_h=pg_query($db_handle, $sql_h) or die("Can't get the items");
	if (pg_num_rows($result_h)>0)
	{
		$sql_u=query("update $newwords set status='present' where word='$surface';");
		echo "noh; ".$dehsurface."<".$surface."\n";
	}
}

}
?>
