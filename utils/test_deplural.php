<?php

/* 
*********************************************************************
Copyright Kevin Donnelly 2016-18.
kevindonnelly.org.uk
This file is part of Autoglosser2, a POS-tagger for Welsh.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License and the GNU
Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
and the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
*********************************************************************
*/ 

// This script tests the depluralising function.  It can also be used to test other things like de-softing.

include("includes/fns.php");
include("/opt/autoglosser2/config.php");

// Test depluralising.
$word="henuriaid";
$word=de_pluralise($word);
echo $word."\n";

// Test de-softing.
// $word="ardd";
// $word=de_soft($word);
// echo $word."\n";
// $bits=explode("]", $word);
// print_r($bits);
// $inits=explode("|", substr($bits[0], 1));
// print_r($inits);
// $init1=$inits[0];  // ... get the first mutation ...
// $init2=$inits[1];  // ... and the second letter ...
// $final=$bits[1];  // ... and the rest of the word ...
// echo $init1."\n";
// echo $init2."\n";
// echo $final."\n";
// $mutarray['soft1']=$init1.$final;  // ... and create two new words without the couplet ...
// $mutarray['soft2']=$init2.$final;
// $mutarray['soft1u']=ucfirst($init1.$final);  // ... and also capitalise them.
// $mutarray['soft2u']=ucfirst($init2.$final);
// print_r($mutarray);

?>