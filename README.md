# Autoglosser 2 #

Autoglosser2 is a glosser/tagger for Welsh.  It is a re-focussed version of the [Bangor Autoglosser]( https://github.com/donnekgit/autoglosser) aimed at formal written Welsh text rather than conversational multilingual text.  Autoglosser2 is rule-based rather than stats-based, using constraint grammar to prune a list of part-of-speech possibilities generated from [Eurfa](http://eurfa.org.uk), my GPL-ed Welsh dictionary.  The default output (in *pdf* and *txt* format) consists of glosses as used in the [Bangor corpora](http://bangortalk.org.uk), but it can also output using [CorCenCC](http://www.corcencc.org) tags. Autoglosser2 runs as a pipeline of command-line PHP scripts, but a web-browser interface is also available.  Glossing speed is upwards of 22,000 words/minute.

### Licensing ###

Autoglosser2 is licensed under the GPLv3 or later.

### How do I get set up? ###

See the [detailed manual](http://autoglosser.org.uk/resources/manual.pdf) on the website.

### Contribution guidelines ###

You're welcome to fork and make improvements.  Please try and follow the coding style (procedural, plenty of comments, blank lines between code sections, and NO opening brace at the end of lines - put them on a new line).

### Who do I talk to? ###

If you have any queries or comments, contact kevin@dotmon.com.

Kevin Donnelly